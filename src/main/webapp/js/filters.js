angular.module('jaduApp')
.filter('titleCase', function() {
  return function(input) {
    input = input || '';
    return input.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
  };
});

angular.module('jaduApp')
.filter("trustUrl", ['$sce', function ($sce) {
  return function (recordingUrl) {
      return $sce.trustAsResourceUrl(recordingUrl);
  };
}]);
