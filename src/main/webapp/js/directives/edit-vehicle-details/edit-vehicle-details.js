angular.module('jaduApp').directive('editVehicleDetails', ['$http', '$location', '$timeout','$compile', '$window','ajaxService', function ($http, $location, $timeout, $compile, $window, ajaxService) {
    return {
        replace: true,
        restrict: 'EA',
        scope: {
            vehicleDetails : "=",
            vehicleNumber: "=",
            caseId: "="
        },
        templateUrl: "js/directives/edit-vehicle-details/edit-vehicle-details.html",
        controller: function ($scope, $element, $q) {

          $scope.newVehicleDetails = {};

          $scope.setSelectedMakeModels = function(){
            $scope.vehicleMakeSelectedModels = [];

            for(i=0;i<$scope.vehicleMakeModels.length;i++){
              if($scope.vehicleMakeModels[i].make==$scope.newVehicleDetails.make){
                $scope.vehicleMakeSelectedModels.push($scope.vehicleMakeModels[i]);
              }
            }
            console.log('$scope of dire-->',$scope);

          }

          $scope.setNewVehicleDetails = function(){
            console.log('setting data-->', $scope);
            console.log('this-->', this.vehicleNumber);


            $scope.newVehicleDetails.vehicle_number = $scope.vehicleNumber;
            console.log('setting data-->', $scope);

            $scope.setSelectedMakeModels();
          }

          $scope.getVehicleDetails = function(){
            angular.element(document).ready(function () {
              $q.all([
                  ajaxService.getVehicleTypes(null, 'Please Wait!...'),
                  ajaxService.getVehicleFuelTypes(null, 'Please Wait!...'),
                  ajaxService.getVehicleMakeModels(null, 'Please Wait!...'),
              ]).then(function(data){
                  $scope.vehicleTypes = [];
                  _.map(_.uniqBy(data[0], 'id'), function (item) {
                    $scope.vehicleTypes.push(item);
                  });

                  $scope.vehicleFuelTypes = data[1];
                  $scope.vehicleMakeModels = data[2];

                  $scope.vehicleMakeModelsUniq = [];
                  _.map(_.uniqBy(data[2], 'make'), function (item) {
                    $scope.vehicleMakeModelsUniq.push(item);
                  });

                  $scope.setNewVehicleDetails();

              });

            });
          }

          $scope.getVehicleDetails();

          $scope.verifyVahanMessage = "Please verify vehicle number!"
          $scope.vahanVerified = true;

          $scope.getVahanData = function(){
            ajaxService.getVahanDetails({vehicle_number:$scope.newVehicleDetails.vehicle_number},'Verifying Vehicle Details!...')
            .then(function(res){
              console.log('Vahan Data', res);
              $scope.newVehicleDetails.chassis_number = res.chasisNumber;
              $scope.newVehicleDetails.engine_number = res.engineNumber;
              $scope.newVehicleDetails.vehicle_fuel_type = res.vehicleFuelType;
              $scope.newVehicleDetails.yom = res.year;

              $scope.verifyVahanMessage = ""
              $scope.vahanVerified = false;
            })
            .catch(function(e){
              console.log('Verifying vehicle details failed!', e);
              alert('Verifying vehicle details failed!');
            })
          }

          $scope.vahanVerify = function(){
            console.log('verify===>',$scope.vehicleNumber,$scope.newVehicleDetails.vehicle_number);
            if(!$scope.newVehicleDetails.vehicle_number)
              $scope.newVehicleDetails.vehicle_number = $scope.vehicleNumber;
            $scope.getVahanData();
          }

          $scope.submitNewVehicleData = function(){

            var data = {
              case_id: $scope.caseId,
              vehicle_number: $scope.newVehicleDetails.vehicle_number,
              vehicle_type: $scope.newVehicleDetails.type,
              vehicle_color: $scope.newVehicleDetails.color,
              make_model_id: $scope.newVehicleDetails.makeModel,
              fuel_type: $scope.newVehicleDetails.vehicle_fuel_type,
              yom: $scope.newVehicleDetails.yom
            }
            console.log('data=================================*******',data);

            ajaxService.updateCaseVehicleDetails(data, 'Update Details. PleaseWait!...')
            .then(function(){
              alert('Vehicle Data Updated!');
              $scope.vahanVerified = true;
            })
            .catch(function(e){
              alert('Error updating Vehicle Data.');
              console.error('Error updating Vehicle Data.',e);
            })
          }

        }
    };
}]);
