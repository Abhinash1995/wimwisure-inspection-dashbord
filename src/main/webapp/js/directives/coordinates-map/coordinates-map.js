
/* global ModalWindow, angular, atqUTIL, ATQConfirmDialog */

angular.module('jaduApp').directive('coordinatesMap', ['$http', '$location', '$timeout','$compile', '$window','ajaxService', function ($http, $location, $timeout, $compile, $window, ajaxService) {
    return {
        replace: true,
        restrict: 'EA',
        scope: {
            casePhotos : "=",
            caseId : "="
        },
        controller: function ($scope, $element) {

        },
        link: function(scope, element, attrs){

            function calcCrow(lat1, lon1, lat2, lon2) {
                var R = 6371; // km
                var dLat = toRad(lat2-lat1);
                var dLon = toRad(lon2-lon1);
                var lat1 = toRad(lat1);
                var lat2 = toRad(lat2);

                var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
                  Math.sin(dLon/2) * Math.sin(dLon/2) * Math.cos(lat1) * Math.cos(lat2);
                var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
                var d = R * c;
                return d*1000;
            }

            function getChasisNumberPhoto(){
                for(var i=0; i<scope.casePhotos.length; i++){
                    if(scope.casePhotos[i].photoType === "90-deg-front")
                        return scope.casePhotos[i];
                }

                return null;
            };

            // Converts numeric degrees to radians
            function toRad(Value) {
                return Value * Math.PI / 180;
            }

            function showChart(){

              var chachisPhoto = getChasisNumberPhoto();

              if(chachisPhoto){
                  var chasisLat =  chachisPhoto.latitude;
                  var chasisLng = chachisPhoto.longitude;

                  for(var i=0; i<scope.casePhotos.length; i++){
                      var item = scope.casePhotos[i];
                      // console.log('item in coor--', item);
                      var lat = item.latitude;
                      var lng = item.longitude;

                      item.thisPhotoUrl = "util/case-image/"+scope.caseId+"/"+item.fileName;
                      item.photoIndex = i+1;

                      if(i > 0 &&  lat >0 && lng > 0){
                        item.distanceFromLastPhoto = Math.round(calcCrow(scope.casePhotos[i-1].latitude, scope.casePhotos[i-1].longitude, lat, lng)*100)/100  + "  Mtrs.";
                        item.timeFromLastPhoto = moment(parseInt(item.fileName.split('.')[0])).diff(moment(parseInt(scope.casePhotos[i-1].fileName.split('.')[0])), 'minutes')+' min';


                        if(item.timeFromLastPhoto == '0 min'){
                          item.timeFromLastPhoto = moment(parseInt(item.fileName.split('.')[0])).diff(moment(parseInt(scope.casePhotos[i-1].fileName.split('.')[0])), 'seconds')+' sec';
                        }
                      }
                      else{
                        item.distanceFromLastPhoto = "N/A";
                        item.timeFromLastPhoto = "N/A";
                      }

                      item.photoTime = moment.unix(item.fileName.replace(/\.[^/.]+$/, "")/1000).format("YYYY/MM/DD HH:mm:ss");

                      if(calcCrow(chasisLat, chasisLng, lat, lng) > 25 ){

                          item.indexLabel = "Check";
                          item.markerType = "triangle";
                          item.markerSize = 12;
                          item.markerColor = "red";
                      }
                  }
              }

              var xmin = null, xmax = null, ymin = null, ymax = null;
                  for(var i=0; i<scope.casePhotos.length; i++){
                      scope.casePhotos[i].x = scope.casePhotos[i].latitude;
                      scope.casePhotos[i].y = scope.casePhotos[i].longitude;


                      if(!scope.casePhotos[i].x  || !scope.casePhotos[i].y)
                          continue;

                      if(xmin == null)
                          xmin = scope.casePhotos[i].x;

                      if(xmax == null)
                          xmax = scope.casePhotos[i].x;

                      if(ymin == null)
                          ymin = scope.casePhotos[i].y;

                      if(ymax == null)
                          ymax = scope.casePhotos[i].y;

                      if(xmin > scope.casePhotos[i].x)
                          xmin = scope.casePhotos[i].x;

                      if(xmax < scope.casePhotos[i].x)
                          xmax = scope.casePhotos[i].x;

                      if(ymin > scope.casePhotos[i].y)
                          ymin = scope.casePhotos[i].y;

                      if(ymax < scope.casePhotos[i].y)
                          ymax = scope.casePhotos[i].y;
                  }

                  xmin = xmin - 0.0001;
                  xmax = xmax + 0.0001;

                  ymin = ymin - 0.0001;
                  ymax = ymax + 0.0001;

              var chart = new CanvasJS.Chart("chartContainer", {
                  //width: "100",
                      animationEnabled: true,
                      zoomEnabled: true,
                      title:{
                              text: ""
                      },
                      axisX: {
                              title:"",
                              minimum: xmin,
                              maximum: xmax
                      },
                      axisY:{
                              title: "",
                              minimum: ymin,
                              maximum: ymax
                      },
                      data: [{
                              type: "scatter",
                              toolTipContent: "\
                                  <b>No: </b>{photoIndex} <br/>\n\
                                  <b>Latitude: </b>{x} <br/>\n\
                                  <b>Longitude: </b>{y} <br/> \n\
                                  <b>Photo Type: </b> {photoType} <br/>\n\
                                  <b>Distance from last photo: </b> {distanceFromLastPhoto} <br/>\n\
                                  <b>Photo Time: </b>  {photoTime} <br/>\n\
                                  <b>Elapsed Time Since last photo: </b>  {timeFromLastPhoto} <br/>\n\
                                  <img class='case-photo-loader' style='width:300px;' src={thisPhotoUrl} /> <br/>\n\
                                  ",
                              dataPoints: scope.casePhotos
                      }]
              });
              $timeout(function(){
                  chart.render();
              }, 1000);
              
          }

            showChart();

            console.log(scope.chartData)
        }
    };
}]);
