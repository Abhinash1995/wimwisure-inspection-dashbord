angular
		.module('jaduApp')
		.directive(
				'agentComments',
				[
						'$http',
						'$location',
						'$timeout',
						'$compile',
						'$window',
						'ajaxService',
						function($http, $location, $timeout, $compile, $window,
								ajaxService) {
							return {
								replace : true,
								restrict : 'EA',
								scope : {
									agentinspectionCase : "="
								},
								templateUrl : "js/directives/agentComments/agentComments.html",
								style : ".md-datepicker-calendar-pane {z-index: 1200;}",
								controller : function($scope, $element) {

								},
								resolve : {
									style : function() {
										angular
												.element('head')
												.append(
														'.md-datepicker-calendar-pane {z-index: 1200 !important;}');
									}
								},
								link : function(scope, element, attrs) {
									console
											.log(scope.$root.selectedCaseForComment);
									// var maxDate = new Date();
									// scope.maxDateStart = new Date();
									// scope.maxDateEnd = new Date();
									// scope.StartDate = function(){
									// scope.minDateEnd = scope.maxDateStart;
									// }
									scope.getComments = function() {
										// scope.$root.selectedCaseForComment.from
										// = (scope.startDate).getTime();
										// scope.$root.selectedCaseForComment.to
										// = (scope.endDate).getTime();
										ajaxService
												.agentComments(
														{
															phonenumber : scope.$root.selectedCaseForComment.phoneNumber
														// from :
														// scope.$root.selectedCaseForComment.from,
														// to:
														// scope.$root.selectedCaseForComment.to
														},
														"Fetching comments. Please wait!")
												.then(
														function(result) {
															if (result.data.lenght <= 0) {
																alert("No comments")
															}
															var data = result.data;
															scope.caseCount = result.data.casesCount;
															scope.sevenDays = scope.caseCount[0];
															scope.fifteenDays = scope.caseCount[1];
															scope.thirtyDays = scope.caseCount[2];
															scope.total = scope.caseCount[3];
															scope.comments = result.data.caseCommentDTO;
														},
														function(err) {
															console.log(err);
															alert("Unable to fetch comments. Please try again later.");
														});
									};

									scope.addComments = function() {
										ajaxService
												.addAgentComments(
														{
															phonenumber : scope.$root.selectedCaseForComment.phoneNumber,
															comment : scope.comment
														},
														"Adding comment. Please wait!")
												.then(
														function(data) {
															alert("Comment added");
															scope.comment = null;
															scope.getComments()
														},
														function(err) {
															console.log(err);
															alert("Unable to add comment. Please try again later.");
														});
									};

									// scope.getComments();
								}
							};
						} ]);
