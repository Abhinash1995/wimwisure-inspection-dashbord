
/* global ModalWindow, angular, atqUTIL, ATQConfirmDialog */

angular.module('jaduApp').directive('userProfile', ['$http', '$location', '$timeout','$compile', '$window','ajaxService', 'GoogleMaps', function ($http, $location, $timeout, $compile, $window, ajaxService, GoogleMaps) {
    return {
        replace: true,
        restrict: 'EA',
        scope: {
            userProfile : "="
        },
        templateUrl: "js/directives/user-profile/user-profile.html",
        controller: function ($scope, $element, $mdDialog) {
            console.log('User-profile directive loaded');
        }
    };
}]);
