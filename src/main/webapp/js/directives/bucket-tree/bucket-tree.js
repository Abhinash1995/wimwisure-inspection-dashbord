angular.module('jaduApp').directive('bucketTree', ['$http', '$location', '$timeout','$compile', '$window','ajaxService', function ($http, $location, $timeout, $compile, $window, ajaxService) {
    return {
        replace: true,
        restrict: 'EA',
        scope: {
            callback: "&"
        },
        templateUrl: "js/directives/bucket-tree/bucket-tree.html",
        link: function(scope, element, attrs){
            ajaxService.getBucketObjects("Fetching uploaded files. Please wait!").then(function(data){
                scope.files = data;
            }, function(error){
                console.log(error);
                alert("Unable to fetch data!");
            });
            
            scope.selectFile = function(file){
                scope.callback({
                    file : file
                });
            };
        }
    };
}]);
