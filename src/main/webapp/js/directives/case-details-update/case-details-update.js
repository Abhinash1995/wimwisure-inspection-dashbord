angular.module('jaduApp').directive('caseDetailsUpdate', ['$http', '$location', '$timeout','$compile', '$window','ajaxService', function ($http, $location, $timeout, $compile, $window, ajaxService) {
    return {
        replace: true,
        restrict: 'EA',
        scope: {
            currentCaseId : "=",
            newCaseData : "=",
            callback: "&"
        },
        templateUrl: "js/directives/case-details-update/case-details-update.html",
        controller: function ($scope, $element) {
          console.log('DIRECTIVE SCOPE-->', $scope);
          $scope.updateCaseDetails = function(){
            var dataCPN = {
              case_id: $scope.currentCaseId,
              customer_phone_number: $scope.newCaseData.newCustomerPhoneNumber
            }
            var dataRPN = {
              case_id: $scope.currentCaseId,
              phone_number: $scope.newCaseData.newRequestorPhoneNumber
            }
            var dataVN = {
              case_id: $scope.currentCaseId,
              vehicle_number: $scope.newCaseData.newVehicleNumber
            }
            console.log('=================>***********', dataVN, dataCPN, dataRPN);
            $q.all([
              ajaxService.updateCustomerPhoneNumber(dataCPN),
              ajaxService.updateAgentPhoneNumber(dataRPN),
              ajaxService.updateVehicleNumber(dataVN)
            ])
            .then(function(res){
              // console.log('updatecasedet===> ', res);
              alert('Case Details Updated!');
              $scope.callback('refresh');
            })
            .catch(function(){
              alert('Error in Case Details!')
            });

          }
        },
        link: function(scope, element, attrs){

          console.log('link----------------> ', scope);
            // scope.closeReasons = [
            //     {id : "Customer not interested"},
            //     {id : "Inpector not avilable"},
            //     {id : "Inspection already done"},
            //     {id : "Agent asked to cancel"}
            // ];
            //
            // scope.closeCase = function(){
            //     var dataObj = {
            //         case_id:scope.inspectionCase.id,
            //         reason:scope.inspectionCase.closeReason
            //     };
            //
            //     ajaxService.closeCase(dataObj,'Please Wait...')
            //     .then(function(res){
            //         alert('Case Closed!');
            //         scope.callback();
            //     })
            //     .catch(function(e){
            //         console.error('Case Rescheduling Error',e);
            //     });
            // };

        }
    };
}]);
