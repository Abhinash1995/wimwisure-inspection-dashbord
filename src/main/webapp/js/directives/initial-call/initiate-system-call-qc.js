angular
		.module('jaduApp')
		.directive(
				'initiateSystemCallQc',
				[
						'$http',
						'$location',
						'$timeout',
						'$compile',
						'$window',
						'ajaxService',
						function($http, $location, $timeout, $compile, $window,
								ajaxService) {
							return {
								replace : true,
								restrict : 'EA',
								scope : {
									initiateSysCallQc : "="
								},
								templateUrl : "js/directives/initial-call/initiate-system-call-qc.html",
								style : ".md-datepicker-calendar-pane {z-index: 1200;}",
								controller : function($scope, $element) {

								},
								resolve : {
									style : function() {
										angular
												.element('head')
												.append(
														'.md-datepicker-calendar-pane {z-index: 1200 !important;}');
									}
								},
								link : function(scope, element, attrs) {
									var maxDate = new Date();
									scope.maxDateStart = new Date();
									scope.maxDateEnd = new Date();
									scope.StartDate = function() {
										scope.minDateEnd = scope.startDate;
									}
									scope.initiatecall = function() {
										var inspection = "";

										ajaxService
												.initiatecallHoldCases(
														{
															self_inspection : false,
															from : new Date(
																	scope.startDate)
																	.getTime(),
															to : new Date(
																	scope.endDate)
																	.getTime(),
															case_stage : 4
														},
														"Initiating system call. Please wait!")
												.then(
														function(result) {
															alert("System call has been initiated successfully");
															console.log(result);
														},
														function(err) {
															console.log(err);
															alert("Unable to initiate call. Please try again later.");
														});
									};

								}
							};
						} ]);
