jaduApp.controller('CancelledCasesController',['$scope', '$timeout', '$location', '$compile', 'ajaxService', '$q', '$window',  function($scope, $timeout, $location, $compile, ajaxService, $q, $window) {

    angular.element(document).ready(function () {
        $q.all([
            ajaxService.getCancelledCases(null, "Fetching data. Please wait!")
        ]).then(function(res){
            $scope.cancelledCases = res[0].data;
        });
    });
}]);
