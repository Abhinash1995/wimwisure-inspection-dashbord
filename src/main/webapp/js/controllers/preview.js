/* global angular */

jaduApp.controller('PreviewController', ['$scope', '$compile', 'ajaxService','$routeParams', function ($scope, $compile, ajaxService, $routeParams) {

        $scope.caseVideoRequest = true;
        
        console.log($routeParams)
        
        angular.element(document).ready(function () {
            var vidReq = new XMLHttpRequest();
            vidReq.open('GET', "util/inspection-video/" + $routeParams.caseId + "/" + $routeParams.videoName, true);
            vidReq.responseType = 'blob';

            vidReq.onload = function () {
                // Onload is triggered even on 404
                // so we need to check the status code
                if (this.status === 200) {
                    var videoBlob = this.response;
                    var vid = URL.createObjectURL(videoBlob); // IE10+
                    // Video is now downloaded
                    console.log('Video is now downloaded', vid);
                    // and we can set it as source on the video element
                    $scope.$apply(function () {
                        $scope.caseVideo = vid;
                        $scope.caseVideoRequest = false;
                    });
                    // $scope.caseVideo = vid;
                    // $scope.caseVideoRequest = false;

                }
            };

            vidReq.onerror = function () {
                alert('Video download Error');
            };

            vidReq.send();

        });
    }]);
