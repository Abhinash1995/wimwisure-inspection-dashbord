jaduApp.controller('ClosedCasesController',['$scope', '$timeout', '$location', '$compile', 'ajaxService', '$q', '$window',  function($scope, $timeout, $location, $compile, ajaxService, $q, $window) {

    angular.element(document).ready(function () {
        $q.all([
            ajaxService.getClosedCases(null, "Fetching data. Please wait!")
        ]).then(function(res){
            $scope.closedCases = res[0].data;
        });
    });
}]);
