jaduApp.controller('CaseRequestsController',['$scope', '$timeout', '$location', '$compile', 'ajaxService', '$q',  function($scope, $timeout, $location, $compile, ajaxService, $q) {

    $scope.editCase = function(id){
        $scope.currentCase = getCaseById(id);

        var timestamp = moment.unix($scope.currentCase.inspectionTime/1000);
        $scope.currentCase.inspectionTimeModel = timestamp;
        $scope.currentCase.inspectionTime = ( timestamp.format("YYYY/MM/DD HH:mm:ss") );
        $("#case-requests-modal").modal('toggle');
    };

    $scope.allotCase = function(id){
        $scope.currentCase = getCaseById(id);

        var timestamp = moment.unix($scope.currentCase.inspectionTime/1000);
        $scope.currentCase.inspectionTimeModel = timestamp;
        $scope.currentCase.inspectionTime = ( timestamp.format("YYYY/MM/DD HH:mm:ss") );
        $("#case-requests-modal-2").modal('toggle');
    };

    $scope.searchUser = function(){
        $scope.matchedUser = null;
        ajaxService.getUserByPhoneNumber($scope.searchUserModel).then(function(data){
            $scope.matchedUser = (data);
        }, function(){
            alert("No user found with given phone number");
        });
    };

    $scope.updateCase = function(){
        $("#case-requests-modal-2").modal('hide');
        ajaxService.allotCaseManual({
            case_id : $scope.currentCase.id,
            username : $scope.matchedUser.username
        }, "Alloting case. Please wait!").then(function(){
            alert("Case allotted successfully!");
            $location.url('/scheduled-cases');
        }, function(){
            alert("Unable to allot case!");
        });
    };



    function getCaseById(id){
        for(var i=0; i< $scope.qcCases.length; i++){
            if($scope.qcCases[i].id === id)
                return $scope.qcCases[i];
        }

        return null;
    }

        angular.element(document).ready(function () {
        $q.all([
            ajaxService.getCaseRequests(null, "Fetching data. Please wait!")
        ]).then(function(res){
            $scope.qcCases = res[0].data;
        });
    });
}]);
