jaduApp.controller('ReportController',['$scope', '$timeout', '$location', '$compile', 'ajaxService', '$q', '$routeParams', '$rootScope',  function($scope, $timeout, $location, $compile, ajaxService, $q, $routeParams, $rootScope) {

    function getSelectedOptionForQuestion(questionId){
        for(var i=0; i<$scope.answers.length; i++){
            var item = $scope.answers[i];
            if(item.questionId === questionId){
                var optionId = item.optionId;
                for(var j=0; j<$scope.options.length; j++){
                    if($scope.options[j].id === optionId)
                        return $scope.options[j];
                }
            }
        }
    }

    angular.element(document).ready(function () {
        $rootScope.fullScreen = true;
        $q.all([
            ajaxService.getCaseDetails({ case_id : $routeParams.id}, "Fetching data. Please wait!"),
            ajaxService.getQCAnswers({ case_id : $routeParams.id}, ""),
            ajaxService.getCaseQuestions({ case_id : $routeParams.id}, "Fetching data. Please wait!"),
            ajaxService.getQuestionOptions(null, "Fetching data. Please wait!")
        ]).then(function(res){
            console.log(res);
            var questions = res[2].data;
            $scope.answers = res[1].data;
            $scope.options = res[3].data;
            $scope.ic = res[0].data;

            for(var i=0; i<questions.length; i++){
                var item = questions[i];
                item.answer = getSelectedOptionForQuestion(item.id);
            }

            $scope.questionsGrouped = _.groupBy(questions,'photoTag');
            //console.log(questionsGrouped)
        });
    });
}]);
