jaduApp.controller('AllocationPendingController',['$scope', '$timeout', '$location', '$compile', 'ajaxService', '$q', 'GoogleMaps','gmap',  function($scope, $timeout, $location, $compile, ajaxService, $q, GoogleMaps, gmap) {

  $scope.verifyAgentContactActive = false;
  $scope.verifyAgentContactStatus = false;
  $scope.verifyAgentContactActiveClass = '';
  $scope.verifyFlag = 0;
  $scope.searchPlaceholder = "Agent Phone Number"
  $scope.case = {
    agentType : 'Agent',
    radius : 20,
    selectedInspectorUsername: "",
    address:null
  };

  $scope.newCaseData = {
    customerPhoneNumber: undefined,
    requestorPhoneNumber: undefined,
    vehicleNumber: undefined
  };

  $scope.flagCalculator = function(){
    $scope.currentTime = moment((moment().unix())*1000);
      for(i=0;i<$scope.qcCases.length;i++){
        if($scope.qcCases[i].inspectionStartTime == null){//insepection not started
          var insTime = moment($scope.qcCases[i].inspectionTime);
          var diff = $scope.currentTime.diff(insTime, 'minutes', true);
          if(diff >= 15 && diff < 30){
            $scope.qcCases[i].flagStatus = 'b-status-orange';
          }
          else
          if(diff >= 30){
            $scope.qcCases[i].flagStatus = 'a-status-red';
          }
          else{
            $scope.qcCases[i].flagStatus = '';
          }
        }
        else{
          var insStartTime = moment($scope.qcCases[i].inspectionStartTime);

          var diff = $scope.currentTime.diff(insStartTime, 'minutes', true);
          if(diff >= 15 && diff < 30){
            $scope.qcCases[i].flagStatus = 'b-status-orange';
          }
          else
          if(diff >= 30){
            $scope.qcCases[i].flagStatus = 'a-status-red';
          }
          else{
            $scope.qcCases[i].flagStatus = '';
          }
        }
    }

    $timeout(function(){
      $scope.flagCalculator();
    },6000)

  }

  $scope.getCases = function(spinnerText){
    $q.all([
      ajaxService.getAllocationRequestedCases(null, spinnerText)
    ])
    .then(function(res){
      $scope.qcCases = res[0].data;
      $scope.flagCalculator();
      // $scope.refreshDeamon();
    });
  };

  $scope.refreshDeamon = function(){
    $timeout(function(){
      $scope.getCases('refresh');
    },60000)
  }

  function getCaseById(id){
    for(var i=0; i< $scope.qcCases.length; i++){
      if($scope.qcCases[i].id === id)
        return $scope.qcCases[i];
    }
    return null;
  }

  $scope.allotCase = function(id, index){
    $scope.allotCaseId = id;
    $scope.currentCase = $scope.qcCases[index];
    $("#modal-allocation-pending-allot").modal('toggle');
    $scope.initializeGoogleMap($scope.currentCase.inspectionLatitude, $scope.currentCase.inspectionLongitude);
    $scope.getInspectorsInRadius();
  }

  $scope.editCase = function(id, index){
    $scope.editCaseId = id;
    $scope.editCaseIndex = index;
    $scope.currentCase = $scope.qcCases[index];

    $scope.newCaseData.customerPhoneNumber = $scope.currentCase.customerPhoneNumber;
    $scope.newCaseData.requestorPhoneNumber = $scope.currentCase.requestorPhoneNumber;
    $scope.newCaseData.vehicleNumber = $scope.currentCase.vehicleNumber;

    $("#modal-allocation-pending-edit").modal('toggle');
  };

  $scope.initializeGoogleMap = function(lat, lng){

    var mapElement = document.getElementById('allocation-map');
    var mapInputElement = document.getElementById('allocation-map-input');

    $scope.map = gmap.initMap(mapElement, lat, lng);
    gmap.addCirlce($scope.map, lat, lng, $scope.case.radius);

  }

  $scope.initializeAgentAutoComplete = function(){
    self.agents = loadAll();
    self.simulateQuery = false;
    self.isDisabled    = false;

    self.querySearch   = querySearch;
    self.selectedItemChange = selectedItemChange;
    self.searchTextChange   = searchTextChange;

    self.newState = newState;
  }

  $scope.setAgentType = function(){
    $scope.searchPlaceholder = $scope.case.agentType+" Phone Number";
    $scope.getInspectorsInRadius();
  }

  $scope.updateInspectorRadius = function(t){
    $timeout(function(){
      var currentMapPosition = gmap.getCurrentPosition();
      gmap.addCirlce($scope.map, currentMapPosition.lat, currentMapPosition.lng, $scope.case.radius);
      $scope.getInspectorsInRadius();
    },500*t);

  }

  $scope.getInspectorsInRadius = function(){
    var currentMapPosition = gmap.getCurrentPosition();
    var data = {
      latitude: currentMapPosition.lat ,
      longitude: currentMapPosition.lng ,
      radius: $scope.case.radius
    }
    ajaxService.getInspectorsInRadius(data, "Fetching inspectors. Please wait!")
    .then(function(res){
      $scope.inspectorsInRadius = res;
      $scope.initializeAgentAutoComplete();
    })
  }

  $scope.assignCaseToInspector = function(){
    var data = {
      case_id: $scope.allotCaseId,
      username: $scope.case.selectedInspectorUsername
    }
    ajaxService.assignCaseToInspector(data,'Assigning Case to Inspector. Please wait!')
    .then(function(res){
      $("#modal-allocation-pending-allot").modal('toggle');
    })
  }

  $scope.updateCaseDetails = function(){
    var dataCPN = {
      case_id:$scope.editCaseId,
      customer_phone_number:$scope.newCaseData.customerPhoneNumber
    }
    var dataRPN = {
      case_id:$scope.editCaseId,
      phone_number:$scope.newCaseData.requestorPhoneNumber
    }
    var dataVN = {
      case_id:$scope.editCaseId,
      vehicle_number:$scope.newCaseData.vehicleNumber
    }
    $q.all([
      ajaxService.updateCustomerPhoneNumber(dataCPN),
      ajaxService.updateAgentPhoneNumber(dataRPN),
      ajaxService.updateVehicleNumber(dataVN)
    ])
    .then(function(res){
      alert('Case Details Updated!');
      $scope.qcCases[$scope.editCaseIndex].customerPhoneNumber = $scope.newCaseData.customerPhoneNumber;
      $scope.qcCases[$scope.editCaseIndex].requestorPhoneNumber = $scope.newCaseData.requestorPhoneNumber;
      $scope.qcCases[$scope.editCaseIndex].vehicleNumbervehicleNumber = $scope.newCaseData.vehicleNumber;
      $("#modal-allocation-pending-edit").modal('hide');

    })
    .catch(function(){
      alert('Error in Case Details!');
    });

  };

  $scope.closeCaseCallback = function(){
    $("#modal-allocation-pending-edit").modal('hide');
    $scope.getCases("Fetching cases. Please wait!");
  };

  angular.element(document).ready(function () {
    $scope.getCases("Fetching cases. Please wait!");
  });

  var self = $scope;
  self.agents = "";

  function newState(state) {
    alert("Sorry! No data present for entered query!");
  }

  function querySearch (query) {
    var results = query ? self.agents.filter( createFilterFor(query) ) : self.agents,
        deferred;

    if (self.simulateQuery) {
      deferred = $q.defer();
      $timeout(function () { deferred.resolve( results ); }, Math.random() * 1000, false);
      return deferred.promise;
    }
    else {
      return results;
    }
  }

  function searchTextChange(text) {
  }

  function selectedItemChange(item) {
    $scope.case.inspectorUsername = item.value;
  }

  function loadAll() {

    var agents = [];
    $scope.inspectorsInRadius.forEach(function(agent, index){
      agents.push(agent.phoneNumber);
    });

    return agents.map( function (agent) {
      return {
        value: agent,
        display: agent
      };
    });
  }

  function createFilterFor(query) {
    var lowercaseQuery = angular.lowercase(query);
    return function filterFn(agent) {
      return (agent.value.indexOf(lowercaseQuery) === 0);
    };
  }

}]);
