/* global angular, jaduApp */

jaduApp
        .controller(
                'AllCasesController',
                [
                    '$scope',
                    '$timeout',
                    '$location',
                    '$compile',
                    'ajaxService',
                    '$q',
                    '$window',
                    'NgTableParams',
                    function ($scope, $timeout, $location, $compile,
                            ajaxService, $q, $window, NgTableParams) {
                        $scope.data = [];

                        $scope.showInspectionReport = function (id) {
                            $window.open('#/report/' + id, '_blank');
                        };

                        $scope.getCaseDetailsModel = {
                            case_id: "",
                            vehicle_id: "",
                            customer_phone: "",
                            requester_phone: ""
                        };

                        $scope.clearValues = function () {
                            $scope.getCaseDetailsModel = {
                                case_id: "",
                                vehicle_id: "",
                                customer_phone: "",
                                requester_phone: ""
                            };
                        }

                        /* Old code to fetch all the cases form DB */
                        /*
                         * $scope.fetchAllCases = function(spinnerText) { $q
                         * .all( [ ajaxService.getAllCases(null,
                         * spinnerText) ]) .then( function(res) {
                         * $scope.data = res[0].data; $scope.allCasesTable =
                         * new NgTableParams( { count : 50 // count per page }, {
                         * dataset : $scope.allCases }); }); };
                         */

                        /*
                         * $scope.fetchAllCases = function(spinnerText) {
                         * $scope.allCases = [];
                         * 
                         * $scope.allCasesTable = new NgTableParams({ count :
                         * 50 // count per page }, { dataset :
                         * $scope.allCases }); };
                         */
                        
                        $scope.submitReportToIffco = function(id){
                            ajaxService.submitStatusToIffco({
                                case_id : id
                            }, "Updaing status. Please wait!").then(
                                    function (res) {
                                        alert(res.status);
                                    }, function (err) {
                                alert("Unable to update. Please contact administrator.");
                                console.log(err);
                            });
                        }

                        $scope.getCaseDetails = function () {
                            var fieldName = '';
                            var fieldValue = "";
                            // To get form value and name
                            if ($scope.getCaseDetailsModel.case_id != "") {
                                fieldName = "case_id";
                                fieldValue = $scope.getCaseDetailsModel.case_id;
                            } else if ($scope.getCaseDetailsModel.vehicle_id != "") {
                                fieldName = "vehicle_id";
                                fieldValue = $scope.getCaseDetailsModel.vehicle_id;
                            } else if ($scope.getCaseDetailsModel.customer_phone != "") {
                                fieldName = "customer_phone";
                                fieldValue = $scope.getCaseDetailsModel.customer_phone;
                            } else {
                                fieldName = "requester_phone";
                                fieldValue = $scope.getCaseDetailsModel.requester_phone;
                            }
                            ajaxService.getRequestedCases({
                                field: fieldName,
                                value: fieldValue
                            }, "Fetching details. Please wait!").then(
                                    function (res) {
                                        $scope.data = res.data;
                                        $scope.clearValues();
                                    }, function (err) {
                                alert("No results found");
                                $scope.clearValues();
                            });
                        };

                        $scope.fetchCompletedCases = function (spinnerText) {
                            $q
                                    .all(
                                            [ajaxService
                                                        .getCompletedCases(
                                                                null,
                                                                spinnerText),
                                                ajaxService.getVehicleTypes(null, 'Please Wait!...')

                                            ])
                                    .then(
                                            function (res) {
                                                $scope.data = res[0].data;
                                                $scope.vehicleTypes = [];

                                                _.map(_.uniqBy(res[1].data, 'id'), function (item) {
                                                    $scope.vehicleTypes.push(item);
                                                });
                                                $scope.completedCasesTable = new NgTableParams(
                                                        {
                                                            count: 50
                                                                    // count per page
                                                        },
                                                {
                                                    dataset: $scope.allCompletedCases
                                                });
                                            });
                        };

                        $scope.fetchClosedCases = function (spinnerText) {
                            $q
                                    .all(
                                            [ajaxService.getClosedCases(
                                                        null, spinnerText)])
                                    .then(
                                            function (res) {
                                                $scope.closedCases = res[0].data;

                                                $scope.closedCasesTable = new NgTableParams(
                                                        {
                                                            count: 50
                                                                    // count per page
                                                        },
                                                {
                                                    dataset: $scope.closedCases
                                                });

                                            });
                        };

                        $scope.refreshDeamonActive = false;

                        $scope.refreshDeamon = function () {
                            $timeout(function () {
                                $scope.fetchAllCases('refresh');
                                $scope.fetchCompletedCases('refresh');
                                $scope.fetchClosedCases('refresh');
                            }, 60000);
                        };

                        $scope
                                .fetchCompletedCases("Fetching data. Please wait!");

                        $scope.reopenCaseForQC = function (qcCase) {
                            $scope.currentCase = qcCase;

                            $("#manual-qc-container").modal('show');
                            $("#manual-qc-container .modal-body")
                                    .html(
                                            $compile(
                                                    "<reopen-case-qc inspection-case='currentCase' callback='reopenCaseCallback()'></reopen-case-qc>")
                                            ($scope));

                        };

                        $scope.reopenCaseCallback = function () {
                            $("#manual-qc-container").modal('hide');
                            $scope
                                    .fetchCompletedCases("Fetching data. Please wait!");
                        };

                        $scope.allRemarks = [{
                                id: "recommended",
                                title: "Recommended"
                            }, {
                                id: "not-recommended",
                                title: "Not Recommended"
                            }, {
                                id: "underwriter",
                                title: "Underwriter"
                            }];

                        $scope.showComments = function (data) {

                            $scope.selectedCaseForComment = data;

                            $("#manual-qc-container3").modal('show');
                            $("#manual-qc-container3 .modal-body")
                                    .html(
                                            $compile(
                                                    "<case-comments inspection-case='selectedCaseForComment'></case-comments>")
                                            ($scope));
                        };

                        $scope.reopenCase = function (qcCase) {
                            $scope.currentCase = qcCase;

                            $("#reopen-case-container").modal('show');
                            $("#reopen-case-container .modal-body")
                                    .html(
                                            $compile(
                                                    "<reopen-case inspection-case='currentCase' callback='reopenCaseCallback()'></reopen-case>")
                                            ($scope));
                        }

                        $scope.sendReport = function (qcCase) {
                            ajaxService
                                    .sendReport({
                                        case_id: qcCase
                                    }, "Mailing report. Please wait!")
                                    .then(
                                            function () {
                                                alert("Report sent successfully!");
                                            },
                                            function () {
                                                alert("Unable to send report. Please try again later!");
                                            });
                        }

                        $scope.recommendations = [{
                                id: "recommended",
                                value: "Recommended"
                            }, {
                                id: "not-recommended",
                                value: "Not Recommended"
                            }, {
                                id: "underwriter",
                                value: "Reffered to underwriter"
                            }];

                        $scope.updateCaseStatus = function (qcCase) {
                            $scope.currentCase = qcCase;
                            $scope.recommendation = qcCase.remark;
                            $scope.remarks = "";

                            $("#update-case-container").modal('show');
                        }

                        $scope.submitReport = function () {
                            ajaxService
                                    .updateInspectionReport(
                                            {
                                                case_id: $scope.currentCase.id,
                                                recommendation: $scope.recommendation,
                                                remarks: $scope.remarks
                                            },
                                    "Updating case status. Please wait!")
                                    .then(
                                            function () {
                                                alert("Updated successfully");
                                                $scope.currentCase.remark = $scope.recommendation;
                                                $("#update-case-container")
                                                        .modal('hide');
                                            },
                                            function () {
                                                alert("Unable to update. Please try again later!");
                                                $("#update-case-container")
                                                        .modal('hide');
                                            })
                        };

                        $scope.getOTP = function (customerPhoneNumber) {
                            ajaxService.getUnexpiredOTP({
                                username: customerPhoneNumber
                            }, "Fetching data. Please wait!").then(
                                    function (result) {
                                        var data = result.data;
                                        $scope.otps = data;
                                        $("#modal-n").modal('toggle');
                                    }, function (err) {
                                console.log(err);
                                alert(err.data.error_message);
                            });
                        };
                        $scope.newCaseData = {};

                        $scope.editCase = function (id) {
                            $scope.editCaseId = id;
                            $scope.currentCase = getCaseById(id);

                            var timestamp = moment
                                    .unix($scope.currentCase.inspectionTime / 1000);
                            $scope.currentCase.inspectionTimeModel = timestamp;
                            $scope.currentCase.inspectionTime = (timestamp
                                    .format("YYYY/MM/DD HH:mm:ss"));

                            $scope.newCaseData.newCustomerPhoneNumber = $scope.currentCase.customerPhoneNumber;
                            $scope.newCaseData.newRequestorPhoneNumber = $scope.currentCase.requestorPhoneNumber;
                            $scope.newCaseData.newVehicleNumber = $scope.currentCase.vehicleNumber;
                            $("#modal-2").modal('toggle');
                        };
                        $scope.cancelCase = function () {

                            $("#modal-2").modal('hide');
                            ajaxService.cancelCase({case_id: $scope.currentCase.id}, 'Cancelling Case. Please Wait...')
                                    .then(function (res) {
                                        alert('Case Cancelled!');
                                        $scope.fetchScheduledCases("Updating Case. Please wait!");
                                    })
                                    .catch(function (e) {
                                        console.error('Case Cancellation Error', e);
                                    })
                        };

                        $scope.rescheduleCaseCallback = function () {
                            console.log('rescheduleCaseCallback called');
                            $("#modal-2").modal('hide');
                            $scope.fetchScheduledCases("Updating Case. Please wait!");
                        };

                        $scope.closeCaseCallback = function () {
                            console.log('rescheduleCaseCallback called');
                            $("#modal-2").modal('hide');
                            $scope.fetchScheduledCases("Updating Case. Please wait!");
                        };
                        $scope.updateCaseDetails = function () {
                            var dataCPN = {
                                case_id: $scope.editCaseId,
                                customer_phone_number: $scope.newCaseData.newCustomerPhoneNumber
                            }
                            var dataRPN = {
                                case_id: $scope.editCaseId,
                                phone_number: $scope.newCaseData.newRequestorPhoneNumber
                            }
                            var dataVN = {
                                case_id: $scope.editCaseId,
                                vehicle_number: $scope.newCaseData.newVehicleNumber
                            }
                            console.log('=================>***********', dataVN, dataCPN, dataRPN);
                            $q.all([
                                ajaxService.updateCustomerPhoneNumber(dataCPN),
                                ajaxService.updateAgentPhoneNumber(dataRPN),
                                ajaxService.updateVehicleNumber(dataVN)
                            ])
                                    .then(function (res) {
                                        alert('Case Details Updated!');
                                        $scope.fetchScheduledCases('refresh');
                                    })
                                    .catch(function () {
                                        alert('Error in Case Details!');
                                    });

                        };
                        function getCaseById(id) {
                            for (var i = 0; i < $scope.data.length; i++) {
                                if ($scope.data[i].id === id)
                                    return $scope.data[i];
                            }
                            return null;
                        }
                        $scope.moveToQC = function (ic) {
                            $scope.selectedCaseForQC = ic;
                            $("#move-to-qc").modal('show');
                            $("#move-to-qc .modal-title").html(
                                    'Move case ' + ic + " to QC!");
                        };

                        $scope.moveToQCSubmit = function () {
                            ajaxService
                                    .moveToQC(
                                            {
                                                case_id: $scope.selectedCaseForQC,
                                                vehicle_type: $scope.selectedCaseForQCVehicleType
                                            },
                                    "Moving case "
                                            + $scope.selectedCaseForQC
                                            + " to QC. Please wait!")
                                    .then(function () {
                                        $("#move-to-qc").modal('hide');
                                        $(".modal-backdrop").hide();
                                        alert("Successfully moved to QC!");

                                        window.location.href = '#/qc';
                                    }, function (a) {
                                        console.log(a);
                                        alert("Unable to update!");
                                    })
                        };

                        $scope.uploadInspectionFileModel = function (data) {
                            $scope.selectedCaseForManualQc = data;

                            $("#manual-qc-container2").modal('show');
                        };

                        $scope.uploadFileManually = function () {
                            var file = document.getElementById('manualUploadFile').files[0];
                            var formData = new FormData();
                            formData.append('case_file', file);
                            formData.append('case_id', $scope.selectedCaseForManualQc.id);

                            console.log(formData)

                            $q.all([
                                ajaxService.uploadInspectionFile(formData, "Uploading file. Please wait!"),
                            ]).then(
                                    function () {
                                        ajaxService.reupload($scope.selectedCaseForManualQc.id, "Uploading file. Please wait!");
                                        alert("File uploaded successfully!");
                                        $("#manual-qc-container2").modal('hide');
                                        $location.url('/qc');
                                    }, function (err) {

                                if (err.data.error_message)
                                    alert(err.data.error_message);
                                else
                                    alert("Unable to upload file!");
                                console.log(err);
                            });
                        };
                        $scope.sendCaseCreatedSMS = function (caseId) {
                            ajaxService
                                    .sendCaseCreatedSMS(
                                            {
                                                case_id: caseId
                                            },
                                    "Sending message, Please wait...")
                                    .then(
                                            function () {
                                                alert("Message sent successfully");
                                            },
                                            function (err) {
                                                console.log(err);
                                                alert(err.data);
                                            });
                        };
                    }]);
