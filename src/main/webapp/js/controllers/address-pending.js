jaduApp.controller('AddressPendingController',['$scope', '$timeout', '$location', '$compile', 'ajaxService', '$q', 'gmap',  function($scope, $timeout, $location, $compile, ajaxService, $q, gmap) {

  $scope.editCaseId = undefined;
  $scope.editCaseIndex = undefined;
  $scope.currentCase = undefined;
  $scope.addressCases = undefined;
  $scope.map = undefined;
  $scope.newCaseDetails = {
    customerPhoneNumber: undefined,
    requestorPhoneNumber: undefined,
    vehicleNumber: undefined
  };

  $scope.flagCalculator = function(){
    $scope.currentTime = moment((moment().unix())*1000);
      for(i=0;i<$scope.addressCases.length;i++){
        if($scope.addressCases[i].inspectionStartTime == null){//insepection not started
          var insTime = moment($scope.addressCases[i].inspectionTime);
          var diff = $scope.currentTime.diff(insTime, 'minutes', true);
          if(diff >= 15 && diff < 30){
            $scope.addressCases[i].flagStatus = 'b-status-orange';
          }
          else
          if(diff >= 30){
            $scope.addressCases[i].flagStatus = 'a-status-red';
          }
          else{
            $scope.addressCases[i].flagStatus = '';
          }
        }
        else{
          var insStartTime = moment($scope.addressCases[i].inspectionStartTime);

          var diff = $scope.currentTime.diff(insStartTime, 'minutes', true);
          if(diff >= 15 && diff < 30){
            $scope.addressCases[i].flagStatus = 'b-status-orange';
          }
          else
          if(diff >= 30){
            $scope.addressCases[i].flagStatus = 'a-status-red';
          }
          else{
            $scope.addressCases[i].flagStatus = '';
          }
        }
    }

    $timeout(function(){
      $scope.flagCalculator();
    },6000)

  }

  $scope.refreshDeamon = function(){
    $timeout(function(){
      $scope.getCases('refresh');
    },60000)
  }

  $scope.getCases = function(spinnerText){
    $q.all([
      ajaxService.getAllocationRequestedCases(null, spinnerText)
    ])
    .then(function(res){
      // console.log(res);
      $scope.addressCases = res[0].data;
      $scope.flagCalculator();
      // $scope.refreshDeamon();
    });
  };

  $scope.editCase = function(id, index){
    $scope.editCaseId = id;
    $scope.editCaseIndex = index;
    $scope.currentCase = $scope.addressCases[index];
    console.log($scope.currentCase);
    $scope.newCaseDetails.customerPhoneNumber = $scope.currentCase.customerPhoneNumber;
    $scope.newCaseDetails.requestorPhoneNumber = $scope.currentCase.requestorPhoneNumber;
    $scope.newCaseDetails.vehicleNumber = $scope.currentCase.vehicleNumber;

    $scope.initializeGoogleMap(null, null);
    $("#modal-address-pending").modal('toggle');
  };

  $scope.updateCaseDetails = function(){
    var dataCPN = {
      case_id:$scope.editCaseId,
      customer_phone_number:$scope.newCaseDetails.customerPhoneNumber
    }
    var dataRPN = {
      case_id:$scope.editCaseId,
      phone_number:$scope.newCaseDetails.requestorPhoneNumber
    }
    var dataVN = {
      case_id:$scope.editCaseId,
      vehicle_number:$scope.newCaseDetails.vehicleNumber
    }
    $q.all([
      ajaxService.updateCustomerPhoneNumber(dataCPN),
      ajaxService.updateAgentPhoneNumber(dataRPN),
      ajaxService.updateVehicleNumber(dataVN)
    ])
    .then(function(res){
      alert('Case Details Updated!');
      $scope.addressCases[$scope.editCaseIndex].customerPhoneNumber = $scope.newCaseDetails.customerPhoneNumber;
      $scope.addressCases[$scope.editCaseIndex].requestorPhoneNumber = $scope.newCaseDetails.requestorPhoneNumber;
      $scope.addressCases[$scope.editCaseIndex].vehicleNumber = $scope.newCaseDetails.vehicleNumber;
      $("#modal-address-pending").modal('hide');

    })
    .catch(function(){
      alert('Error in Case Details!');
    });

  };

  $scope.closeCaseCallback = function(){
    $("#modal-address-pending").modal('hide');
    $scope.getCases("Fetching cases. Please wait!");
  };

  angular.element(document).ready(function () {
    $scope.getCases("Fetching cases. Please wait!");
  });

  $scope.submitAddress = function(){
    var myLatlng = gmap.getCurrentPosition()
    console.log('Address ',document.getElementById('address-map-input').value);
    console.log('Location ',myLatlng);
    $("#modal-address-pending").modal('hide');
  }

  //map
  $scope.initializeGoogleMap = function(lat, lng){
    var mapElement = document.getElementById('address-map');
    var mapInputElement = document.getElementById('address-map-input');

    $scope.map = gmap.initMap(mapElement, lat, lng);
    gmap.addInputField(mapInputElement, $scope.map);
  }

}]);
