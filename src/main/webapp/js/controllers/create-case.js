jaduApp.controller('CreateCaseController',['$scope', '$rootScope', '$timeout', '$location', '$compile', 'ajaxService', '$q', 'GoogleMaps', '$log',   function($scope, $rootScope, $timeout, $location, $compile, ajaxService, $q, GoogleMaps, $log) {

    $scope.vehicleNumberRegex = '/^(([A-Z]){2,3}(?:[0-9]){1,2}(?:[A-Z]){1,2}([0-9]){1,4})$/';//not using

    $scope.createCaseModel = {
      purpose_of_inspection :  undefined,
      customer_name : undefined,
      customer_phone_number : undefined,
      self_inspect : true,
      inspection_type : undefined,
      vehicle_number : undefined,
      agent_id : undefined,
      agent_phone_number : undefined,
      inspection_time : undefined
    };

    $scope.resetCase = function(){
      $scope.createCaseModel = {
        purpose_of_inspection :  undefined,
        customer_name : undefined,
        customer_phone_number : undefined,
        self_inspect : true,
        inspection_type : undefined,
        vehicle_number : undefined,
        agent_id : undefined,
        agent_phone_number : undefined,
        inspection_time : undefined
      };
      $scope.setInspectionType();

    }

    $scope.setInspectionType = function(){
      $scope.inspectionTypes.forEach(function(inspectionType, index){
        if(inspectionType.isDefault){
          $scope.createCaseModel.inspection_type = inspectionType.id;
        }
      });
    }

    $scope.createCase = function(){
      var latlng = GoogleMaps.getLatLong();
      $scope.createCaseModel.latitude = typeof latlng.lat  === "function" ? latlng.lat() :latlng.lat;
      $scope.createCaseModel.longitude = typeof latlng.lng  === "function" ? latlng.lng() : latlng.lng;

      ajaxService.getAgentByPhoneNumber($scope.createCaseModel.agent_phone_number, "Validating. Please wait!")
      .then(function(res){
        // console.log('getAgentByPhoneNumber', res);
          $scope.createCaseModel.agent_id = res.data.username;
          ajaxService.createCase($scope.createCaseModel, "Creating case. Please wait!")
          .then(function(res){
              alert("Case has been created successfully!");
              if($scope.createCaseModel.inspection_type == 'ASSIGN_TO_INSPECTOR')
                $location.url('/address-pending');
              else
                $location.url('/scheduled-cases');

              $scope.resetCase();

          }, function(err){
              alert("Unable to create case!",err);
          });

      }, function(){
          alert("Agent doesn't exists with given phone number!");
      });
    };

    angular.element(document).ready(function () {

      $q.all([
          ajaxService.getPurposeOfInspections(null, "Fetching data. Please wait!"),
          ajaxService.getAllAgents(null, "Fetching data. Please wait!"),
          ajaxService.getInspectionTypes()
      ]).then(function(res){
            $scope.poi = res[0].data;
            $scope.allAgents = res[1].data;
            console.log($scope.allAgents)
            $scope.inspectionTypes = res[2].data;

            $scope.setInspectionType();
            $scope.initializeAgentAutoComplete();
            GoogleMaps.getCurrentLocation(function(){
              GoogleMaps.addMap(null, null, "create-case-map");
              GoogleMaps.addInputField(document.getElementById('pac-input-create-case'));

            },
            function(){
              GoogleMaps.addMap(null, null, "create-case-map");
              GoogleMaps.addInputField(document.getElementById('pac-input-create-case'), function(){
            });

          });
      });
    });

    $scope.verifyAgentContactActive = false;
    $scope.verifyAgentContactStatus = false;
    $scope.verifyAgentContactActiveClass = '';
    $scope.verifyFlag = 0;

    $scope.verifyAgentContact = function(){
      if(!$scope.verifyAgentContactActive && $scope.createCaseModel.agent_phone_number.length==10 && !$scope.verifyAgentContactStatus){
        $scope.verifyAgentContactActive = true;
        ajaxService.getAgentByPhoneNumber($scope.createCaseModel.agent_phone_number, "Verifyring Agent Contact. Please wait!")
        .then(function(data){
          $scope.verifyFlag++;
          $scope.createCaseModel.agent_id = data.username;
          $scope.verifyAgentContactActive = false;
          $scope.verifyAgentContactStatus = true;
        },
        function(){
          $scope.verifyFlag++;
          $scope.verifyAgentContactActive = false;
          $scope.verifyAgentContactStatus = false;
        }
        )
      }
    }

    $scope.checkAgentContact = function(){
      if($scope.createCaseModel.agent_phone_number != undefined){
        if($scope.createCaseModel.agent_phone_number.length == 10 && !$scope.verifyAgentContactActive){
          $scope.verifyAgentContact();
        }
        else{
          $scope.verifyFlag = 0;
          $scope.verifyAgentContactStatus = false;
        }
      }
    };

    //auto-complete below
      var self = $scope;
      self.agents = "";

      $scope.initializeAgentAutoComplete = function(){
        self.agents = loadAll();
        self.simulateQuery = false;
        self.isDisabled    = false;

        self.querySearch   = querySearch;
        self.selectedItemChange = selectedItemChange;
        self.searchTextChange   = searchTextChange;

        self.newState = newState;
      }

      function newState(state) {
        alert("Sorry! No data present for entered query!");
      }

      function querySearch (query) {
        var results = query ? self.agents.filter( createFilterFor(query) ) : self.agents,
            deferred;
        if (self.simulateQuery) {
          deferred = $q.defer();
          $timeout(function () { deferred.resolve( results ); }, Math.random() * 1000, false);
          return deferred.promise;
        } else {
          return results;
        }
      }

      function searchTextChange(text) {
        // $log.info('Text changed to ' + text);
      }

      function selectedItemChange(item) {
        if(item != undefined)
          $scope.createCaseModel.agent_phone_number = item.value;
        // $log.info('Item changed to ' + JSON.stringify(item));
      }

      function loadAll() {
        var agents = [];
        $scope.allAgents.forEach(function(agent, index){
          agents.push(agent);
        });

        return agents.map( function (agent) {
          return {
            value: agent,
            display: agent
          };
        });
      }

      function createFilterFor(query) {
        var lowercaseQuery = angular.lowercase(query);
        return function filterFn(agent) {
          return (agent.value.indexOf(lowercaseQuery) === 0);
        };
      }


}]);
