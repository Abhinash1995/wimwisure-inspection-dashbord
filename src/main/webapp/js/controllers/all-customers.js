jaduApp
		.controller(
				'AllCustomersController',
				[
						'$scope',
						'$timeout',
						'$location',
						'$compile',
						'$auth',
						'$q',
						'ajaxService',
						function($scope, $timeout, $location, $compile, $auth,
								$q, ajaxService) {

							$scope.customerList = [];
							$scope.getCustomerDetailsModel = {
								contact : "",
								emailId : ""
							};

							$scope.clearValues = function() {
								$scope.getCustomerDetailsModel = {
									contact : "",
									emailId : ""
								};
							}

							$scope.getCustomerDetails = function() {
								var fieldName = '';
								var fieldValue = "";
								// To get form value and name
								if ($scope.getCustomerDetailsModel.contact != "") {
									fieldName = "contact";
									fieldValue = $scope.getCustomerDetailsModel.contact;
								} else if ($scope.getCustomerDetailsModel.emailId != "") {
									fieldName = "emailId";
									fieldValue = $scope.getCustomerDetailsModel.emailId;
								}

								ajaxService.getReqCustomerDetails({
									field : fieldName,
									value : fieldValue
								}, "Fetching details. Please wait!").then(
										function(res) {
											$scope.customerList = res.data;
											$scope.clearValues();
										}, function(err) {
											alert("No results found");
											$scope.clearValues();
										});
							}
							/*
							 * ajaxService.getAllCustomers({},'Loading Customer
							 * Data....') .then(function(res){
							 * $scope.customerList = res.data; })
							 * .catch(function(e){ alert("Unable to fetch
							 * Customer data!"); console.error('All customer
							 * fetch error', e); })
							 */

							$scope.deleteCustomer = function(customer) {

								ajaxService
										.deleteCustomer({
											phonenumber : customer,
											role : "ROLE_CUSTOMER",
											secretKey : "magic@W!mwisure"
										}, "Deleting customer. Please wait!")
										.then(
												function(res) {
													alert("Successfully Deleted!");
													window.location.href = '#/all-customers';
												}, function(err) {
													alert("Something went wrong");
													$scope.clearValues();
												});
							};
							$scope.showCustomerActionsModel = function(agent) {
								console.log(agent);
								$scope.currentAgent = agent;
								$('#agent-actions').modal('show');
								$('#agent-actions .modal-body')
										.html(
												$compile(
														"<user-actions  user='currentAgent' callback='updateCallback()'></user-actions>")
														($scope));
							};

							$scope.updateCallback = function() {
								$('#agent-actions').modal('hide');
							};

							$scope.goToCreateCase = function() {
								$location.url('/create-case');
							};

						} ]);
