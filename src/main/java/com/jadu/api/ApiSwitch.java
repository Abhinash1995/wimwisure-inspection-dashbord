/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jadu.api;

import com.jadu.model.CompanyBranchDivision;
import com.jadu.model.InspectionCase;
import com.jadu.model.OnlineCases;

/**
 *
 * @author gkumar
 */
public enum ApiSwitch {

    IFFCO_CENTRAL_OFFICE {
                @Override
                public ThirdPartyApi getApi() {
                    return new IffcoCentralOffice();
                }

                @Override
                public boolean qualify(CompanyBranchDivision cbd, InspectionCase ic, OnlineCases oc) {
                    return cbd.getId() == 322921 && oc != null;
                }
            },
    IFFCO_REGISTERED_OFFICE {
                @Override
                public ThirdPartyApi getApi() {
                    return new IffcoRegisteredOffice();
                }

                @Override
                public boolean qualify(CompanyBranchDivision cbd, InspectionCase ic, OnlineCases oc) {
                    return cbd.getId() == 21168 && oc != null;
                }
            },
    POLICY_BACHAT {
                @Override
                public ThirdPartyApi getApi() {
                    return new PolicyBachat();
                }

                @Override
                public boolean qualify(CompanyBranchDivision cbd, InspectionCase ic, OnlineCases oc) {
                    return cbd.getCompany().equals("Policybachat") && oc != null;
                }

            },
    POLICY_BOSS {
                @Override
                public ThirdPartyApi getApi() {
                    return new Policyboss();
                }

                @Override
                public boolean qualify(CompanyBranchDivision cbd, InspectionCase ic, OnlineCases oc) {
                    return cbd.getBranch().equals("Policyboss") && oc != null;
                }

            },
    SQUARE_INSURANCE {
                @Override
                public ThirdPartyApi getApi() {
                    return new SquareInsurance();
                }

                @Override
                public boolean qualify(CompanyBranchDivision cbd, InspectionCase ic, OnlineCases oc) {
                    return cbd.getBranch().equals("Square Insurance") && oc != null;
                }

            };

    public abstract ThirdPartyApi getApi();

    public abstract boolean qualify(CompanyBranchDivision cbd, InspectionCase ic, OnlineCases oc);
}
