/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jadu.api;

import com.jadu.model.CaseType;
import com.jadu.model.InspectionCase;
import com.jadu.service.CallbackService;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.codec.binary.Base64;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

/**
 *
 * @author gkumar
 */

public class IffcoRegisteredOffice extends ThirdPartyApi{
    
    HttpClient client;
    HttpPost httpPost;
    HttpResponse response;

    public IffcoRegisteredOffice() {
        super();
    }

    @Override
    public void execute() {
        
        client = HttpClientBuilder.create().build();
        httpPost = new HttpPost(appConfigService.getProperty("IFFCO_ONLINE_CASE_STATUS_API","https://staging.iffcotokio.co.in/uatportal/breakin/api/approve"));
        
        try {
            this.setHeader();
            this.setPayload();
            response = client.execute(httpPost);
            System.out.println(response);
            System.out.println(EntityUtils.toString(response.getEntity()));
        } catch (IOException ex) {
            Logger.getLogger(IffcoRegisteredOffice.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void setHeader() {
        String auth = appConfigService.getProperty("IFFCO_CASE_STATUS_API_BASIC_AUTHORIZATION_USER_NAME","wimwisure") + ":"+ appConfigService.getProperty("IFFCO_CASE_STATUS_API_BASIC_AUTHORIZATION_PASSWORD","Tfem6x2OBNs/H8eh9sapbQ==");
	String authHeader = "Basic " + new String(Base64.encodeBase64(auth.getBytes(StandardCharsets.ISO_8859_1)));
			
        httpPost.setHeader("Content-Type", "application/json");
        httpPost.setHeader("apikey", appConfigService.getProperty("IFFCO_CASE_STATUS_API_KEY", "breakin"));
        httpPost.setHeader(HttpHeaders.AUTHORIZATION, authHeader);
    }

    public void setPayload() {
        try {
            JSONObject result = new JSONObject();
            result.put("quoteId",           onlineCase.getQuoteNumber());
            result.put("caseId",            ic.getId());
            result.put("status",            convertStatus(ic));
            result.put("inspectionDate",    formatDate(ic));
            result.put("inspectionNo",      ic.getId());
            result.put("chassisNumber",     ic.getChassisNumber());
            result.put("engineNumber",      ic.getEngineNumber());
            result.put("inspectionAddress", googleLocationService.getLocalityByLatLng(ic.getInspectionLatitude(), ic.getInspectionLongitude()));
            result.put("latitude",          ic.getInspectionLatitude());
            result.put("longitude",         ic.getInspectionLongitude());
            
            StringEntity data = new StringEntity(result.toString());
            httpPost.setEntity(data);
        } catch (JSONException | UnsupportedEncodingException ex) {
            Logger.getLogger(IffcoRegisteredOffice.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(IffcoRegisteredOffice.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(IffcoRegisteredOffice.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public boolean isQualified() {
        return (ic.getRemark().equals("recommended") || ic.getRemark().equals("not-recommended") ) && ic.getCaseType() == CaseType.ONLINE;
    }  
    
    private static String convertStatus(InspectionCase ic) throws Exception {
        if(ic.getCurrentStage() == -1) 
            return "CLOSED";
        
        String remark = ic.getRemark();
        if (remark != null && remark.equalsIgnoreCase("recommended")) {
            return "APPROVED";
        } else if (remark != null && remark.equalsIgnoreCase("not-recommended")) {
            return "REJECTED";
        }
        throw new Exception("Invalid Status");
    }
    
    private static String formatDate(InspectionCase cases) {
        String pattern = "dd-MM-YYYY";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        return simpleDateFormat.format(cases.getQcTime());
    }
}
