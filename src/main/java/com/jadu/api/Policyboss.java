/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jadu.api;

import com.jadu.model.InspectionCase;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import okhttp3.Credentials;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author gkumar
 */
public class Policyboss extends ThirdPartyApi{

    @Override
    public void execute() {
        try {
            
            String downloadKey = ("https://iffco.wimwisure.com/util/cases/download-uploaded-photos/" + ic.getDownloadKey());
            
            OkHttpClient client = new OkHttpClient();
            
            JSONObject payload = new JSONObject();
            payload.put("reportDownloadLink", downloadKey);
            payload.put("caseId", ic.getId());
            payload.put("inspectionDate", ic.getQcTime().toString());
            payload.put("branch", ic.getCompanyBranchDivision().getBranch());
            payload.put("vehicleRegNo", ic.getVehicleNumber());
            payload.put("status", convertStatus(ic));
            payload.put("client_key", "CLIENT-CNTP6NYE-CU9N-DUZW-CSPI-SH1IS4DOVHB9");
            payload.put("secret_key", "SECRET-HZ07QRWY-JIBT-XRMQ-ZP95-J0RWP3DYRACW");

            MediaType mediaType = MediaType.parse("application/json");
            RequestBody body = RequestBody.create(mediaType, payload.toString());
            Request request = new Request.Builder()
              .url("http://horizon.policyboss.com:5000/inspection/iffco_inspection_status")
              .post(body)
              .addHeader("Content-Type", "application/json")
              .addHeader("cache-control", "no-cache")
              .addHeader("Postman-Token", "aa0be086-fab0-4068-9995-24834566af47")
              .build();

            Response response = client.newCall(request).execute();
            System.out.println(response);
        } catch (JSONException ex) {
            Logger.getLogger(PolicyBachat.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(PolicyBachat.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(PolicyBachat.class.getName()).log(Level.SEVERE, null, ex);
        }
    
    }
    
    private static String convertStatus(InspectionCase ic) throws Exception {
        if(ic.getCurrentStage() == -1) 
            return "CLOSED";
        
        String remark = ic.getRemark();
        if (remark != null && remark.equalsIgnoreCase("recommended")) {
            return "APPROVED";
        } else if (remark != null && remark.equalsIgnoreCase("not-recommended")) {
            return "REJECTED";
        }
        throw new Exception("Invalid Status");
    }

    @Override
    public boolean isQualified() {
        return (ic.getRemark().equals("recommended") || ic.getRemark().equals("not-recommended") || ic.getRemark().equals("underwriter") );
    }
    
}
