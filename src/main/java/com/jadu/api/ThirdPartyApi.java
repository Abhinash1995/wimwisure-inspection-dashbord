/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jadu.api;

import com.jadu.dao.OnlineCasesDAO;
import com.jadu.model.CasePhoto;
import com.jadu.model.InspectionCase;
import com.jadu.model.OnlineCases;
import com.jadu.service.AppConfigService;
import com.jadu.service.GoogleLocationService;
import com.jadu.service.S3Service;
import java.util.Date;
import java.util.List;

/**
 *
 * @author gkumar
 */
public abstract class ThirdPartyApi {

    InspectionCase ic;
    OnlineCases onlineCase;
    OnlineCasesDAO onlineCasesDAO;
    AppConfigService appConfigService;
    GoogleLocationService googleLocationService;
    List<CasePhoto> casePhotos;
    S3Service s3Service;

    public void call(
            InspectionCase ic,
            OnlineCases onlineCase,
            OnlineCasesDAO onlineCasesDAO,
            AppConfigService appConfigService,
            GoogleLocationService googleLocationService,
            List<CasePhoto> casePhotos,
            S3Service s3Service
    ) {
        this.ic = ic;
        this.onlineCase = onlineCase;
        this.onlineCasesDAO = onlineCasesDAO;
        this.appConfigService = appConfigService;
        this.googleLocationService = googleLocationService;
        this.casePhotos = casePhotos;
        this.s3Service = s3Service;
        
        if(isQualified()){

            try {
                this.execute();
                this.postProcess("SUCCESS");
            } catch (Exception ex) {
                this.postProcess("FAILED");
            }
        }    
    }

    public abstract void execute();

    public abstract boolean isQualified();

    public void postProcess(String statusMessage) {
        onlineCase.setStatusMessage(statusMessage);
        onlineCase.setStatusUpdated(true);
        onlineCase.setUpdatedDate(new Date());
        onlineCasesDAO.saveOrUpdate(onlineCase);
    }
}
