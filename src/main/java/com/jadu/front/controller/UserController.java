package com.jadu.front.controller;

import java.io.IOException;
import java.util.List;

import javax.mail.MessagingException;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.jadu.dao.AndroidDeviceDAO;
import com.jadu.dao.UserDAOImpl;
import com.jadu.dao.UserNotificationDAO;
import com.jadu.dao.WebDeviceDAO;
import com.jadu.error.handler.ErrorHandler;
import com.jadu.helpers.UtilHelper;
import com.jadu.model.AndroidDevice;
import com.jadu.model.Device;
import com.jadu.model.WebDevice;
import com.jadu.service.MailService;

import freemarker.template.TemplateException;

@RestController
@PropertySource("/WEB-INF/config.properties")
@RequestMapping("/user")
public class UserController {

	@Autowired
	UserDAOImpl userDAO;

	@Autowired
	UserNotificationDAO userNotificationDAO;

	@Autowired
	AndroidDeviceDAO androidDeviceDAO;

	@Autowired
	WebDeviceDAO webDeviceDAO;

	@Autowired
	private MailService mailService;

	@RequestMapping(value = "/check-access-token-validity", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public void checkAccessTokenValidity() {
	}

	@RequestMapping(value = "/profile-details", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public Object checkAgentDetails() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		User p = (User) authentication.getPrincipal();
		return userDAO.getUserByUsername(p.getUsername());
	}

	@RequestMapping(value = "/reset-password-link", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public void sendResetLink() throws Exception {

		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		User p = (User) authentication.getPrincipal();
		com.jadu.model.User currentUser = userDAO.getUserByUsername(p.getUsername());
		mailService.resetPasswordLink(currentUser, "Reset password");
	}

	@RequestMapping(value = "/change-password", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public void resetPassword(@RequestParam(value = "current_password", required = true) String currentPassword,
			@RequestParam(value = "new_password", required = true) String newPassword) throws Exception {

		if (newPassword == null || newPassword.length() < 6)
			throw new Exception("Password should be of minimum 6 characters!");
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		User p = (User) authentication.getPrincipal();
		com.jadu.model.User currentUser = userDAO.getUserByUsername(p.getUsername());
		if (!currentUser.getPassword().equals(currentPassword))
			throw new Exception("Invalid current password!");
		userDAO.updatePassword(currentUser.getUsername(), newPassword);
	}

	@RequestMapping(value = "/reset-password", method = RequestMethod.POST, produces = MediaType.TEXT_HTML_VALUE)
	@ResponseBody
	public String changePassword(@RequestParam(value = "username", required = true) String username,
			@RequestParam(value = "currentPassword", required = true) String currentPassword,
			@RequestParam(value = "newPassword", required = true) String newPassword,
			@RequestParam(value = "newPassword2", required = true) String newPassword2) throws Exception {

		if (newPassword == null || newPassword.length() < 6 || !newPassword.equals(newPassword2))
			throw new Exception("Password should be same and of minimum 6 characters! ");
		List<com.jadu.model.User> userList = userDAO.getUserByPhoneNumber(username);
		com.jadu.model.User currentUser = userList != null && !userList.isEmpty() ? userList.get(0) : null;
		if (currentUser == null || !currentUser.getPassword().equals(currentPassword)
				|| currentPassword.equalsIgnoreCase(newPassword))
			throw new Exception("Invalid current password!");
		userDAO.updatePassword(currentUser.getUsername(), newPassword);
		return "<!DOCTYPE HTML>\r\n" + "<html>\r\n" + "<head>\r\n" + "    <title>Reset password : Success </title>\r\n"
				+ "    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />\r\n" + "</head>\r\n"
				+ "<body>\r\n" + "    <p>Password has been changed, click  <a href=\"/login\">here</a> to login</p>\r\n"
				+ "</body>\r\n" + "</html>";
	}

	@RequestMapping(value = "/web/change-password", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public void changePassword(@RequestParam(value = "new_password", required = true) String newPassword,
			@RequestParam(value = "confirm_password", required = true) String confirm_password

	) throws Exception {

		if (newPassword == null || newPassword.length() < 6)
			throw new Exception("Password should be of minimum 6 characters!");
		if (confirm_password.equalsIgnoreCase(newPassword)) {
			Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
			User p = (User) authentication.getPrincipal();
			com.jadu.model.User currentUser = userDAO.getUserByUsername(p.getUsername());
			userDAO.updatePassword(currentUser.getUsername(), newPassword);
		} else {
			throw new Exception("New password and Confirm password should be same");

		}
	}

	@RequestMapping(value = "/subscribe-web-device", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public void subscribeWebDevice(@RequestParam(value = "device_id", required = true) String deviceId)
			throws IOException, TemplateException, MessagingException {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		User p = (User) authentication.getPrincipal();
		com.jadu.model.User currentUser = userDAO.getUserByUsername(p.getUsername());
		webDeviceDAO.delete(p.getUsername());
		if (webDeviceDAO.get(deviceId, currentUser.getUsername()) == null) {
			Device device = new WebDevice();
			device.setDeviceId(deviceId);
			device.setUsername(currentUser.getUsername());
			device.setUpdateTime(UtilHelper.getDate());
			webDeviceDAO.save(device);
		}
	}

	@RequestMapping(value = "/subscribe-device", method = RequestMethod.POST, produces="application/json")
    @ResponseBody
    public void subscribe(
            @RequestParam(value = "device_id", required=true ) String deviceId,
            @RequestParam(value = "device_instance_id", required=true ) String deviceInstanceId,
            @RequestParam(value = "app_version", required=true ) String appVersionNumber
    ) throws IOException, TemplateException, MessagingException{
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User  p = (User)authentication.getPrincipal();
        com.jadu.model.User currentUser = userDAO.getUserByUsername(p.getUsername()); 
        androidDeviceDAO.delete(p.getUsername());
        if(androidDeviceDAO.get(deviceId, currentUser.getUsername()) == null){
        	AndroidDevice device = new AndroidDevice();
            device.setDeviceId(deviceId);
            device.setUsername(currentUser.getUsername());
            device.setDeviceInstanceId(deviceInstanceId);
            device.setAppVersionNumber(appVersionNumber);
            device.setUpdateTime(UtilHelper.getDate());
            androidDeviceDAO.save(device);
        }
    }

	@RequestMapping(value = "/unsubscribe-device", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public void unsubscribe(@RequestParam(value = "device_id", required = true) String deviceId)
			throws IOException, TemplateException, MessagingException {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		User p = (User) authentication.getPrincipal();
		com.jadu.model.User currentUser = userDAO.getUserByUsername(p.getUsername());
		Device device = androidDeviceDAO.get(deviceId, currentUser.getUsername());
		if (device != null) {
			androidDeviceDAO.delete(device);
		}
	}

	@RequestMapping(value = "/get-count-of-unread-notifications", method = RequestMethod.GET)
	@ResponseBody
	public Object getCountOfUnreadNotifications() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		User p = (User) authentication.getPrincipal();
		return userNotificationDAO.getCountOfUnreadNotifications(p.getUsername());
	}

	@RequestMapping(value = "/get-notifications", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public Object getNotifications() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		User p = (User) authentication.getPrincipal();
		return userNotificationDAO.getAll(p.getUsername());
	}

	@ExceptionHandler(Exception.class)
	public void handleAllException(Exception ex, HttpServletResponse response) throws IOException {
		ErrorHandler.handleError(ex, response);
	}
}
