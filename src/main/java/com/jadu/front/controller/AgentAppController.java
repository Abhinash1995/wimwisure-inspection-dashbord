/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jadu.front.controller;

import com.jadu.service.S3Service;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author gauta
 */
@RestController
public class AgentAppController {
    
    @Autowired
    S3Service s3Service;

    @RequestMapping(value = "/agentapp")
    public ModelAndView getIndex(
            HttpServletRequest request,
            HttpServletResponse response) throws Exception {
        return new ModelAndView("redirect:https://play.google.com/store/apps/details?id=com.proj.iffcotokio");

    }
    
    @RequestMapping(value = "/inspectiondemo")
    public ModelAndView getVideo(
            HttpServletRequest request,
            HttpServletResponse response) throws Exception {
        return new ModelAndView("redirect:https://youtu.be/1TeYam7d0EQ");

    }
}
