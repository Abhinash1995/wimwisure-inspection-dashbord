package com.jadu.front.controller;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Set;
import javax.mail.MessagingException;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.s3.transfer.MultipleFileDownload;
import com.itgi.web.service.DCTMWebServiceImpl;
import com.itgi.web.service.DCTMWebServiceImplServiceLocator;
import com.itgi.web.service.DCTMWebServiceResult;
import com.itgi.web.service.KeyValueMap;
import com.itgi.web.service.WebContent;
import com.jadu.api.ApiSwitch;
import com.jadu.bean.PolicyBachatBean;
import com.jadu.dao.AgentCommentDAO;
import com.jadu.dao.CaseCommentDAO;
import com.jadu.dao.CaseDAO;
import com.jadu.dao.CasePhotoDAO;
import com.jadu.dao.OnlineCasesDAO;
import com.jadu.dao.PhotoTypeDAO;
import com.jadu.dao.UserDAOImpl;
import com.jadu.dto.AgentCommentDTO;
import com.jadu.dto.CaseCommentDTO;
import com.jadu.dto.CaseMakeDTO;
import com.jadu.dto.CasePhotoDTO2;
import com.jadu.dto.ScheduledCasesDTO;
import com.jadu.error.handler.ErrorHandler;
import com.jadu.helpers.EncryptionHelper;
import com.jadu.helpers.UtilHelper;
import com.jadu.model.CasePhoto;
import com.jadu.model.CompanyBranchDivisionUser;
import com.jadu.model.InspectionCase;
import com.jadu.model.OnlineCases;
import com.jadu.model.PhotoType;
import com.jadu.model.User;
import com.jadu.pdf.create.GenerateReport;
import com.jadu.push.notification.AndroidPushNotificationsService;
import com.jadu.service.AppConfigService;
import com.jadu.service.CallbackService;
import com.jadu.service.CaseService;
import com.jadu.service.ConnectCustomerToFlow;
import com.jadu.service.GoogleLocationService;
import com.jadu.service.MailService;
import com.jadu.service.S3DirectUploadService;
import com.jadu.service.S3Service;
import com.jadu.service.SmsService;
import com.jadu.service.URLShortnerService;
import com.jadu.service.ZipUtility;
import com.siebel.CustomUI.ITGIMotorPreInspectionAgencyRecommendationResponse_Input;
import com.siebel.CustomUI.ITGIMotorPreInspectionAgencyRecommendationResponse_Output;
import com.siebel.CustomUI.ITGIMotorPreInspectionAgencyResponse;
import com.siebel.CustomUI.ITGI_spcMotor_spcPreInspection_spcAgency_spcResponseLocator;
import freemarker.template.TemplateException;
import java.rmi.RemoteException;
import javax.xml.rpc.ServiceException;

@RestController
@PropertySource("/WEB-INF/config.properties")
@RequestMapping("/jadu-test")
public class TestController {

    @Autowired
    AndroidPushNotificationsService androidPushNotificationsService;

    @Autowired
    CaseDAO caseDAO;

    @Autowired
    GenerateReport generateReportService;

    @Autowired
    UserDAOImpl userDAO;

    @Autowired
    MailService mailService;

    @Autowired
    SmsService smsService;

    @Autowired
    S3Service s3Service;

    @Autowired
    S3DirectUploadService s3DirectUploadService;

    @Autowired
    CaseService caseService;

    @Autowired
    URLShortnerService urlShortnerService;

    @Autowired
    ConnectCustomerToFlow connectCustomerToFlow;

    @Autowired
    Environment env;

    @Autowired
    CasePhotoDAO casePhotoDAO;

    @Autowired
    private CaseCommentDAO caseCommentDAO;

    @Autowired
    private AgentCommentDAO agentCommentDAO;

    @Autowired
    private PhotoTypeDAO photoTypeDAO;

    @Autowired
    private AppConfigService appConfigService;

    @Autowired
    private OnlineCasesDAO onlineCasesDAO;

    @Autowired
    private CallbackService callbackService;

    @Autowired
    private GoogleLocationService googleLocationService;

    private static final Logger LOGGER = LoggerFactory.getLogger(TestController.class);

    @RequestMapping(value = "/send-mail", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public boolean sendMail() throws IOException, TemplateException, MessagingException {
        User user = userDAO.getUserByPhoneOrEmail("7066908372");
        mailService.resetPasswordLink(user, "Reset password!");
        return true;
    }

    @RequestMapping(value = "/get-time", method = RequestMethod.GET)
    @ResponseBody
    public Object getTime() throws IOException, TemplateException, MessagingException {
        return UtilHelper.getDate().toString();
    }

    @RequestMapping(value = "/send-link", method = RequestMethod.GET)
    @ResponseBody
    public void sendLink() throws IOException, TemplateException, MessagingException, Exception {

        InspectionCase ic = (InspectionCase) caseDAO.getCaseById(1403);

        String url = urlShortnerService.shortenUrl("customer-inspection/", ic.getId(),
                ic.getInsuranceCompany().getName());
        smsService.sendSMSAsync(ic.getCustomerPhoneNumber(),
                "We have an insurance inspection request for " + ic.getVehicleNumber() + ". Please download the app "
                + url + " to complete the inspection. Call 7290049100 for support");
    }

    @RequestMapping(value = "/dddddd", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public String ddddd() throws IOException, TemplateException, MessagingException, Exception {
        return (EncryptionHelper.decrypt("AGZ6DBN7MKLEIY6B",
                "Ntg+51MaqPcs56CIfjE37DL+X4IeKL8SDqNExvkxljGh+yFx2mgTSp5C7tka7YDMmM4K9KO7LJYKoYq79eBPlaoIfJzCtCS6KtlXQyokEF1fNRWsm3gfeKd7B0BumapiDWAzQCWypH2tOcvJBDYdeyl07dc7Ou5sCy16QgecFHuygascvPTXy4Z2NSryRHBEI+K9jUumgOVe3+GTr68WVU8+/boDsMtCgENDXmrDbG16wSDIAeYT3L5IMhlk/jyQJIYAtaxS89Gr9pkqfVnxQgPnU7gF/22/Ess8hmXHERhMr9UBBbZa4AlUz2XkUamzvaCPKNxpkmGI4QnFCsXzVz3NXKqpdALAd4fpNvcUo724gL7zBNM1LUHngCr1Z/3h4ahpbpnoVrIqVnWtzNXYp+ijR90tEJ71i2e70IuHPMVEIVS4lDp7R3aQTQF3bTgC9lP4rj2bk5SIebSu48zXT9L6braNkjnA/Y9UCboZ1XmC7HiDYuobCoCYosjnNhgEEOp+2EBuldfgHbZKs1jTTi2aLGXzU0ZHtVEVgZVCQehTq0MR5szapzpSIogdXYQff2h7zFFfEQOdV1pD9VcXvJv6WeDP6vEDm12GrNpMDBJoTYl66LxrBH/YJ0EG2m9sPMl3R0N/x9gJtqXGX2RefhB5mv3HRy5u0N5Ai2aqQIHz2k5utK12QWYXixdbXIHULNTYnOxhBZeRwpw8Hbnv3PPIcMYz6GBnqMV+ABYURN/CyarwBFnhdioMgES+AknwAt3fSxYDdUGRY6ZfjdKygMp61nQ395EfSYejV4DDYUShRn7ypxWUiBmkGXHGf16HjqbBIPsk+pCMi+PIbvIYnWfd2kzTrAdI2e1SH4YZV0jf0d1ooSw6ps3wP0eDCxXID/3E2BMNFOg9Gt768LsuYaLW6RRIh2mxQLGlJ/I68EEiEEaz4qu3Gr7C/cCycVb4AeHJunbiGX2ALPSsNLze5PTCsEOa0Kh29bXSFAbFrZkLKFsOy0ZteXrmQ6uOy0vFqKluvJor3mCKrc81HOeqaJnFJ9++GHG5W1GDq42DHIsGnLmRE9pAjY3p2PfhTFg0F7+yjl+Cg3/dX0eAAUFJmf3tHJtmbGc71WBnjpZreWPUExvYIObRIWJwHLJ4R7vfMHORzr1cbkImNnx4bcFd3w15PvNtD4qLLGLUEOb1982T94C/B2+fxFeVgvpPdw9zIbJGXE9BWU0ehgOTQOL33tTWkZxUfifZaxVPy2AIdaPvCnQntQgqLrpAtlho7qIBC2Ew/Z9ZvMzwPTpwOX2DH4O9RR+ySL0JRMbgvySka1CoBJNh/A9Uu9IQNyBVkTqa2nqS4pvXnwETD95XZpjDm5IYP1vrM/BBlwnhlW6h248zazg9lCbk3ppR/4rCXbuqGsAY2bW0UeXDaoizIUcfId7BDTS6jEEaS5+YOt/GiCxPccEoXzB+6lxLUAUsTL1uUnUtJUZF0GfRTlKP1G4MqYMTz/FTF/6yjibI8yCFKId6ScJ0xZL/diQWOQXkyUCNgxjRfloEAZZkwcq77h6ctQV+7P4UsN2xWCkmwNxSdlD6JrA69i76ShmvdSxVgwhocNKbTw2GEq06J3kG6QNCmMwww6Vuh45KwtWxHdrqbyozWPMS64/DMyODniLAV5xC/bbmeNfSNns0K1/4UADhZhuZfiddbo9T8CPRNU84OwUmDmwrku9LRPEkjUkN2gQjW3xW7i4LMKhWXj6wo1K+wiGRcQ710RWhvLCC+s0WR6cibt+/fAUeS/2TFFvykaFnbPFtmt6ZSFGEEeg1sz974uYaltxmFoZ0CHcBlLlbw87hHNn4m5OczPGj4+zclLX8F8LTMvvIZcIS9+m5bnBFKuTdc4EGrxxT0apAAnk1QARt28azN6fY/Zo83P32Rd1+y5pD6Th+223mcIZIlbA/s3OVJYyLS2+iV+LE23pFIzzOCnOrpUNGv+52H0aTjyuAhOhcq2FhSAOszwFzwc9aZ5ieVXJav3FvxtiTznBThkduhnp081dl5foz87Zl/tqn5ixdptycj+Rl3kXKOPYlF5/QSGYp87YaEKtinuVW+s7fLbi+RWgkpNSZAJXEon9V88WBIUunE5v6WgqIYLhVLR20t/JOjg/8tayjX2bealblUY6cFwBwvRFiog98T0yXljGapv6Sd6/pThy/LnJTWjUunIYWGNzDfqCr1GF/ar5NHb6q+9ymyOJF5ZMHhEqsmgq5OTdrZgjAXxAgWIW0dK12E4Lp0qgJMoVnIpjbdCQJQk08qXhGtMXaYpJgJdSdK+XB6ogezQxNzW4Ja7c9IGCevHYdclqyJMm1ZlBLM0rmDwWmg8rCEfn4yO8YvPgyOLUdXNplpm9Pa2zB9FeRPRaYEvN5xE9VGPaJel8HNOGFFA0eu+Oht+UV8kFKdnj3GY87q5Ooi0Oxrow9SludbMPM0ALvjDDOEn7YYpq9qv87VBUlvQ1zSQdONaDbcIQVPGfdXN34qaAiqMUmnB6BYNLmXNrI+mfjDcSus2GfbBaBzgzYKGUvQEIkj1n8eIsZ7N+PgFW9njYTcE0aw+t8uJfqpD3o7IQMjDJSBaka3X16ni9szQi1RbdyGrRYwe55VJMXH592xIguWQQ0BdJMbIKEaam/aQ8cIh9h91Ju5KhPHjW3UWN3KuNmKRhKByp5n5aswpraDfXMxb3UO0/ZpmO2n5LaJpJmr+m56nB+cnmkv7Jojv/hX3Lur5VxpDyHJi8R2sJvPZdg6YvaytxhaIYVWCzeo+WmxzCE9YpcfUg6Y7itlsh8an8syrSLuvQHOdHrmh+TycLxOQ5kn6EJsGxZm/rFw/ngN1r/mNjxLqbJwu42Wop0OJ+/iE9qxLlveqnhhbsu2MbbtM+fFLTnJu5fBxxnSe8fm3QWQ0zLjdDgalizgB86IBbd8xZoYMS9vWavFOXhJncRBQzRWvevuzfehUtItXr61iresBx/UwGFQUM/S9mybruSBZVDhInDGLcrhV5mYTASmou3Unr6SyB+JvHr153wiU6S9YeC1OlNk7R/Op7HGk40emtvbEj5".toString()
        ));
    }

    @RequestMapping(value = "/do-qc", method = RequestMethod.GET)
    @ResponseBody
    public void doQC(@RequestParam(value = "case_id", required = true) int case_id)
            throws IOException, TemplateException, MessagingException, Exception {
        InspectionCase c = (InspectionCase) caseDAO.getCaseById(case_id);

        if (c == null) {
            return;
        }

        if (c.getCurrentStage() > 4) {
            return;
        }

        if (s3Service.exists("cases/" + c.getId() + "/" + c.getId() + ".zip")) {
            caseService.uploadZipToS3(case_id, "cases/" + c.getId() + "/" + c.getId() + ".zip", false);

        } else {
            throw new Exception("Inspection file not uploaded to server!");
        }
    }

    @RequestMapping(value = "/create-report", method = RequestMethod.GET)
    @ResponseBody
    public void createReport(@RequestParam(value = "case_id", required = true) int case_id)
            throws IOException, TemplateException, MessagingException, Exception {
        InspectionCase ic = (InspectionCase) caseDAO.getCaseById(case_id);

        String outputFile1 = "report.pdf";
        generateReportService.createPdf((InspectionCase) caseDAO.getCaseById(ic.getId()), ic.getId() + ".pdf",
                outputFile1, true);
    }

    @RequestMapping(value = "/call-gautam", method = RequestMethod.GET)
    @ResponseBody
    public void callGautam(@RequestParam(value = "phoneNumber", required = true) String phoneNumber)
            throws IOException, TemplateException, MessagingException, Exception {
        connectCustomerToFlow.callCustomerAfterCaseCreate(phoneNumber);
        connectCustomerToFlow.callCustomerAfterSignup(phoneNumber);
    }

    @RequestMapping(value = "/check-for-zip", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public String checkForzip() throws IOException, TemplateException, MessagingException, Exception {

        Set<CompanyBranchDivisionUser> companyBranchDivisionUsers = null;

        List<ScheduledCasesDTO> items = caseDAO.getScheduledCasesDTO(companyBranchDivisionUsers);

        JSONArray result = new JSONArray();

        for (ScheduledCasesDTO item : items) {
            if (s3Service.exists("cases/" + item.getId() + "/" + item.getId() + ".zip")) {
                result.put(item.getId());
            }
        }

        return result.toString();
    }

    @RequestMapping(value = "/get-random", method = RequestMethod.GET)
    @ResponseBody
    public String getRandom() throws IOException, TemplateException, MessagingException {
        return UtilHelper.getRandomStringLowerCase(16);
    }

    @RequestMapping(value = "/get-time-string", method = RequestMethod.GET)
    @ResponseBody
    public Object getTime(
            @RequestParam(value = "user_specified_time", required = false) @DateTimeFormat(pattern = "yyyy/MM/dd HH:mm:ss") Date date) {
        return date.toString();
    }

    @RequestMapping(value = "/get-buckets", method = RequestMethod.GET)
    @ResponseBody
    public Object getBuckets() {
        return s3Service.getObjectslistFromFolder();
    }

    @RequestMapping(value = "/file-exists", method = RequestMethod.GET)
    @ResponseBody
    public Object fileExists(@RequestParam(value = "file", required = true) String file) {
        return s3DirectUploadService.exists(file);
    }

    @RequestMapping(value = "/generate-test-report", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public void generateTestReport() throws Exception {
        InspectionCase ic = (InspectionCase) caseDAO.getCaseById(1);
        generateReportService.createPdf((InspectionCase) caseDAO.getCaseById(ic.getId()), ic.getId() + ".pdf",
                "report.pdf", true);
    }

    @RequestMapping(value = "/send-push-notification-to-agents", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public void sendPushNotification() throws IOException, TemplateException, MessagingException, JSONException {
        JSONObject data = new JSONObject();
        JSONArray l = new JSONArray();
        l.put("Value1");
        l.put("Value2");
        data.put("ACTION", "SCHEDULED_CASES");
        data.put("VALUES", l);

        androidPushNotificationsService.sendNotificationToUserAsync("USER_515761558339245390", "Testing",
                "This has data payload!", data.toString());
    }

    @RequestMapping(value = "/export-images", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public void exportImages() throws IOException, TemplateException, MessagingException, JSONException {
        String rootFolder = env.getProperty("jadu.upload.root.url");
        List<CaseMakeDTO> result = caseDAO.getCarByMake("Maruti");

        for (CaseMakeDTO temp : result) {
            long item = temp.getId();
            List<CasePhotoDTO2> photos = casePhotoDAO.getCasePhotosByCaseIdLimit(item);

            for (CasePhotoDTO2 photo : photos) {
                InputStream in = s3Service.readFileStream("cases/" + item + "/" + photo.getFilename());

                File dir = new File(rootFolder + "/" + temp.getModel() + "/" + photo.getType());
                if (!dir.exists()) {
                    dir.mkdirs();
                }

                File file = new File(
                        rootFolder + "/" + temp.getModel() + "/" + photo.getType() + "\\" + photo.getFilename());

                Files.copy(in, file.toPath());
            }
        }

    }

    @RequestMapping(value = "/export-odometer", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public void exportOdometer() throws IOException, TemplateException, MessagingException, JSONException {
        String rootFolder = "D:\\odometer";
        String csvFile = "C:\\Users\\asad.ali\\Documents\\casesFiles.csv";
        BufferedReader br = null;
        String line = "";
        String cvsSplitBy = ",";
        try {
            br = new BufferedReader(new FileReader(csvFile));
            while ((line = br.readLine()) != null) {
				// line = line.replaceAll(",", "").replaceAll("\\t", "").trim();
                // use comma as separator
                String[] csvValues = line.split(cvsSplitBy);
				// if (!(csvValues[1].isEmpty() &&
                // csvValues[0].isEmpty())&&csvValues[1]!=null)
                if (csvValues.length == 2) {
                    InputStream in = s3Service.readFileStream("cases/" + csvValues[0] + "/" + csvValues[1]);

                    File dir = new File(rootFolder);
                    if (!dir.exists()) {
                        dir.mkdirs();
                    }

                    File file = new File(rootFolder + "/" + csvValues[0] + "_" + csvValues[1]);

                    Files.copy(in, file.toPath());
                }
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    @RequestMapping(value = "/validate-odometer-reading", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public int validate() throws IOException, TemplateException, MessagingException, JSONException {
        int count = 0;
        FileWriter writer = new FileWriter("C:\\Users\\asad.ali\\Downloads\\odometerCompareResult.csv");
        writer.append("caseID");
        writer.append(',');
        writer.append("Machine Reading");
        writer.append(',');
        writer.append("Actual Reading");
        writer.append('\n');
        String csvFile = "C:\\Users\\asad.ali\\Downloads\\nohup.out";
        BufferedReader br = null;
        String line = "";
        String cvsSplitBy = ",";
        try {
            br = new BufferedReader(new FileReader(csvFile));
            while ((line = br.readLine()) != null) {
                count++;
                String[] csvValues = line.split(cvsSplitBy);
                if (csvValues.length == 2) {
                    if (csvValues[1].equalsIgnoreCase("None")) {
                        continue;
                    }
                    String[] cases = csvValues[0].split("_");
                    InspectionCase inspectionCase;
                    try {
                        inspectionCase = (InspectionCase) caseDAO.getCaseById(Long.valueOf(cases[0]));
                        if (inspectionCase != null) {
                            boolean isDemocase = inspectionCase.getInsuranceCompany() != null
                                    && inspectionCase.getInsuranceCompany().getName().equalsIgnoreCase("Demo Insurer");

                            if (isDemocase) {
                                continue;
                            }
                            if (inspectionCase.getOdometerReading().equals(csvValues[1])) {
                                continue;
                            }
                            if (!inspectionCase.getRemark().equalsIgnoreCase("recommended")) {
                                continue;
                            }
                            writer.append(cases[0]);
                            writer.append(',');
                            writer.append(csvValues[1]);
                            writer.append(',');
                            writer.append(inspectionCase.getOdometerReading());
                            writer.append('\n');
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        LOGGER.error("Exception raised while validating recor{} error={}", line, e.getMessage());
                        writer.append(line);
                        writer.append('\n');
                    }

                }
            }
            System.out.println("Total row processed =" + count);
            writer.flush();
            writer.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return count;

    }

    @RequestMapping(value = "/download-Folder", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public ResponseEntity<MultipleFileDownload> downloadFolder() throws IOException, TemplateException,
            MessagingException, JSONException, AmazonServiceException, AmazonClientException, InterruptedException {

        MultipleFileDownload in = s3Service.downloadFolder("101");

        return new ResponseEntity<>(in, HttpStatus.OK);
    }

    @RequestMapping(value = "/download-Folders", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public void downloadFileWithPost(@RequestParam("caseId") String caseId, HttpServletResponse response)
            throws IOException {
        String path = "D:/cases/";
        File folder = new File(path + File.separator + caseId);
        File[] listOfFiles = folder.listFiles();
        ZipUtility.zip(Arrays.asList(listOfFiles), path + caseId + ".zip");
        File file = new File(path + File.separator + caseId + ".zip");
        InputStream inputStream = new FileInputStream(file);
        try {
            response.setHeader("Content-Disposition", "attachment; filename=\"" + caseId + ".zip" + "\"");
            IOUtils.copy(inputStream, response.getOutputStream());
            response.flushBuffer();
        } catch (IOException ex) {
            throw new RuntimeException("IOError writing file to output stream");
        }
    }

    @RequestMapping(value = "/send-push-notification-web", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public void sendPushNotificationWeb(@RequestParam(value = "token", required = true) String token)
            throws IOException, TemplateException, MessagingException, JSONException {
        JSONObject data = new JSONObject();
        JSONArray l = new JSONArray();
        l.put("Value1");
        l.put("Value2");
        data.put("ACTION", "SCHEDULED_CASES");
        data.put("VALUES", l);

        androidPushNotificationsService.sendNotificationToDevice(token, "Testing", "This has data payload!",
                data.toString());
    }

    @RequestMapping(value = "/agents/comments", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public List<CaseCommentDTO> getAgentComments(
            @RequestParam(value = "phonenumber", required = true) String phonenumber,
            @RequestParam(value = "from", required = true) Long from,
            @RequestParam(value = "to", required = true) Long to) {
        List<InspectionCase> cases = caseDAO.getCaseByAgentPhoneNumberAndCreatedTime(phonenumber, new Date(from),
                new Date(to));
        List<CaseCommentDTO> caseCommentDTOs = new ArrayList<CaseCommentDTO>();
        for (InspectionCase inspectionCase : cases) {
            caseCommentDTOs.addAll(caseCommentDAO.getCommentsByCommentTimeAndCaseId(inspectionCase.getId(),
                    new Date(from), new Date(to)));
        }
        return caseCommentDTOs;
    }

    @RequestMapping(value = "/comments", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public AgentCommentDTO getAgentComments(@RequestParam(value = "phonenumber", required = true) String phonenumber) {
        AgentCommentDTO agentCommentDTO = new AgentCommentDTO();
        agentCommentDTO.setCasesCount(caseDAO.getCaseCount(phonenumber));

        agentCommentDTO.setCaseCommentDTO(agentCommentDAO.get(phonenumber));

        return agentCommentDTO;
    }

    @RequestMapping(value = "/test-third-party-api", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public void testThirdPartyApi(@RequestParam(value = "case_id", required = true) Long caseId) throws Exception {
        InspectionCase ic = (InspectionCase) caseDAO.getCaseById(caseId);
        OnlineCases oc = onlineCasesDAO.findByCaseId(ic.getId());
        List<CasePhoto> casePhotos = casePhotoDAO.getCasePhotos(ic.getId());
        for (ApiSwitch apiSwitch : ApiSwitch.values()) {
            if (apiSwitch.qualify(ic.getCompanyBranchDivision(), ic, oc)) {
                apiSwitch.getApi().call(ic, oc, onlineCasesDAO, appConfigService, googleLocationService, casePhotos, s3Service);
            }
        }
    }

    @RequestMapping(value = "/callback", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public String getAgentComments(@RequestParam(value = "caseId", required = true) long caseId) throws Exception {
        InspectionCase inspectionCase = (InspectionCase) caseDAO.getCaseById(caseId);
        return callbackService.pushOnlineCaseStatus(inspectionCase, null);
    }

    @RequestMapping(value = "/soap-test-22", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public DCTMWebServiceResult soapTest2() throws Exception {
        DCTMWebServiceImplServiceLocator l = new DCTMWebServiceImplServiceLocator();
        try {
            DCTMWebServiceImpl ser = l.getDCTMWebServiceImpl();

            KeyValueMap[] metadata = {
                new KeyValueMap("source", "upreinsp"),
                new KeyValueMap("product", "FWP"),
                new KeyValueMap("file_name", "SuppEstimate2.pdf"),
                new KeyValueMap("doc_code", "RC"),
                new KeyValueMap("preinsp_id", "112")
            };

            WebContent webContent = new WebContent(
                    "tesst".getBytes(),
                    metadata
            );

            WebContent[] webContents = {webContent};
            DCTMWebServiceResult result = ser.upload(webContents);
            return result;

        } catch (ServiceException | RemoteException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return null;
    }

    @RequestMapping(value = "/test-enum", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public PolicyBachatBean testEnum() throws Exception {
        return new PolicyBachatBean(
                "AP09AB1234",
                "12345",
                "2020-03-04 15:22:03",
                PolicyBachatBean.Status.APPROVED,
                "The New India Assurance Co. Ltd",
                ""
        );
    }

    @RequestMapping(value = "/get-objects", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public Object getObjectsInBucket(
            @RequestParam(value = "case", required = true) String inspectionCase,
            @RequestParam(value = "regex", required = false) String regex    
    ) throws Exception {
        return s3Service.getObjectsInBucket("cases/" + inspectionCase, regex);
    }
    
    @RequestMapping(value = "/get-signed-video", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public Object getsignedVideo() throws Exception {
        return s3Service.getReadSignedUrl("cases/1000002/video.mp4", 240);
    }
    
    @RequestMapping(value = "/update-status-soap-test", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public ITGIMotorPreInspectionAgencyRecommendationResponse_Output updateStatusSoapTest(
            @RequestParam(value = "preInspectionNumber", required = true) java.lang.String preInspectionNumber,
            @RequestParam(value = "recommendationResponse", required = true) String recommendationResponse,
            @RequestParam(value = "latitudeLongitude", required = true) String latitudeLongitude,
            @RequestParam(value = "inspectionLocation", required = true) String inspectionLocation,
            @RequestParam(value = "preInspectionIntegrationId", required = true) String preInspectionIntegrationId,
            @RequestParam(value = "remarks", required = true) String remarks,
            @RequestParam(value = "chassisNumber", required = true) String chassisNumber,
            @RequestParam(value = "inspectionDateTime", required = true) String inspectionDateTime,
            @RequestParam(value = "engineNumber", required = true) String engineNumber
    ) throws Exception {

        ITGI_spcMotor_spcPreInspection_spcAgency_spcResponseLocator l = new ITGI_spcMotor_spcPreInspection_spcAgency_spcResponseLocator();
        ITGIMotorPreInspectionAgencyResponse s = l.getITGIMotorPreInspectionAgencyResponse();

        ITGIMotorPreInspectionAgencyRecommendationResponse_Input input = new ITGIMotorPreInspectionAgencyRecommendationResponse_Input(
                preInspectionNumber,
                recommendationResponse,
                latitudeLongitude,
                inspectionLocation,
                preInspectionIntegrationId,
                remarks,
                chassisNumber,
                inspectionDateTime,
                engineNumber
        );

        return s.ITGIMotorPreInspectionAgencyRecommendationResponse(input);
    }

    @RequestMapping(value = "/check-json", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public InspectionCase jsonTest() throws IOException, TemplateException, MessagingException, Exception {
        InspectionCase ic = (InspectionCase) caseDAO
                .getCaseById(appConfigService.getLongProperty("IC_TEST_CASE_ID", 79728l));

        String data = "{\"details\":\"[{\\\"id\\\":\\\"others\\\",\\\"photos\\\":[{\\\"answer\\\":\\\"\\\",\\\"fileName\\\":\\\"1564053577106.png\\\",\\\"hash\\\":\\\"9a7d4cd8b6fab72684899caa9e097bb8\\\",\\\"latitude\\\":17.4285562,\\\"longitude\\\":78.4138683},{\\\"answer\\\":\\\"\\\",\\\"fileName\\\":\\\"1564053571207.png\\\",\\\"hash\\\":\\\"b8bbcfdd72a94703bb917178eeb14530\\\",\\\"latitude\\\":17.4285562,\\\"longitude\\\":78.4138683},{\\\"answer\\\":\\\"\\\",\\\"fileName\\\":\\\"1564053565391.png\\\",\\\"hash\\\":\\\"f3b6e534562e3a52d355c53fa96bb736\\\",\\\"latitude\\\":17.4285562,\\\"longitude\\\":78.4138683},{\\\"answer\\\":\\\"\\\",\\\"fileName\\\":\\\"1564053559350.png\\\",\\\"hash\\\":\\\"c6079268fbbc5c142cca19d482225f31\\\",\\\"latitude\\\":17.4285562,\\\"longitude\\\":78.4138683}]}]\",\"encryptionKey\":\"U1QB9EIJ6I003BYL\",\"filesList\":[],\"fuelType\":\"\",\"makeModel\":\"\",\"rcPhotoPath\":\"\",\"refNo\":\"79730\",\"uploadId\":\"\",\"vehicleColor\":\"\",\"vehicleNumber\":\"TDGG666666\",\"vehicleType\":\"\",\"videoName\":\"\",\"yom\":\"\"}";
        JSONObject json = new JSONObject(data);
        PhotoType photoType = photoTypeDAO.get("others");
        JSONArray details = new JSONArray(json.getString("details"));
        List<CasePhoto> casePhotos = new ArrayList<>();
        for (int i = 0; i < details.length(); i++) {
            JSONObject jsonObject = details.getJSONObject(i);
            JSONArray photos = jsonObject.getJSONArray("photos");
            for (int j = 0; j < photos.length(); j++) {
                JSONObject photo = photos.getJSONObject(j);
                CasePhoto cp = new CasePhoto();
                cp.setInspectionCase(ic);
                cp.setAnswer(photo.has("answer") ? photo.getString("answer") : "");
                cp.setQcAnswer(photo.has("qcanswer") ? photo.getString("qcanswer") : "");
                cp.setLatitude(photo.has("latitude") ? photo.getDouble("latitude") : 0.0);
                cp.setLongitude(photo.has("longitude") ? photo.getDouble("longitude") : 0.0);
                cp.setSnapTime(new Date(photo.has("snapTime") ? photo.getLong("snapTime") : new Date().getTime()));
                cp.setFileName(photo.has("fileName") ? photo.getString("fileName") : i + j + "");
                cp.setPhotoType(photoType);
                casePhotos.add(cp);
            }
        }
        caseDAO.saveCasePhotos(ic, casePhotos, "");
        return ic;

    }

    @ExceptionHandler(Exception.class)
    public void handleAllException(Exception ex, HttpServletResponse response) throws IOException {
        ErrorHandler.handleError(ex, response);
        ex.printStackTrace();
    }
}
