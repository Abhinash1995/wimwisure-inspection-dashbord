/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jadu.tasks;

import com.jadu.dao.CaseAttemptAllocationDAO;
import com.jadu.dao.CaseDAO;
import com.jadu.dao.InspectorDAO;
import com.jadu.dto.CaseAttemptAllocationDTO;
import com.jadu.helpers.BoundingBox;
import com.jadu.helpers.UtilHelper;
import com.jadu.model.CaseAttemptAllocation;
import com.jadu.model.GeoLocation;
import com.jadu.model.InspectionCase;
import com.jadu.model.Inspector;
import com.jadu.push.notification.AndroidPushNotificationsService;
import com.jadu.service.InspectorService;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author gauta
 */
public class RunMeTask {
    
    @Autowired
    CaseDAO caseDAO;
    
    @Autowired
    InspectorDAO inspectorDAO;
    
    @Autowired
    AndroidPushNotificationsService androidPushNotificationsService;
    
    @Autowired
    CaseAttemptAllocationDAO caseAttemptAllocationDAO;
    
    public void printMe() {
        
        try {
            System.out.println("Spring 3 + Quartz 1.8.6 ~");
            
            List<InspectionCase> cases = caseDAO.getCasesToBeAllocated();
            
            for(InspectionCase cs : cases){
                
                List<CaseAttemptAllocationDTO> attempts = (List<CaseAttemptAllocationDTO>)caseAttemptAllocationDAO.get(cs.getId());
                List<GeoLocation> result = 
                        BoundingBox.getBoundingBox(cs.getInspectionLatitude(), cs.getInspectionLongitude(), 30.0);
                List<Inspector> inspectors = inspectorDAO.getInspectorBoundingBox(result.get(0), result.get(1));
                for(Inspector inspector : inspectors){
                    if(!InspectorService.isInspectorAttempted(attempts, inspector)){
                        CaseAttemptAllocation c =  new CaseAttemptAllocation();
                        c.setAttemptTime(UtilHelper.getDate());
                        c.setInspectionCase(cs);
                        c.setInspector(inspector);
                        Date expiryTime = UtilHelper.getDateAddMinutes(10);
                        c.setAttemptExpiryTime(expiryTime);
                        caseAttemptAllocationDAO.save(c);
                        
                        cs.setAttemptExpiryTime(expiryTime);
                        caseDAO.save(cs);
                        
                        androidPushNotificationsService.sendNotificationToUserAsync(
                            inspector.getUsername(), 
                            "Received inspection request", 
                            "Case No: " + cs.getId() + " Received Inspection request. Please check case request tab for accepting the case. Decline the case if not available",
                            "");
                        break;
                    }
                }
                
                cs.setAllocationAttempts(cs.getAllocationAttempts() + 1);
                caseDAO.save(cs);
            }
            
        } catch (Exception ex) {
            Logger.getLogger(RunMeTask.class.getName()).log(Level.SEVERE, null, ex);
        }
	}
}
