package com.jadu.prune;

import java.io.IOException;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.jadu.error.handler.ErrorHandler;
import com.jadu.service.AppConfigService;
import com.jadu.service.PruneCaseService;

@RestController
@RequestMapping("/prune")
public class PruneController {

	private static final Logger LOGGER = LoggerFactory.getLogger(PruneController.class);

	@Autowired
	private AppConfigService appConfigService;

	@Autowired
	private PruneCaseService pruneCaseService;

	@RequestMapping(value = "/offline/cases", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public ResponseEntity<?> pruneOfflineCases(@RequestParam(value = "secret_key", required = true) String secret_key,
			@RequestParam(value = "from", required = false) int from,
			@RequestParam(value = "to", required = false) int to,
			@RequestParam(value = "current_stage", required = false) Integer current_stage,
			@RequestParam(value = "case_ids", required = false) String case_ids) {
		if (secret_key.equals(appConfigService.getProperty("PRUNE_CASES_API_PASSWORD", "magic@W!mwisure"))) {
			LOGGER.info("/prune/old/cases request recieved with from ={},to={}, current_stage={},case_ids={}", from, to,
					current_stage, case_ids);
			if (from != 0 && current_stage != null && current_stage != 3 && current_stage != 4) {
				Date fromDate = new Date(getDateInLong(from));
				Date toDate = new Date(getDateInLong(to));
				String prunedCase = pruneCaseService.pruneAndPersistInInspectionCasesHistory(fromDate, toDate,
						current_stage, 0);
				return new ResponseEntity<>(prunedCase, HttpStatus.OK);
			} else if (current_stage != null && current_stage != 3 && current_stage != 4) {
				String prunedCase = pruneCaseService.pruneAndPersistInInspectionCasesHistory(current_stage, 0);
				return new ResponseEntity<>(prunedCase, HttpStatus.OK);
			} else if (case_ids != null) {
				List<String> caseIds = Arrays.asList(case_ids.split("\\,"));
				for (String caseId : caseIds) {
					pruneCaseService.pruneAndPersistInInspectionCasesHistory(Long.valueOf(caseId), 0);
				}
				return new ResponseEntity<>(HttpStatus.OK);
			}
			LOGGER.info("No valid prune criteria found for parameter from ={},to={}, current_stage={},case_ids={}",
					from, to, current_stage, case_ids);
		}
		return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);

	}

	@RequestMapping(value = "/online/cases", method = RequestMethod.POST, produces = "application/json")

	@ResponseBody
	public ResponseEntity<?> pruneOnlineCases(@RequestParam(value = "secret_key", required = true) String secret_key,

			@RequestParam(value = "from", required = false) int from,

			@RequestParam(value = "to", required = false) int to,

			@RequestParam(value = "current_stage", required = false) Integer current_stage,

			@RequestParam(value = "case_ids", required = false) String case_ids) {
		if (secret_key.equals(appConfigService.getProperty("PRUNE_CASES_API_PASSWORD", "magic@W!mwisure"))) {
			LOGGER.info("/prune/old/cases request recieved with from ={},to={}, current_stage={},case_ids={}", from, to,
					current_stage, case_ids);
			if (from != 0 && current_stage != null && current_stage != 3 && current_stage != 4) {
				Date fromDate = new Date(getDateInLong(from));
				Date toDate = new Date(getDateInLong(to));
				String prunedCase = pruneCaseService.pruneAndPersistInInspectionCasesHistory(fromDate, toDate,
						current_stage, 1);
				return new ResponseEntity<>(prunedCase, HttpStatus.OK);
			} else if (current_stage != null && current_stage != 3 && current_stage != 4) {
				String prunedCase = pruneCaseService.pruneAndPersistInInspectionCasesHistory(current_stage, 1);
				return new ResponseEntity<>(prunedCase, HttpStatus.OK);
			} else if (case_ids != null) {
				List<String> caseIds = Arrays.asList(case_ids.split("\\,"));
				for (String caseId : caseIds) {
					pruneCaseService.pruneAndPersistInInspectionCasesHistory(Long.valueOf(caseId), 1);
				}
				return new ResponseEntity<>(HttpStatus.OK);
			}
			LOGGER.info("No valid prune criteria found for parameter from ={},to={}, current_stage={},case_ids={}",
					from, to, current_stage, case_ids);

		}
		return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);

	}

	private Long getDateInLong(Integer fromOrTo) {
		if (fromOrTo < 3)
			fromOrTo = appConfigService.getIntProperty("DEFAULT_MONTH_CASES_NOT_TO_BE_PRUNED", 3);
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(new Date());
		calendar.add(Calendar.MONTH, -fromOrTo);
		return calendar.getTimeInMillis();
	}

	@ExceptionHandler(Exception.class)
	public void handleAllException(Exception ex, HttpServletResponse response) throws IOException {
		ErrorHandler.handleError(ex, response);
		ex.printStackTrace();
	}

}
