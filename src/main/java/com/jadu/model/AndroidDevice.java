package com.jadu.model;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("ANDROID")
public class AndroidDevice extends Device{
	private static final long serialVersionUID = 1L;
	@Column(name="device_instance_id")
	private String deviceInstanceId;
    @Column(name="app_version")
	private String appVersionNumber;
    
	public String getDeviceInstanceId() {
		return deviceInstanceId;
	}

	public void setDeviceInstanceId(String deviceInstanceId) {
		this.deviceInstanceId = deviceInstanceId;
	}

	public String getAppVersionNumber() {
		return appVersionNumber;
	}

	public void setAppVersionNumber(String appVersionNumber) {
		this.appVersionNumber = appVersionNumber;
	}
}
