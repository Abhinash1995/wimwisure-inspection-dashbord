package com.jadu.model;

import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
@Entity
@Table(name="vehicle_photo_req")
public class VehiclePhotoReq implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private Integer  id;
    @ManyToOne
    @JoinColumn(name="vehicle_type", nullable=false)
    private VehicleType vehicleType;
    
    @ManyToOne
    @JoinColumn(name="fuel_type", nullable=false)
    private VehicleFuelType vehicleFuelType;
    
    @ManyToOne
    @JoinColumn(name="purpose_of_inspection", nullable=false)
    private PurposeOfInspection purposeOfInspection;
    
    @ManyToOne
    @JoinColumn(name="vehicle_sub_type", nullable=false)
    private VehicleType vehicleSubType;
    
    @ManyToOne(cascade = { CascadeType.DETACH }, fetch = FetchType.EAGER)
    @JoinColumn(name="photo_type")
    private PhotoType photoType;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public VehicleType getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(VehicleType vehicleType) {
        this.vehicleType = vehicleType;
    }

    public VehicleFuelType getVehicleFuelType() {
        return vehicleFuelType;
    }

    public void setVehicleFuelType(VehicleFuelType vehicleFuelType) {
        this.vehicleFuelType = vehicleFuelType;
    }

    public VehicleType getVehicleSubType() {
        return vehicleSubType;
    }

    public void setVehicleSubType(VehicleType vehicleSubType) {
        this.vehicleSubType = vehicleSubType;
    }

    public PhotoType getPhotoType() {
        return photoType;
    }

    public void setPhotoType(PhotoType photoType) {
        this.photoType = photoType;
    }

    public PurposeOfInspection getPurposeOfInspection() {
        return purposeOfInspection;
    }

    public void setPurposeOfInspection(PurposeOfInspection purposeOfInspection) {
        this.purposeOfInspection = purposeOfInspection;
    }
    
    
    
}
