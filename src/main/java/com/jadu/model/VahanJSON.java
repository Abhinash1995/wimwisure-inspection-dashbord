/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jadu.model;

import com.sun.org.apache.xerces.internal.parsers.DOMParser;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 *
 * @author gauta
 */
public class VahanJSON {
    
    private boolean isValid;
    private JSONObject json;
    
    public VahanJSON(String xml){
        DOMParser parser = new DOMParser();
        try {
            parser.parse(new InputSource(new java.io.StringReader(xml)));
            Document doc = parser.getDocument();
            json = new JSONObject(doc.getElementsByTagName("vehicleJson").item(0).getTextContent());
            isValid = true;
        }  catch (IOException | SAXException | JSONException ex) {
            Logger.getLogger(VahanJSON.class.getName()).log(Level.SEVERE, null, ex);
            isValid = false;
        }
    }
    
    public Integer getYear(){
        if(isValid)
            try {
                return Integer.parseInt(json.getString("RegistrationYear"));
            } catch (JSONException ex) {
                Logger.getLogger(VahanJSON.class.getName()).log(Level.SEVERE, null, ex);
                return null;
            } catch(java.lang.NumberFormatException ex){
                Logger.getLogger(VahanJSON.class.getName()).log(Level.SEVERE, null, ex);
                return null;
            }catch(Exception ex){
                Logger.getLogger(VahanJSON.class.getName()).log(Level.SEVERE, null, ex);
                return null;
            }
        else
            return null;
    }
    
    public String getMake(){
        if(isValid)
            try {
                return json.getJSONObject("CarMake").getString("CurrentTextValue") == "null" ? null : json.getJSONObject("CarMake").getString("CurrentTextValue");
            } catch (JSONException ex) {
                Logger.getLogger(VahanJSON.class.getName()).log(Level.SEVERE, null, ex);
                return null;
            }
        else
            return null;
    }
    
    public String getModel(){
        if(isValid)
            try {
                return json.getJSONObject("CarModel").getString("CurrentTextValue") == "null" ? null : json.getJSONObject("CarModel").getString("CurrentTextValue");
            } catch (JSONException ex) {
                Logger.getLogger(VahanJSON.class.getName()).log(Level.SEVERE, null, ex);
                return null;
            }
        else
            return null;
    }
    
    public String getFuelType(){
        if(isValid)
            try {
                return json.getJSONObject("FuelType").getString("CurrentTextValue");
            } catch (JSONException ex) {
                Logger.getLogger(VahanJSON.class.getName()).log(Level.SEVERE, null, ex);
                return null;
            }
        else
            return null;
    }
    
    public String getChasisNumber(){
        if(isValid)
            try {
                return json.getString("VechileIdentificationNumber");
            } catch (JSONException ex) {
                Logger.getLogger(VahanJSON.class.getName()).log(Level.SEVERE, null, ex);
                return null;
            }
        else
            return null;
    }
    
    public String getEngineNumber(){
        if(isValid)
            try {
                return json.getString("EngineNumber");
            } catch (JSONException ex) {
                Logger.getLogger(VahanJSON.class.getName()).log(Level.SEVERE, null, ex);
                return null;
            }
        else
            return null;
    }
    
    public String getColor(){
        if(isValid)
            try {
                return json.getString("Colour");
            } catch (JSONException ex) {
                Logger.getLogger(VahanJSON.class.getName()).log(Level.SEVERE, null, ex);
                return null;
            }
        else
            return null;
    }
}
