package com.jadu.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PreUpdate;
import javax.persistence.Table;

import org.hibernate.annotations.Index;

@Entity
@Table(name = "online_cases")
public class OnlineCases implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "quote_number")
    private String quoteNumber;

    @Column(name = "case_id")
    private Long inspectionCaseId;

    @Column(name = "is_status_updated")
    private boolean isStatusUpdated;

    @Column(name = "status_message")
    private String statusMessage;

    @Column(name = "vehicle_number")
    private String vehicleNumber;

    @Column(name = "chassis_number")
    private String chassisNumber;

    @Column(name = "engine_number")
    private String engineNumber;
    
    @Column(name = "inspection_number")
    private String inspectionNumber;

    @Column(name = "inspection_address")
    private String inspectionAddress;

    @Column(name = "inspection_latitude")
    private Double inspectionLatitude;

    @Column(name = "inspection_longitude")
    private Double inspectionLongitude;

    @Index(name = "created_date")
    @Column(name = "created_date")
    private Date createdDate = new Date();

    @Index(name = "updated_date")
    @Column(name = "updated_date")
    private Date updatedDate = new Date();

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getQuoteNumber() {
        return quoteNumber;
    }

    public void setQuoteNumber(String quoteNumber) {
        this.quoteNumber = quoteNumber;
    }

    public Long getInspectionCaseId() {
        return inspectionCaseId;
    }

    public void setInspectionCaseId(Long inspectionCaseId) {
        this.inspectionCaseId = inspectionCaseId;
    }

    public boolean isStatusUpdated() {
        return isStatusUpdated;
    }

    public void setStatusUpdated(boolean isStatusUpdated) {
        this.isStatusUpdated = isStatusUpdated;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public boolean isIsStatusUpdated() {
        return isStatusUpdated;
    }

    public void setIsStatusUpdated(boolean isStatusUpdated) {
        this.isStatusUpdated = isStatusUpdated;
    }

    public String getVehicleNumber() {
        return vehicleNumber;
    }

    public void setVehicleNumber(String vehicleNumber) {
        this.vehicleNumber = vehicleNumber;
    }

    public String getChassisNumber() {
        return chassisNumber;
    }

    public void setChassisNumber(String chassisNumber) {
        this.chassisNumber = chassisNumber;
    }

    public String getEngineNumber() {
        return engineNumber;
    }

    public void setEngineNumber(String engineNumber) {
        this.engineNumber = engineNumber;
    }

    public String getInspectionAddress() {
        return inspectionAddress;
    }

    public void setInspectionAddress(String inspectionAddress) {
        this.inspectionAddress = inspectionAddress;
    }

    public Double getInspectionLatitude() {
        return inspectionLatitude;
    }

    public void setInspectionLatitude(Double inspectionLatitude) {
        this.inspectionLatitude = inspectionLatitude;
    }

    public Double getInspectionLongitude() {
        return inspectionLongitude;
    }

    public void setInspectionLongitude(Double inspectionLongitude) {
        this.inspectionLongitude = inspectionLongitude;
    }

    public String getInspectionNumber() {
        return inspectionNumber;
    }

    public void setInspectionNumber(String inspectionNumber) {
        this.inspectionNumber = inspectionNumber;
    }
    
    @PreUpdate
    public void setLastUpdate() {  
        this.updatedDate = new Date(); 
    }
}
