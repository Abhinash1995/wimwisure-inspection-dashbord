package com.jadu.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="irdai_agent_data")
public class IRDAIAgentData implements Serializable {
    
    @Id
    @Column(name="irda_urn")
    private String irdaiUrn;
    @Column(name="agent_name")
    private String agentName;
    @Column(name="license_number")
    private int licenseNumber;
    @Column(name="agent_id")
    private String agentId;
    @Column(name="insurance_type")
    private String insuranceType;
    @Column(name="insurer")
    private String insurer;
    @Column(name="dp_id")
    private int dpId;
    @Column(name="state")
    private String state;
    @Column(name="district")
    private String district;
    @Column(name="pin_code")
    private String pinCode;
    @Column(name="valid_from")
    private Date validFrom;
    @Column(name="valid_to")
    private Date validTo;
    @Column(name="absorbed_agent")
    private String absorbedAgent;
    @Column(name="phone_number")
    private String phoneNumber;
    @Column(name="mobile_number")
    private String mobileNumber;

    public String getIrdaiUrn() {
        return irdaiUrn;
    }

    public void setIrdaiUrn(String irdaiUrn) {
        this.irdaiUrn = irdaiUrn;
    }

    public String getAgentName() {
        return agentName;
    }

    public void setAgentName(String agentName) {
        this.agentName = agentName;
    }

    public int getLicenseNumber() {
        return licenseNumber;
    }

    public void setLicenseNumber(int licenseNumber) {
        this.licenseNumber = licenseNumber;
    }

    public String getAgentId() {
        return agentId;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    public String getInsuranceType() {
        return insuranceType;
    }

    public void setInsuranceType(String insuranceType) {
        this.insuranceType = insuranceType;
    }

    public String getInsurer() {
        return insurer;
    }

    public void setInsurer(String insurer) {
        this.insurer = insurer;
    }

    public int getDpId() {
        return dpId;
    }

    public void setDpId(int dpId) {
        this.dpId = dpId;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getPinCode() {
        return pinCode;
    }

    public void setPinCode(String pinCode) {
        this.pinCode = pinCode;
    }

    public Date getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(Date validFrom) {
        this.validFrom = validFrom;
    }

    public Date getValidTo() {
        return validTo;
    }

    public void setValidTo(Date validTo) {
        this.validTo = validTo;
    }

    public String getAbsorbedAgent() {
        return absorbedAgent;
    }

    public void setAbsorbedAgent(String absorbedAgent) {
        this.absorbedAgent = absorbedAgent;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }
    
    
}
