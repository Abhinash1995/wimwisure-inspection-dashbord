package com.jadu.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

@Entity
@Table(name = "agent_details")
public class AgentDetail implements Serializable {
	@Id
	@Column(name = "username", unique = true, nullable = false)
	@GeneratedValue(generator = "gen")
	@GenericGenerator(name = "gen", strategy = "foreign", parameters = @Parameter(name = "property", value = "agent"))
	private String username;

	@Column(name = "agent_id")
	private String agentId;

	@ManyToOne(cascade = { CascadeType.DETACH }, fetch = FetchType.EAGER)
	@JoinColumn(name = "company_branch_division_id")
	private CompanyBranchDivision companyBranchDivision;

	@JsonIgnore
	@OneToOne
	@PrimaryKeyJoinColumn
	private Agent agent;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getAgentId() {
		return agentId;
	}

	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}

	public Agent getAgent() {
		return agent;
	}

	public void setAgent(Agent agent) {
		this.agent = agent;
	}

	public CompanyBranchDivision getCompanyBranchDivision() {
		return companyBranchDivision;
	}

	public void setCompanyBranchDivision(CompanyBranchDivision companyBranchDivision) {
		this.companyBranchDivision = companyBranchDivision;
	}

	@Override
	public String toString() {
		return "AgentDetail [username=" + username + ", agentId=" + agentId + ", companyBranchDivision="
				+ companyBranchDivision + ", agent=" + agent + "]";
	}

}
