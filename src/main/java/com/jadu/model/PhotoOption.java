package com.jadu.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;

@Entity
@Table(name="photo_options")
public class PhotoOption implements Serializable {
    @Id
    @Column(name="id")
    private int id;
    
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name="photo_type", nullable=false)
    private PhotoType photoType ;
    
    @Column(name="opt")
    private String option;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;  
    }

    public PhotoType getPhotoType() {
        return photoType;
    }

    public void setPhotoType(PhotoType photoType) {
        this.photoType = photoType;
    }

    public String getOption() {
        return option;
    }

    public void setOption(String option) {
        this.option = option;
    }
    
    
}
