package com.jadu.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;

@Entity
@DiscriminatorValue("QC")
public class QC extends User implements Serializable{
    
    @JsonIgnore
    @OneToMany(mappedBy="username", cascade = { CascadeType.ALL }, fetch = FetchType.EAGER)
    private Set<CompanyBranchDivisionUser> companyBranchDivisionUser;

    public Set<CompanyBranchDivisionUser> getCompanyBranchDivisionUser() {
        return companyBranchDivisionUser;
    }

    public void setCompanyBranchDivisionUser(Set<CompanyBranchDivisionUser> companyBranchDivisionUser) {
        this.companyBranchDivisionUser = companyBranchDivisionUser;
    }
    
    
}
