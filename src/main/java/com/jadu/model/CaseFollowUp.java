package com.jadu.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;

import org.hibernate.annotations.Index;

@Entity
@Table(name = "cases_follow-up")
public class CaseFollowUp {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@Index(name = "case_id")
	@Column(name = "case_id", nullable = false)
	private long caseId;

	@Index(name = "callSid")
	@Column(name = "callSid")
	private String callSid;

	@Column(name = "status")
	private String status;

	@Column(name = "recordingUrl")
	private String recordingUrl;

	@Column(name = "dateUpdated")
	@Temporal(javax.persistence.TemporalType.TIMESTAMP)
	private Date dateUpdated;

	@Column(name = "follow_up_time")
	@Temporal(javax.persistence.TemporalType.TIMESTAMP)
	private Date followUpTime;

	@Column(name = "call_type")
	private CallType callType;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getCaseId() {
		return caseId;
	}

	public void setCaseId(long caseId) {
		this.caseId = caseId;
	}

	public String getCallSid() {
		return callSid;
	}

	public void setCallSid(String callSid) {
		this.callSid = callSid;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getRecordingUrl() {
		return recordingUrl;
	}

	public void setRecordingUrl(String recordingUrl) {
		this.recordingUrl = recordingUrl;
	}

	public Date getDateUpdated() {
		return dateUpdated;
	}

	public void setDateUpdated(Date dateUpdated) {
		this.dateUpdated = dateUpdated;
	}

	public Date getFollowUpTime() {
		return followUpTime;
	}

	public void setFollowUpTime(Date followUpTime) {
		this.followUpTime = followUpTime;
	}

	public CallType getCallType() {
		return callType;
	}

	public void setCallType(CallType callType) {
		this.callType = callType;
	}

	@Override
	public String toString() {
		return "CaseFollowUp [id=" + id + ", caseId=" + caseId + ", callSid=" + callSid + ", status=" + status
				+ ", recordingUrl=" + recordingUrl + ", dateUpdated=" + dateUpdated + ", followUpTime=" + followUpTime
				+ ", callType=" + callType + "]";
	}

}
