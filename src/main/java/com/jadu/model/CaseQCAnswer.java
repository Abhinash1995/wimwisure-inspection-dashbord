package com.jadu.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="cases_qc_answers")
public class CaseQCAnswer implements Serializable {
    @Id
    private int id;
    @JsonIgnore  
    @ManyToOne
    @JoinColumn(name="case_id", nullable=false)
    private InspectionCase inspectionCase;
    @JsonIgnore  
    @ManyToOne
    @JoinColumn(name="question_id", nullable=false)
    private CaseQCQuestion question;
    @JsonIgnore  
    @ManyToOne
    @JoinColumn(name="answer_id", nullable=false)
    private CaseQCQuestionOption option;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public CaseQCQuestion getQuestion() {
        return question;
    }

    public void setQuestion(CaseQCQuestion question) {
        this.question = question;
    }

    public CaseQCQuestionOption getOption() {
        return option;
    }

    public void setOption(CaseQCQuestionOption option) {
        this.option = option;
    }

    public InspectionCase getInspectionCase() {
        return inspectionCase;
    }

    public void setInspectionCase(InspectionCase inspectionCase) {
        this.inspectionCase = inspectionCase;
    }
    
    
}
