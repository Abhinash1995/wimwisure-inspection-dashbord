package com.jadu.model;

import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import org.json.JSONException;
import org.json.JSONObject;


@Entity
@Table(name="cases_logs")
public class CaseLog {
    @Id
    @Column(name="id")
    @GeneratedValue(strategy=GenerationType.AUTO)
    private long id;
    
    @Column(name="case_id")
    private long caseId;
    
    @Column(name="log_time")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date logTime;
    
    @Column(name="log")
    private String log;
    
    @Column(name="log_group")
    private String logGroup;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getCaseId() {
        return caseId;
    }

    public void setCaseId(long caseId) {
        this.caseId = caseId;
    }

    public Date getLogTime() {
        return logTime;
    }

    public void setLogTime(Date logTime) {
        this.logTime = logTime;
    }

    public String getLogGroup() {
        return logGroup;
    }

    public void setLogGroup(String logGroup) {
        this.logGroup = logGroup;
    }

    public String getLog() {
        return log;
    }

    public void setLog(String log) {
        this.log = log;
    }
    
    public JSONObject getLogJson(){
        try {
            return new  JSONObject(this.log);
        } catch (JSONException ex) {
            Logger.getLogger(CaseLog.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
}
