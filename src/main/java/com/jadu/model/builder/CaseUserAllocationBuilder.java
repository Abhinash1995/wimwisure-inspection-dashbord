package com.jadu.model.builder;

import com.jadu.helpers.UtilHelper;
import com.jadu.model.CaseUserAllocation;
import com.jadu.model.InspectionCase;
import com.jadu.model.User;

public class CaseUserAllocationBuilder {
    
    private final CaseUserAllocation caseUserAllocation;
    
    public CaseUserAllocationBuilder(
            InspectionCase ic,
            User user,
            int stage
    ){
        caseUserAllocation = new CaseUserAllocation();
        caseUserAllocation.setAllocationTime(UtilHelper.getDate());
        caseUserAllocation.setInspectionCase(ic);
        caseUserAllocation.setStage(stage);
        caseUserAllocation.setUser(user);
    }
    
    public CaseUserAllocation build(){
        return this.caseUserAllocation;
    }
}
