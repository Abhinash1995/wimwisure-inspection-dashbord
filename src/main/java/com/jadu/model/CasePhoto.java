package com.jadu.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;

@Entity
@Table(name="cases_photos")
public class CasePhoto implements Serializable {
    @Id
    @Column(name="id")
    @GeneratedValue(strategy=GenerationType.AUTO)
    private long id;
    
    @JsonIgnore  
    @ManyToOne( cascade = { CascadeType.DETACH }, fetch = FetchType.LAZY)
    @JoinColumn(name="case_id", nullable=false)
    private InspectionCase inspectionCase ;
    
    @Column(name="snap_time")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date snapTime;
    
    @Column(name="latitude")
    private Double latitude;
    
    @Column(name="longitude")
    private Double longitude;
    
    @Column(name="answer")
    private String answer;
    
    @Column(name="qc_answer")
    private String qcAnswer;
    
    @Column(name="file_name")
    private String fileName;
    
    @JsonIgnore  
    @ManyToOne( cascade = { CascadeType.DETACH }, fetch = FetchType.EAGER)
    @JoinColumn(name="photo_type", nullable=false)
    private PhotoType photoType ;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public InspectionCase getInspectionCase() {
        return inspectionCase;
    }

    public void setInspectionCase(InspectionCase inspectionCase) {
        this.inspectionCase = inspectionCase;
    }

    public Date getSnapTime() {
        return snapTime;
    }

    public void setSnapTime(Date snapTime) {
        this.snapTime = snapTime;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    } 

    public PhotoType getPhotoType() {
        return photoType;
    }

    public void setPhotoType(PhotoType photoType) {
        this.photoType = photoType;
    }

    public String getQcAnswer() {
        return qcAnswer;
    }

    public void setQcAnswer(String qcAnswer) {
        this.qcAnswer = qcAnswer;
    }
}
