/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jadu.bean;

/**
 *
 * @author gkumar
 */
public class PolicyBachatBean {
    private String vehicleRegNo;
    private String caseId;
    private String inspectionDate;
    private Status status; 
    private String branch;
    private String reportDownloadLink;

    public PolicyBachatBean(String vehicleRegNo, String caseId, String inspectionDate, Status status, String branch, String reportDownloadLink) {
        this.vehicleRegNo = vehicleRegNo;
        this.caseId = caseId;
        this.inspectionDate = inspectionDate;
        this.status = status;
        this.branch = branch;
        this.reportDownloadLink = reportDownloadLink;
    }

    public PolicyBachatBean() {
    }
    
    
    
    public enum Status {
        APPROVED, REJECTED, CLOSED
    }

    public String getVehicleRegNo() {
        return vehicleRegNo;
    }

    public void setVehicleRegNo(String vehicleRegNo) {
        this.vehicleRegNo = vehicleRegNo;
    }

    public String getCaseId() {
        return caseId;
    }

    public void setCaseId(String caseId) {
        this.caseId = caseId;
    }

    public String getInspectionDate() {
        return inspectionDate;
    }

    public void setInspectionDate(String inspectionDate) {
        this.inspectionDate = inspectionDate;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getReportDownloadLink() {
        return reportDownloadLink;
    }

    public void setReportDownloadLink(String reportDownloadLink) {
        this.reportDownloadLink = reportDownloadLink;
    }
    
    
}
