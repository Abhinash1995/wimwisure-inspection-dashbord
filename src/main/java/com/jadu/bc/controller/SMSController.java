package com.jadu.bc.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jadu.dao.CaseDAO;
import com.jadu.model.InspectionCase;
import com.jadu.model.User;
import com.jadu.service.AppConfigService;
import com.jadu.service.SmsService;

@Controller
@RequestMapping("/sms")
public class SMSController {

	@Autowired
	private CaseDAO caseDAOImpl;

	@Autowired
	private SmsService smsService;

	@Autowired
	private AppConfigService appConfigService;

	private static final Logger LOGGER = LoggerFactory.getLogger(SMSController.class);

	@RequestMapping(value = "/cases/online", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public ResponseEntity<User> sendSmsToOnlineCases(
			@RequestParam(value = "secretKey", required = true) String secretKey) throws Exception {
		LOGGER.info("/sms/send request recieved, parameter secretKey={}, Date={}", secretKey, new Date());
		if (secretKey.equals(appConfigService.getProperty("APP_CONFIG_SEND_SMS_API_PASSWORD", "magic@W!mwisure"))) {
			List<InspectionCase> onlineCases = caseDAOImpl.getOnlineSheduedCases();
			LOGGER.info("Total={} online scheduled cases found on Date={}", onlineCases.size(), new Date());
			List<Long> count = new ArrayList<Long>();
			for (InspectionCase onlineCase : onlineCases) {
				if ("ASSIGN_TO_CUSTOMER".equals(onlineCase.getInspectionType())) {
					if (smsService.sendSMS(onlineCase)) {
						count.add(onlineCase.getId());
					}
				}
			}
			LOGGER.info("sms sent for online scheduled cases={} on Date={}", count, new Date());
			return new ResponseEntity<>(HttpStatus.OK);
		}
		return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
	}

}
