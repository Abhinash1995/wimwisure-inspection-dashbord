package com.jadu.bc.controller;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.Barcode;
import com.itextpdf.text.pdf.BarcodeEAN;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;
import com.itextpdf.text.pdf.PdfTemplate;
import com.jadu.error.handler.ErrorHandler;
import com.jadu.helpers.FileHelper;
import com.jadu.service.BlockchainService;
import com.jadu.service.QRService;
import com.jadu.service.S3Service;
import com.jadu.service.SmsService;
import com.jadu.service.URLShortnerService;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.FilenameUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;

@Controller
@PropertySource("/WEB-INF/config.properties")
@RequestMapping("/bc")
public class BCController {
    
    @Autowired
    Environment env;
    
    @Autowired
    S3Service s3Service;
    
    @Autowired
    QRService  qrService;
    
    @Autowired
    BlockchainService blockchainService;
    
    @Autowired
    SmsService smsService;
    
    @Autowired
    URLShortnerService urlShortnerService;
    
    @RequestMapping(value = "/upload-file", method = RequestMethod.POST)
    @ResponseBody
    public String uploadFile(
            @RequestParam(value = "file", required=true) MultipartFile fileToBeUploaded
    ) throws Exception{
        String rootFolder = env.getProperty("jadu.upload.root.url");
        
        String fileName = String.valueOf(System.currentTimeMillis()) + "." + FilenameUtils.getExtension(fileToBeUploaded.getOriginalFilename());
        
        s3Service.uploadMultipartFile(fileToBeUploaded, "bc/files/" + fileName);
       
        String md5 = DigestUtils.md5Hex(fileToBeUploaded.getBytes());

        int transactionId = blockchainService.saveDataToBlockchainGetTransactionId(md5);


        JSONObject result = new JSONObject();
        result.put("url", "https://wimwisure.com/RestServer/bc/file/"+ fileName);
        result.put("transaction_id", transactionId);

        String qrCodeFile = qrService.generateQRCodeGetBytes(result);
        
        s3Service.uploadFile(new File(qrCodeFile), "bc/qr_files/qr_" + transactionId + ".png");
        
        FileHelper.deleteFileIfExists(qrCodeFile);
        
        
        PdfReader reader = new PdfReader(fileToBeUploaded.getBytes());
        PdfStamper stamper = new PdfStamper(reader, new FileOutputStream( rootFolder + "test.pdf"));
        int n = reader.getNumberOfPages();
        Rectangle pagesize;
        for (int i = 1; i <= n; i++) {
            PdfContentByte over = stamper.getOverContent(i);
            pagesize = reader.getPageSize(i);
            float x = pagesize.getLeft() + 10;
            float y = pagesize.getTop() - 50;       
            BarcodeEAN barcode = new BarcodeEAN();
            barcode.setCodeType(Barcode.EAN8);
            barcode.setCode(result.toString());
            PdfTemplate template =
                    barcode.createTemplateWithBarcode(over, BaseColor.BLACK, BaseColor.BLACK);
            over.addTemplate(template, x, y);
        }
        stamper.close();
        reader.close();
        
        String shortUrl = urlShortnerService.shortenQRCodeUrl("qr_" + transactionId + ".png");
        
        smsService.sendSMS("8985235216", "Policy has been saved. Scan this QR Code: " + shortUrl + " to download the file.");
        
        
        return md5;
    }
    
    @RequestMapping(value = "/upload-file-get-md5", method = RequestMethod.POST)
    @ResponseBody
    public String uploadFileGetMD5(
            @RequestParam(value = "file", required=true) MultipartFile fileToBeUploaded
    ) throws Exception{
        
        String md5 = DigestUtils.md5Hex(fileToBeUploaded.getBytes());
        return md5;
    }
    
    @RequestMapping(value = "/file/{filename:.+}", method = RequestMethod.GET)
    @ResponseBody
    public HttpEntity<byte[]> downloadReport(
            @PathVariable String filename,
            HttpServletResponse response
    ) throws Exception{
        
        byte[] document = s3Service.readFile("bc/files/" + filename);

        HttpHeaders header = new HttpHeaders();
        header.set("Content-Disposition", "attachment; filename=" + filename);  
        header.setContentLength(document.length);

        return new HttpEntity<>(document, header);
    }
    
    @RequestMapping(value = "/qr/{filename:.+}", method = RequestMethod.GET, produces = "image/png")
    @ResponseBody
    public HttpEntity<byte[]> getBlockchainQR(
            @PathVariable String filename,
            HttpServletResponse response
    ) throws Exception{
        return new HttpEntity<>(s3Service.readFile("bc/qr_files/" + filename));
    }
    
    @RequestMapping(value = "/get-public-blockchain", method = RequestMethod.GET)
    @ResponseBody
    public List<String> getPublicBlockchain(
            HttpServletResponse response
    ) throws Exception{
        String rootFolder = "blockchain.txt";

        return Files.readAllLines(Paths.get(rootFolder), StandardCharsets.UTF_8);
    }
    
    
    @RequestMapping(value = "/get-public-blockchain-json", method = RequestMethod.GET, produces="application/json")
    @ResponseBody
    public String getPublicBlockchainJson(
            HttpServletResponse response
    ) throws Exception{
        String rootFolder = "blockchain.txt";

        List<String> result =  Files.readAllLines(Paths.get(rootFolder), StandardCharsets.UTF_8);
        String jsonStr = "[ " + String.join(",", result) + " ] ";
        return (new JSONArray(jsonStr)).toString();
    }
    
    @ExceptionHandler(Exception.class)
    public void handleAllException(Exception ex, HttpServletResponse response) throws IOException {
        ErrorHandler.handleError(ex, response);
        System.out.println(ex.getStackTrace());
    }
}
