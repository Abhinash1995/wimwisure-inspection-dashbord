package com.jadu.bc.controller;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jadu.dao.AgentDAOImpl;
import com.jadu.dao.CaseDAO;
import com.jadu.dao.DeviceDAO;
import com.jadu.model.Device;
import com.jadu.model.InspectionCase;
import com.jadu.model.User;
import com.jadu.push.notification.AndroidPushNotificationsService;
import com.jadu.service.AppConfigService;
import com.jadu.service.SmsService;

@Controller
@RequestMapping("/notifications")
public class NotificationController {

	@Autowired
	private AgentDAOImpl agentDAOImpl;

	@Autowired
	private DeviceDAO deviceDAO;

	@Autowired
	private AppConfigService appConfigService;

	@Autowired
	private AndroidPushNotificationsService androidPushNotificationsService;

	private static final Logger LOGGER = LoggerFactory.getLogger(NotificationController.class);

	@RequestMapping(value = "/agents", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public ResponseEntity<User> sendSmsToOnlineCases(
			@RequestParam(value = "secretKey", required = true) String secretKey,
			@RequestParam(value = "lastUpdate", required = true) int lastUpdate) throws Exception {
		LOGGER.info("/notifications/agents request recieved, parameter secretKey={}, Date={}", secretKey, new Date());
		if (secretKey.equals(appConfigService.getProperty("APP_CONFIG_SEND_SMS_API_PASSWORD", "magic@W!mwisure"))) {
			List<Device> devices = deviceDAO.getUsernameByUpdateDate(getThresholdDate(-lastUpdate));
			LOGGER.info("Total={} devices found loggedIn before Date={}", devices.size(),
					getThresholdDate(-lastUpdate));
			List<String> count = new ArrayList<String>();
			for (Device device : devices) {
				androidPushNotificationsService.sendNotificationToDevice(device.getUsername(),
						appConfigService.getProperty("AGENT_NOTIFICATION_TITLE", ""),
						appConfigService.getProperty("AGENT_NOTIFICATION_MESSAGE", ""),
						appConfigService.getProperty("AGENT_NOTIFICATION_DATA", ""));
				count.add(device.getUsername());
			}
			LOGGER.info("notification sent to users={} on Date={}", count, new Date());
			return new ResponseEntity<>(HttpStatus.OK);
		}
		return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
	}

	private Date getThresholdDate(int days) {
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, days);
		Date thresholdDate = cal.getTime();
		return thresholdDate;
	}
}
