package com.jadu.bc.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jadu.dao.AgentDAOImpl;
import com.jadu.dao.CaseCommentDAO;
import com.jadu.dao.CaseDAO;
import com.jadu.dao.UserDAOImpl;
import com.jadu.dto.AllCases;
import com.jadu.dto.AllUserDTO;
import com.jadu.front.controller.QCController;
import com.jadu.model.Agent;
import com.jadu.model.CompanyBranchDivisionUser;
import com.jadu.model.InspectionCase;
import com.jadu.model.User;
import com.jadu.service.AppConfigService;
import com.jadu.service.UtilService;

@Controller
@RequestMapping("/search")
public class SearchController {

	@Autowired
	private UtilService utilService;

	@Autowired
	private CaseDAO caseDAO;

	@Autowired
	private CaseCommentDAO caseCommentDAO;

	@Autowired
	private AgentDAOImpl agentDAO;

	@Autowired
	private AppConfigService appConfigService;

	@Autowired
	private UserDAOImpl userDAO;

	private static final Logger LOGGER = LoggerFactory.getLogger(QCController.class);

	@RequestMapping(value = "/cases", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public List<AllCases> searchCase(@RequestParam(value = "case_id", required = false) String caseId,
			@RequestParam(value = "vehicle_id", required = false) String vehicle_id,
			@RequestParam(value = "customer_phone", required = false) String customer_phone,
			@RequestParam(value = "requester_phone", required = false) String requester_phone) throws Exception {
		LOGGER.info(
				"/search/cases request recieved, parameter case_id={}, vehicle_id={}, customer_phone={}, requester_phone={}",
				caseId, vehicle_id, customer_phone, requester_phone);
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		com.jadu.model.User user = utilService.getUser(authentication);
		Set<CompanyBranchDivisionUser> companyBranchDivisionUsers = utilService.getCompanyBranchDivisionUser(user);
		List<InspectionCase> casesList = new ArrayList<InspectionCase>();
		if (caseId != null) {
			casesList.add((InspectionCase) caseDAO.getCaseById(Integer.valueOf(caseId)));
		} else if (vehicle_id != null) {
			List<InspectionCase> cases = caseDAO.getCaseByVehicleNumber(vehicle_id);
			if (cases != null && !cases.isEmpty())
				casesList.addAll(cases);
			else if (checkPartialSeachCriteria(vehicle_id)) {
				cases = caseDAO.getCaseLikeVehicleNumber(vehicle_id);
				if (cases != null && !cases.isEmpty())
					casesList.addAll(cases);
			}
		} else if (customer_phone != null) {
			List<InspectionCase> cases = caseDAO.getCaseByCustomerPhoneNumber(customer_phone);
			if (cases != null && !cases.isEmpty())
				casesList.addAll(cases);
			else if (checkPartialSeachCriteria(customer_phone)) {
				cases = caseDAO.getCaseLikeCustomerPhoneNumber(customer_phone);
				if (cases != null && !cases.isEmpty())
					casesList.addAll(cases);
			}
		} else if (requester_phone != null) {
			List<InspectionCase> cases = caseDAO.getCaseByAgentPhoneNumber(requester_phone);
			if (cases != null && !cases.isEmpty())
				casesList.addAll(cases);
			else if (checkPartialSeachCriteria(requester_phone)) {
				cases = caseDAO.getCaseLikeAgentPhoneNumber(requester_phone);
				if (cases != null && !cases.isEmpty())
					casesList.addAll(cases);

			}
		} else {
			LOGGER.info("No valid input found case_id={}, vehicle_id={}, customer_phone={}, requester_phone={}", caseId,
					vehicle_id, customer_phone, requester_phone);
			throw new Exception("No valid input found");

		}
		if (casesList == null || casesList.isEmpty()) {
			LOGGER.info("Case not found for input case_id={}, vehicle_id={}, customer_phone={}, requester_phone={}",
					caseId, vehicle_id, customer_phone, requester_phone);
			throw new Exception("Case not found");
		}

		List<AllCases> allCasesList = new ArrayList<AllCases>();
		for (InspectionCase ic : casesList) {
			for (CompanyBranchDivisionUser companyBranchDivisionUser : companyBranchDivisionUsers) {
				if (checkCaseDisplayCriteria(companyBranchDivisionUser, ic)) {
					List commentList = caseCommentDAO.get(ic.getId());
					int countComments = commentList != null && !commentList.isEmpty() ? commentList.size() : 0;
					AllCases allCases = new AllCases(ic.getId(), ic.getVehicleNumber(), ic.getCustomerName(),
							ic.getCustomerPhoneNumber(), ic.getInspectionTime(), ic.getInspectionSubmitTime(),
							ic.getCurrentStage(), ic.getInspectionType(),
							ic.getInsuranceCompany() != null ? ic.getInsuranceCompany().getName() : null,
							ic.getCompanyBranchDivision().getBranch(),
							ic.getInsuranceCompany() != null ? ic.getInsuranceCompany().getLogoUrl() : null,
							ic.getRequestorPhoneNumber(), ic.getRemark(), ic.getDownloadKey(), countComments,
							ic.getComment());
					allCasesList.add(allCases);
				}
			}
		}
		LOGGER.info(
				"Case found with details={} for input case_id={}, vehicle_id={}, customer_phone={}, requester_phone={}",
				allCasesList, caseId, vehicle_id, customer_phone, requester_phone);
		return allCasesList;
	}

	private boolean checkCaseDisplayCriteria(CompanyBranchDivisionUser companyBranchDivisionUser, InspectionCase ic) {
		if (companyBranchDivisionUser != null && companyBranchDivisionUser.getCompany() == null) {
			return true;
		} else if (companyBranchDivisionUser != null
				&& companyBranchDivisionUser.getCompany().equalsIgnoreCase(ic.getCompanyBranchDivision().getCompany())
				&& companyBranchDivisionUser.getBranch() != null
				&& companyBranchDivisionUser.getBranch().contains(ic.getCompanyBranchDivision().getBranch())) {
			return true;
		} else if (companyBranchDivisionUser != null
				&& companyBranchDivisionUser.getCompany().equalsIgnoreCase(ic.getCompanyBranchDivision().getCompany())
				&& companyBranchDivisionUser.getBranch() == null) {
			return true;
		}
		return false;

	}

	private boolean checkPartialSeachCriteria(String searchKey) {
		return appConfigService.getBooleanProperty("IS_PARTIAL_SEARCH_ON", true) && searchKey != null
				&& searchKey.length() >= appConfigService.getIntProperty("PARTIAL_SEARCH_MIN_CHAR_LENGTH", 4);
	}

	@RequestMapping(value = "/agents", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public List<AllUserDTO> searchAgent(@RequestParam(value = "contact", required = false) String contact,
			@RequestParam(value = "emailId", required = false) String emailId,
			@RequestParam(value = "agentId", required = false) String agentId,
			@RequestParam(value = "branch", required = false) String branch) throws Exception {
		LOGGER.info("/search/agents request recieved, parameter contact={}, emailId={}, agentId={}, branch={}", contact,
				emailId, agentId, branch);
		List<User> agentList = new ArrayList<User>();
		if (contact != null && !contact.isEmpty()) {
			List<User> agent = userDAO.getUserByPhoneNumber(contact);
			if (agent != null && !agent.isEmpty())
				agentList.addAll(agent);
			else if (checkPartialSeachCriteria(contact)) {
				agent = userDAO.getAgentLikePhoneNumber(contact);
				if (agent != null && !agent.isEmpty())
					agentList.addAll(agent);
			}
		} else if (emailId != null && !emailId.isEmpty()) {
			List<User> agent = userDAO.getAgentByEmail(emailId);
			if (agent != null && !agent.isEmpty())
				agentList.addAll(agent);
			else if (checkPartialSeachCriteria(emailId)) {
				agent = userDAO.getAgentLikeEmail(emailId);
				if (agent != null && !agent.isEmpty())
					agentList.addAll(agent);
			}
		} else if (agentId != null && !agentId.isEmpty()) {
			List<User> agent = (List<User>) userDAO.getAgentById(agentId);
			if (agent != null && !agent.isEmpty())
				agentList.addAll(agent);
			else if (checkPartialSeachCriteria(agentId)) {
				agent = (List<User>) userDAO.getAgentLikeId(agentId);
				if (agent != null && !agent.isEmpty())
					agentList.addAll(agent);
			}
		} else if (branch != null && !branch.isEmpty()) {
			List<User> agent = userDAO.getByBranch(branch);
			if (agent != null && !agent.isEmpty())
				agentList.addAll(agent);
		} else {
			LOGGER.info("No valid input found contact={}, emailId={}, agentId={}, branch={}", contact, emailId, agentId,
					branch);
			throw new Exception("No valid input found");

		}
		if (agentList == null || agentList.isEmpty()) {
			LOGGER.info("Agent not found contact={}, emailId={}, agentId={}, branch={}", contact, emailId, agentId,
					branch);
			throw new Exception("Agent not found");
		}
		List<AllUserDTO> agentDetails = new ArrayList<AllUserDTO>();
		for (User agent : agentList) {
			AllUserDTO allUserDTO = null;
			if(agent instanceof Agent) {
				allUserDTO = utilService.prepareAllUserDTOAndCache((Agent) agent, agent.getPhoneNumber());
			}else {
				allUserDTO = utilService.prepareAllUserDTOAndCache(agent, agent.getPhoneNumber());
			}
			if (allUserDTO != null) {
				Long caseCount = caseDAO.getCaseCountByAgentPhoneNumber(agent.getPhoneNumber());
				allUserDTO.setCaseCount(caseCount);
				agentDetails.add(allUserDTO);
			}
		}
		LOGGER.info("Agent found with details={} for input contact={}, emailId={}, agentId={}, branch={}", agentDetails,
				contact, emailId, agentId, branch);
		return agentDetails;
	}

	@RequestMapping(value = "/customers", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public List<AllUserDTO> searchCustomers(@RequestParam(value = "contact", required = false) String contact,
			@RequestParam(value = "emailId", required = false) String emailId) throws Exception {
		LOGGER.info("/search/customers request recieved, parameter contact={}, emailId={}", contact, emailId);
		List<User> agentList = new ArrayList<User>();
		if (contact != null && !contact.isEmpty()) {
			List<User> userList = userDAO.getUserByPhoneNumber(contact);
			if (userList != null && !userList.isEmpty()) {
				User user = userList.get(0);
				if ("ROLE_CUSTOMER".equalsIgnoreCase(user.getAuthority().getAuthority())) {
					agentList.addAll(userList);
				}
			}
		} else if (emailId != null && !emailId.isEmpty()) {
			User user = userDAO.getUserByEmail(emailId);
			if (user != null) {
				if ("ROLE_CUSTOMER".equalsIgnoreCase(user.getAuthority().getAuthority())) {
					agentList.add(user);
				}
			}
		} else {
			LOGGER.info("No valid input found contact={}, emailId={}", contact, emailId);
			throw new Exception("No valid input found");

		}
		if (agentList == null || agentList.isEmpty()) {
			LOGGER.info("Agent not found contact={}, emailId={}", contact, emailId);
			throw new Exception("Agent not found");
		}
		List<AllUserDTO> agentDetails = new ArrayList<AllUserDTO>();
		for (User agent : agentList) {
			AllUserDTO allUserDTO = utilService.prepareAllUserDTOAndCache(agent, agent.getPhoneNumber());
			if (allUserDTO != null) {
				Long caseCount = caseDAO.getCaseCountByCustomerPhoneNumber(agent.getPhoneNumber());
				allUserDTO.setCaseCount(caseCount);
				agentDetails.add(allUserDTO);
			}
		}
		LOGGER.info("Agent found with details={} for input contact={}, emailId={}", agentDetails, contact, emailId);
		return agentDetails;
	}

}
