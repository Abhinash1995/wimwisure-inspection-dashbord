/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jadu.helpers;

import com.jadu.model.GeoLocation;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author gauta
 */
public class BoundingBox {
    public static List getBoundingBox(
            Double lat,
            Double lon,
            Double distance
    ){
        double R = 6371;  // earth radius in km

        double radius = distance; // km

        double x1 = lon - Math.toDegrees(radius/R/Math.cos(Math.toRadians(lat)));
        double x2 = lon + Math.toDegrees(radius/R/Math.cos(Math.toRadians(lat)));
        double y1 = lat + Math.toDegrees(radius/R);
        double y2 = lat - Math.toDegrees(radius/R);
        List<GeoLocation> locations =  new ArrayList<>();
        
        System.out.println(y1 + " " + x1);
        System.out.println(y2 + " " + x2);
        
        locations.add(new GeoLocation(y2, x1));
        locations.add(new GeoLocation(y1, x2));
        return locations;
    }
}
