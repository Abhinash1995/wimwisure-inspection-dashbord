package com.jadu.helpers;

import java.awt.AlphaComposite;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.awt.image.WritableRaster;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import javax.imageio.ImageIO;

public class ImageHelper {
    public static BufferedImage resizeImage(
            BufferedImage originalImage,
            int imageWidth,
            int imageHeight
    ){
        int type = originalImage.getType() == 0? BufferedImage.TYPE_INT_ARGB : originalImage.getType();
        return resizeImageWithHint(originalImage, type, imageWidth, imageHeight);
    }
    
    private static BufferedImage resizeImageWithHint(
            BufferedImage originalImage, 
            int type,
            int imageWidth,
            int imageHeight){

	BufferedImage resizedImage = new BufferedImage(imageWidth, imageHeight, type);
	Graphics2D g = resizedImage.createGraphics();
	g.drawImage(originalImage, 0, 0, imageWidth, imageHeight, null);
	g.dispose();
	g.setComposite(AlphaComposite.Src);

	g.setRenderingHint(RenderingHints.KEY_INTERPOLATION,
	RenderingHints.VALUE_INTERPOLATION_BILINEAR);
	g.setRenderingHint(RenderingHints.KEY_RENDERING,
	RenderingHints.VALUE_RENDER_QUALITY);
	g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
	RenderingHints.VALUE_ANTIALIAS_ON);

	return resizedImage;
    }
    
    public static Dimension getResizedImageDimension(BufferedImage imageToBeResized, int resizedWidth){
        int width = imageToBeResized.getWidth();
        int height = imageToBeResized.getHeight();
        
        int resizedHeight = (height*resizedWidth)/width;
        
        return new Dimension(resizedWidth, resizedHeight);
    }
    
    public static byte[] rotateClockwise(byte[] item) throws IOException {
        BufferedImage img =  ImageIO.read(new ByteArrayInputStream(item));
        int[] origPix = getIntBuff(img);

        int newWidth = img.getHeight();
        int newHeight = img.getWidth();
        int[] buff = new int[newWidth * newHeight];

        // formula for determining pixel mapping
        // (sizeOf(old y) - 1) - old y -> new x
        // old x -> new y

        for (int x = 0; x < img.getWidth(); x++)
            for (int y = 0; y < img.getHeight(); y++) {

                int pix = origPix[x + (y * img.getWidth())];
                int newX = img.getHeight() - 1 - y, newY = x;

                buff[newX + (newWidth * newY)] = pix;
            }
        // we have now rotated the array clockwise, time to place the buffer in an image


        int type = BufferedImage.TYPE_INT_ARGB;
        BufferedImage ret = new BufferedImage(newWidth, newHeight, type);
        WritableRaster wr = ret.getRaster();
        wr.setDataElements(0, 0, newWidth, newHeight, buff);
        
        try(ByteArrayOutputStream baos = new ByteArrayOutputStream();){
            ImageIO.write( ret, "png", baos );
            baos.flush();

            return baos.toByteArray();
        }
            
    }

    // variation of convertTo2DWithoutUsingGetRGB http://stackoverflow.com/a/9470843/4683264
    private static int[] getIntBuff(BufferedImage image) {

        final byte[] pixels = ((DataBufferByte) image.getRaster().getDataBuffer()).getData();
        final int width = image.getWidth();
        final int height = image.getHeight();
        final boolean hasAlphaChannel = image.getAlphaRaster() != null;

        int[] result = new int[height * width];

        final int pixelLength = hasAlphaChannel ? 4 : 3;
        for (int pixel = 0, resInd = 0; pixel < pixels.length; pixel += pixelLength) {
            int argb = 0;
            if (hasAlphaChannel)
                argb += (((int) pixels[pixel] & 0xff) << 24); // alpha
            else
                argb += -16777216; // 255 alpha

            if(pixels.length > pixel + 1)
                argb += ((int) pixels[pixel + 1] & 0xff); // blue
            
            if(pixels.length > pixel + 2)
                argb += (((int) pixels[pixel + 2] & 0xff) << 8); // green
            
            if(pixels.length > pixel + 3)
                argb += (((int) pixels[pixel + 3] & 0xff) << 16); // red
            
            result[resInd++] = argb;
        }

        return result;
    }
    
    public static Pair<Integer, Integer> getImageWidthAndHeight(byte[] picture) throws IOException{
        InputStream in = new ByteArrayInputStream(picture);

        BufferedImage buf = ImageIO.read(in);
        int width = buf.getWidth();
        int height = buf.getHeight();
        
        return new Pair<>(width, height);
    }

}
