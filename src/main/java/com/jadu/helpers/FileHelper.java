package com.jadu.helpers;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import org.springframework.http.MediaType;
import org.springframework.web.multipart.MultipartFile;

public class FileHelper {
    public static File convert(MultipartFile file) throws IOException {
        File convFile = new File(file.getOriginalFilename());
        convFile.createNewFile();
        try (FileOutputStream fos = new FileOutputStream(convFile)) {
            fos.write(file.getBytes());
        }
        return convFile;
    }
    
    public static MediaType getMediaTypeByFileExtension(String fileNameExtension){
        switch(fileNameExtension){
            case "gif":
                return MediaType.IMAGE_GIF;
            case "jpg":
            case "jpeg":
                return MediaType.IMAGE_JPEG;
            case "png":
                return MediaType.IMAGE_PNG;
            default:
                return null;
        }
    }
    
    public static void createDirectoryIfDoesnotExists(String directoryName){
        File directory = new File(directoryName);
        
        if (! directory.exists())
            directory.mkdirs();
    }
    
    public static byte[] getFileBytes(String absPath) throws IOException{
        return Files.readAllBytes(Paths.get(absPath));
    }
    
    public static boolean deleteFileIfExists(String absPath) throws IOException{
        return Files.deleteIfExists(Paths.get(absPath));
    }
}
