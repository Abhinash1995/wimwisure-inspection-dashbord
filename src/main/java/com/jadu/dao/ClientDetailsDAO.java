package com.jadu.dao;

import org.hibernate.SessionFactory;
import org.springframework.security.oauth2.provider.ClientDetails;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.security.oauth2.provider.ClientRegistrationException;

public class ClientDetailsDAO  implements ClientDetailsService{

    private SessionFactory sessionFactory;
    
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
    
    @Override
    public ClientDetails loadClientByClientId(String string) throws ClientRegistrationException {
        return null;
    }
    
}
