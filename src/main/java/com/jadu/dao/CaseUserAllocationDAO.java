/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jadu.dao;

import com.jadu.model.CaseUserAllocation;
import com.jadu.model.InspectionCase;
import com.jadu.model.User;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author gautam
 */
public class CaseUserAllocationDAO {
	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public List<CaseUserAllocation> get(long caseId, int stage) {
		String hql = "select cua FROM CaseUserAllocation cua join cua.inspectionCase ci where ci.id=?  AND cua.stage=?";

		return this.sessionFactory.getCurrentSession().createQuery(hql).setLong(0, caseId).setInteger(1, stage).list();
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void update(InspectionCase ic, int stage, User user) throws Exception {
		List<CaseUserAllocation> cuaList = new ArrayList<CaseUserAllocation>(ic.getCaseUserAllocations());

		if (cuaList == null || cuaList.isEmpty())
			throw new Exception("Unable to update details");
		for (CaseUserAllocation caseUserAllocation : cuaList) {
			if (caseUserAllocation.getStage() == 3) {
				caseUserAllocation.setUser(user);
				this.sessionFactory.getCurrentSession().saveOrUpdate(caseUserAllocation);
				break;
			}
		}

	}
}
