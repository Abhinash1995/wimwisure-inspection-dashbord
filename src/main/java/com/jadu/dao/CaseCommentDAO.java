package com.jadu.dao;

import com.jadu.dto.CaseCommentDTO;
import com.jadu.model.CaseComment;
import com.jadu.model.InspectionCase;
import com.jadu.model.User;

import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

public class CaseCommentDAO {
	private SessionFactory sessionFactory;
	
	@Autowired
	DefaultCommentDao defaultCommentDao;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void add(InspectionCase ic, String comment, Long commentId, User user) {
		CaseComment cc = new CaseComment();
		cc.setInspectionCase(ic);
		cc.setCommentTime(new Date());
		cc.setUser(user);
		
		String dropdownComment = "";
		if(null != commentId) {
			dropdownComment = defaultCommentDao.get(commentId).getComment();
		}
		
		if(null != commentId && null != comment) {
			cc.setComment(dropdownComment + " - " + comment);
			this.sessionFactory.getCurrentSession().saveOrUpdate(cc);
		}else if(null != commentId && null == comment){
			cc.setComment(dropdownComment);
			this.sessionFactory.getCurrentSession().saveOrUpdate(cc);
		}else if(null == commentId && null != comment){
			cc.setComment(comment);
			this.sessionFactory.getCurrentSession().saveOrUpdate(cc);
		}
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public List<CaseCommentDTO> get(long caseId) {
		String hql = "SELECT new com.jadu.dto.CaseCommentDTO(c.id, c.comment, c.commentTime, c.user.firstName, c.user.lastName) from CaseComment c join c.inspectionCase ic  where ic.id=:id and c.comment is not null";
		List<CaseCommentDTO> comments =  this.sessionFactory.getCurrentSession().createQuery(hql).setLong("id", caseId).list();
		
//		String dropDown = "SELECT new com.jadu.dto.CaseCommentDTO(c.id, c.defaultComment.comment, c.commentTime, c.user.firstName, c.user.lastName) from CaseComment c join c.inspectionCase ic  where ic.id=:id";
//		List<CaseCommentDTO> dropDownComments =  this.sessionFactory.getCurrentSession().createQuery(dropDown).setLong("id", caseId).list();
//		comments.addAll(dropDownComments);
		
		return comments;
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public List count(long caseId) {
		String hql = "SELECT FROM CaseComment c join c.inspectionCase ic  where ic.id=:id";

		return this.sessionFactory.getCurrentSession().createQuery(hql).setLong("id", caseId).list();
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public Collection<CaseCommentDTO> getCommentsByCommentTimeAndCaseId(long id, Date from, Date to) {
		String hql = "SELECT new com.jadu.dto.CaseCommentDTO(c.id, c.comment, c.commentTime, c.user.firstName, c.user.lastName, ic.id) from CaseComment c join c.inspectionCase ic  where ic.id=:id and (c.commentTime>=:from and c.commentTime<=:to)";
		return this.sessionFactory.getCurrentSession().createQuery(hql).setLong("id", id).setDate("from", from)
				.setDate("to", to).list();
	}
}
