package com.jadu.dao;

import com.jadu.model.PurposeOfInspection;
import java.util.List;
import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

public class PurposeOfInspectionDAO {
    private SessionFactory sessionFactory;
    
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
    
    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    public PurposeOfInspection getPurposeOfInspectionById(String id){
        
        return  (PurposeOfInspection) this.sessionFactory
                .getCurrentSession()
                .get(PurposeOfInspection.class, id);
        
    }
    
    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    public List getAll(){
        return this.sessionFactory
                .getCurrentSession()
                .createCriteria(PurposeOfInspection.class)
                .list();
    }
}
