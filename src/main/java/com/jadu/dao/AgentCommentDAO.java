package com.jadu.dao;

import java.util.Date;
import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.jadu.dto.CaseCommentDTO;
import com.jadu.model.AgentComments;
import com.jadu.model.User;

public class AgentCommentDAO {
	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void add(String phonenumber, String comment, User user) {
		AgentComments cc = new AgentComments();
		cc.setComment(comment);
		cc.setAgentPhoneNumber(phonenumber);
		cc.setCommentTime(new Date());
		cc.setUser(user);
		this.sessionFactory.getCurrentSession().saveOrUpdate(cc);
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public List get(String phonenumber) {
		String hql = "SELECT new com.jadu.dto.CaseCommentDTO(c.id, c.comment, c.commentTime, c.user.firstName, c.user.lastName) from AgentComments c   where c.agentPhoneNumber =:phonenumber";
		return this.sessionFactory.getCurrentSession().createQuery(hql).setString("phonenumber", phonenumber).list();
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public List<CaseCommentDTO> get(String phonenumber, Date from, Date to) {
		String hql = "SELECT new com.jadu.dto.CaseCommentDTO(c.id, c.comment, c.commentTime, c.user.firstName, c.user.lastName) from AgentComments c  where c.agentPhoneNumber =:phonenumber and ( c.commentTime >=:from and c.commentTime <=:to )";
		return this.sessionFactory.getCurrentSession().createQuery(hql).setString("phonenumber", phonenumber)
				.setDate("from", from).setDate("to", to).list();
	}

	public List<Integer> getCaseCount(String phonenumber) {
		// TODO Auto-generated method stub
		return null;
	}

}
