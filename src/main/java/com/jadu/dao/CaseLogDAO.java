package com.jadu.dao;

import com.jadu.helpers.UtilHelper;
import com.jadu.model.CaseLog;
import com.jadu.model.InspectionCase;
import com.jadu.model.User;
import java.util.Date;
import java.util.List;
import org.hibernate.SessionFactory;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;



public class CaseLogDAO {
    private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
    
    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    public List get(long caseId){
        String hql = "from caseLog c where ic.caseId=:id";
        
        return this.sessionFactory
                .getCurrentSession()
                .createQuery(hql)
                .setLong("id", caseId)
                .list();
    }
    
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public CaseLog addItem(
            long caseId,
            String log,
            String logGroup
    ){
        CaseLog caseLog = new CaseLog();
        caseLog.setLogTime(UtilHelper.getDate());
        caseLog.setCaseId(caseId);
        caseLog.setLogGroup(logGroup);
        caseLog.setLog(log);
        
        this.sessionFactory.getCurrentSession().persist(caseLog);
        
        return caseLog;
    }
    
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public CaseLog addItem(
            long caseId,
            String log,
            String logGroup,
            Date date
    ){
        CaseLog caseLog = new CaseLog();
        caseLog.setLogTime(date);
        caseLog.setCaseId(caseId);
        caseLog.setLogGroup(logGroup);
        caseLog.setLog(log);
        
        this.sessionFactory.getCurrentSession().persist(caseLog);
        
        return caseLog;
    }
    
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public CaseLog addCreateCase(User user, InspectionCase ic) throws JSONException{
        JSONObject payload = new JSONObject();
        payload.put("case_id", ic.getId());
        payload.put("username", user.getUsername());
        payload.put("first_name", user.getFirstName());
        payload.put("last_name", user.getLastName());
        payload.put("case_id", ic.getId());
        payload.put("time", ic.getCreationTime().getTime());
        
        
        CaseLog caseLog = new CaseLog();
        caseLog.setLogTime(ic.getCreationTime());
        caseLog.setCaseId(ic.getId());
        caseLog.setLogGroup("CREATE_CASE");
        caseLog.setLog(payload.toString());
        
        
        this.sessionFactory.getCurrentSession().persist(caseLog);
        
        return caseLog;
    }
    
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public CaseLog addUpdateInspectionTime(User user, InspectionCase ic, Date fromTime, Date toTime) throws JSONException{
        JSONObject payload = new JSONObject();
        payload.put("case_id", ic.getId());
        payload.put("username", user.getUsername());
        payload.put("first_name", user.getFirstName());
        payload.put("last_name", user.getLastName());
        payload.put("case_id", ic.getId());
        payload.put("from_time", fromTime.getTime());
        payload.put("to_time", toTime.getTime());
        payload.put("time", UtilHelper.getDate().getTime());
        
        CaseLog caseLog = new CaseLog();
        caseLog.setLogTime(ic.getCreationTime());
        caseLog.setCaseId(ic.getId());
        caseLog.setLogGroup("UPDATE_INSPECTION_TIME");
        caseLog.setLog(payload.toString());
        
        this.sessionFactory.getCurrentSession().persist(caseLog);
        
        return caseLog;
    }
    
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public CaseLog addInspectionComplete(User user, InspectionCase ic) throws JSONException{
        JSONObject payload = new JSONObject();
        payload.put("case_id", ic.getId());
        payload.put("username", user.getUsername());
        payload.put("first_name", user.getFirstName());
        payload.put("last_name", user.getLastName());
        payload.put("case_id", ic.getId());
        payload.put("time", ic.getInspectionSubmitTime().getTime());
        
        CaseLog caseLog = new CaseLog();
        caseLog.setLogTime(ic.getCreationTime());
        caseLog.setCaseId(ic.getId());
        caseLog.setLogGroup("INSPECTION_COMPLETE");
        caseLog.setLog(payload.toString());
        
        this.sessionFactory.getCurrentSession().persist(caseLog);
        
        return caseLog;
    }
    
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public CaseLog addCloseCase(User user, InspectionCase ic) throws JSONException{
        JSONObject payload = new JSONObject();
        payload.put("case_id", ic.getId());
        payload.put("username", user.getUsername());
        payload.put("first_name", user.getFirstName());
        payload.put("last_name", user.getLastName());
        payload.put("case_id", ic.getId());
        payload.put("time", ic.getCloseTime().getTime());
        payload.put("reason", ic.getCloseReason());
        
        CaseLog caseLog = new CaseLog();
        caseLog.setLogTime(ic.getCreationTime());
        caseLog.setCaseId(ic.getId());
        caseLog.setLogGroup("CLOSE_CASE");
        
        this.sessionFactory.getCurrentSession().persist(caseLog);
        
        return caseLog;
    }
    
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public CaseLog assignInspection(InspectionCase ic, User allotedTo, User allotedBy) throws JSONException{
        JSONObject payload = new JSONObject();
        payload.put("case_id", ic.getId());
        payload.put("username", allotedTo.getUsername());
        payload.put("first_name", allotedTo.getFirstName());
        payload.put("last_name", allotedTo.getLastName());
        payload.put("case_id", ic.getId());
        payload.put("time", ic.getCloseTime().getTime());
        
        CaseLog caseLog = new CaseLog();
        caseLog.setLogTime(ic.getCreationTime());
        caseLog.setCaseId(ic.getId());
        caseLog.setLogGroup("ASSIGN_INSPECTION");
        
        this.sessionFactory.getCurrentSession().persist(caseLog);
        
        return caseLog;
    }
}
