package com.jadu.dao;

import java.util.Date;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;

import com.jadu.model.ReUploadPhoto;

public class ReUploadPhotoDao {

	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void add(Long caseId, String parts) {
		ReUploadPhoto reUploadPhoto = new ReUploadPhoto();
		reUploadPhoto.setParts(parts);
		reUploadPhoto.setCreatedDate(new Date());
		reUploadPhoto.setStatus(true);
		reUploadPhoto.setCaseId(caseId);
		reUploadPhoto.setUpdatedDate(new Date());
		ReUploadPhoto existing = get(caseId);
		if(null != existing) {
			existing.setStatus(false);
			existing.setUpdatedDate(new Date());
			this.sessionFactory.getCurrentSession().saveOrUpdate(existing);
			this.sessionFactory.getCurrentSession().saveOrUpdate(reUploadPhoto);
		}else {
			this.sessionFactory.getCurrentSession().saveOrUpdate(reUploadPhoto);
		}
		

	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public ReUploadPhoto get(Long caseId) {
		Query query = sessionFactory.getCurrentSession()
				.createQuery(" from ReUploadPhoto where caseId = :caseId and status = true ").setLong("caseId", caseId);
		List results = query.list();
		ReUploadPhoto reUploadPhoto;
		if (results != null && !results.isEmpty()) {
			reUploadPhoto = (ReUploadPhoto) results.get(0);
		} else {
			reUploadPhoto = null;
		}
		return reUploadPhoto;
	}
}
