package com.jadu.dao;

import com.jadu.model.GeoLocation;
import com.jadu.model.Inspector;
import java.util.List;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author gauta
 */
public class InspectorDAO {
    private SessionFactory sessionFactory;
    
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
    
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void save(Inspector inspector){
        this.sessionFactory
                .getCurrentSession()
                .persist(inspector);
    }
    
    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    public Inspector getInspectorByUsername(String username){
        return (Inspector) this.sessionFactory
                .getCurrentSession()
                .get(Inspector.class, username);
    }
    
    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    public List getInspectorBoundingBox(
            GeoLocation minLoc,
            GeoLocation maxLoc
    ){
        String hql = "from Inspector i WHERE i.latitude BETWEEN :minLat AND :maxLat AND i.longitude BETWEEN :minLng AND :maxLng";
        return this.sessionFactory
                .getCurrentSession()
                .createQuery(hql)
                .setDouble("minLat", minLoc.getLatitude())
                .setDouble("minLng", minLoc.getLongitude())
                .setDouble("maxLat", maxLoc.getLatitude())
                .setDouble("maxLng", maxLoc.getLongitude())
                .list();
    }
    
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public int setAvailability(String username, boolean availability){
        return this.sessionFactory
                .getCurrentSession()
                .createQuery("update Inspector set available = :availability where username = :username")
                .setParameter("availability", availability)
                .setParameter("username", username)
                .executeUpdate();
        
    }
    
    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    public List getAgentByPhoneNumber(String phoneNumber){
        return this.sessionFactory
                .getCurrentSession()
                .createCriteria(Inspector.class)
                .add(Restrictions.eq("phoneNumber", phoneNumber))
                .list();
    }
    
    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    public Object getInspectorByPhoneNumber(String phoneNumber){
        return this.sessionFactory
                .getCurrentSession()
                .createCriteria(Inspector.class)
                .add(Restrictions.eq("phoneNumber", phoneNumber))
                .uniqueResult();
    }
}
