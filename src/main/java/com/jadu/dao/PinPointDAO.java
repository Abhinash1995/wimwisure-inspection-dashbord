package com.jadu.dao;

import java.util.List;
import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

public class PinPointDAO {
    private SessionFactory sessionFactory;
    
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
    
    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    public List getPins(String pinCode){
        String hqlQuery = "SELECT p FROM PinPoint p where  p.pinCode = :pinCode";
        
        return sessionFactory
                .getCurrentSession()
                .createQuery( hqlQuery )
                .setString( "pinCode", pinCode )
                .list();
    }
}
