/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jadu.dao;

import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author gauta
 */
public class CaseQCQuestionDAO {
    private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
    
    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    public List getQuestions(String vehicleType){
        String hql = "SELECT new com.jadu.dto.QCQuestionDTO(c.id, c.value, v.id, cq.id, c.photoTag) from CaseQCQuestion c join c.vehicleType v join c.caseQCQuestionGroup cq  WHERE v.id=?";
        
        return this.sessionFactory
                .getCurrentSession()
                .createQuery(hql)
                .setString(0, vehicleType)
                .list();
    }
    
    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    public Object get(
            int id
    ){
        String hql = "SELECT ic FROM CaseQCQuestion ic  WHERE ic.id=?";
        Session session = this.sessionFactory.getCurrentSession();
        Query query = session.createQuery(hql);
        query.setInteger(0, id);
        List result =  query.list();
        
        if(result.isEmpty())
            return null;
        
        return result.get(0);
    }
    
    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    public List getAll(){
        String hql = "FROM CaseQCQuestion";
        Session session = this.sessionFactory.getCurrentSession();
        Query query = session.createQuery(hql);
        return query.list();
    }
}
