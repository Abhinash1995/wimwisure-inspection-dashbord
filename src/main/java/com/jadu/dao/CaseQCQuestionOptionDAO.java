/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jadu.dao;

import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author gauta
 */
public class CaseQCQuestionOptionDAO {
    private SessionFactory sessionFactory;
    
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
    
    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    public List getAll(){
        String hql = "SELECT new com.jadu.dto.CaseQCQuestionOptionDTO(c.id, c.value, qg.id, c.isDefault, c.optionOrder) from CaseQCQuestionOption c join c.questionGroup qg ORDER BY qg.id, c.optionOrder ASC";
        
        return this.sessionFactory.getCurrentSession().createQuery(hql).list();
    }
    
    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    public Object get(
            int id
    ){
        String hql = "SELECT ic FROM CaseQCQuestionOption ic  WHERE ic.id=?";
        Session session = this.sessionFactory.getCurrentSession();
        Query query = session.createQuery(hql);
        query.setInteger(0, id);
        List result =  query.list();
        
        if(result.isEmpty())
            return null;
        
        return result.get(0);
    }
    
    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    public List getAllObjects(){
        String hql = "FROM CaseQCQuestionOption";
        Session session = this.sessionFactory.getCurrentSession();
        Query query = session.createQuery(hql);
        return  query.list();
    }
}
