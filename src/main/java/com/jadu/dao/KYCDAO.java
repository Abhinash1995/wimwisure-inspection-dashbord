package com.jadu.dao;

import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

public class KYCDAO {
    private SessionFactory sessionFactory;
    
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
    
    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    public Long getCountOfUID(String uid){
        String hqlQuery = "SELECT count(*) FROM KYC k where  k.uid = :uid";
        
        return (Long)sessionFactory
                .getCurrentSession()
                .createQuery( hqlQuery )
                .setString( "uid", uid )
                .uniqueResult();
    }
}
