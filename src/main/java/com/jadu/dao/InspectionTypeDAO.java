package com.jadu.dao;

import com.jadu.model.InspectionType;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

public class InspectionTypeDAO {
    private SessionFactory sessionFactory;
    
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
    
    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    public InspectionType get(String id){
        Session session = this.sessionFactory.getCurrentSession();
        
        Criteria cr = session.createCriteria(InspectionType.class);
        cr.add(Restrictions.eq("id", id));
        
        return (InspectionType) cr.uniqueResult();
    }
    
    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    public List getAll(){
        String hql = "FROM InspectionType ORDER BY optionOrder ASC";
        
        return this.sessionFactory.getCurrentSession().createQuery(hql).list();
    }
}
