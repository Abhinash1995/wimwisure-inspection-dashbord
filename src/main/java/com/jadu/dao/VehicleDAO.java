package com.jadu.dao;

import com.jadu.model.Vehicle;
import java.util.List;
import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

public class VehicleDAO {
    private SessionFactory sessionFactory;
    
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
    
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void save(Vehicle vehicle){
        this.sessionFactory
                .getCurrentSession()
                .persist(vehicle);
    }
    
    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    public List getAll(){
        return this.sessionFactory
                .getCurrentSession()
                .createCriteria(Vehicle.class)
                .list();
    }
    
    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    public List getAllDTO(){
        String hql = "select new com.jadu.dto.MakeModelDTO(c.id, c.vehicleType.id, c.subType.id, c.make, c.model) from Vehicle c ";
        
        return this.sessionFactory
                .getCurrentSession()
                .createQuery(hql)
                .list();
    }
    
    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    public Vehicle getVehicleById(int id){
        return (Vehicle) this.sessionFactory
                .getCurrentSession()
                .get(Vehicle.class, id);
    }   
    
    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    public List get(){
        String hql = "select distinct make from Vehicle";
        return this.sessionFactory.getCurrentSession().createQuery(hql).list();
    }
    
    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    public List getModels(String make){
        String hql = "from Vehicle v WHERE v.make=:make";
        return this.sessionFactory.getCurrentSession().createQuery(hql).setParameter("make", make).list();
    }
}
