/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jadu.dao;

import com.jadu.model.VehiclePhotoReq;
import java.util.List;
import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author gauta
 */
public class VehiclePhotoReqDAO {
    private SessionFactory sessionFactory;
    
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
    
    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    public List getAll(){
        return this.sessionFactory
                .getCurrentSession()
                .createCriteria(VehiclePhotoReq.class)
                .list();
    }
    
    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    public List get(){
        String hql = "select new com.jadu.dto.VehiclePhotoReqDTO(id as id, vehicleType.id as vehicleType, vehicleSubType.id, purposeOfInspection.id, vehicleFuelType.id, photoType.id) from VehiclePhotoReq";
        
        return this.sessionFactory.getCurrentSession().createQuery(hql).list();
    }
}
