package com.jadu.dao;

import com.jadu.helpers.RequestHelper;
import com.jadu.model.Vahan;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

public class CarRegistrationAPIDAO {
    private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
    
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public Vahan getVehicleDetails(String vehicleNumber) throws Exception{
        Query query = sessionFactory
                .getCurrentSession()
                .createQuery(" from Vahan where lower(vehicleNumber) = :vehicleNumber ")
                .setString("vehicleNumber", vehicleNumber.toLowerCase());
        List results = query.list();

        Vahan vahan;

        if(results!=null && !results.isEmpty()){
            vahan = (Vahan) results.get(0);
        } else{
            vahan =  null;
        }
        
        if(vahan != null)
            return vahan;
        
        String result = RequestHelper.sendGet("http://www.regcheck.org.uk/api/reg.asmx/CheckIndia?RegistrationNumber="+vehicleNumber+"&username=gautamnitw");
        
        if(result == null)
            throw new Exception("Vehicle details not found!");
        else{
            Vahan vahanToSave =  new Vahan();
            vahanToSave.setVehicleNumber(vehicleNumber);
            vahanToSave.setXml(result);
            sessionFactory
                .getCurrentSession()
                .save(vahanToSave);
            
            return vahanToSave;
        }
        
    }
}
