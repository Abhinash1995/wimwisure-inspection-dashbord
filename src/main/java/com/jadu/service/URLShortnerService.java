package com.jadu.service;

import static com.rosaloves.bitlyj.Bitly.as;
import static com.rosaloves.bitlyj.Bitly.shorten;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class URLShortnerService {

	@Autowired
	private AppConfigService appConfigService;

	public String shortenCaseUrl(long caseId) {
		return as(appConfigService.getProperty("BITLY_ACCESS_KEY"), appConfigService.getProperty("BITLY_SECRET_KEY"))
				.call(shorten(appConfigService.getProperty("UPDATE_ADDRESS_URL") + caseId)).getShortUrl();
	}

	public String shortenUrl(String relativeUrl, long caseId, String companyName) {
		String url = appConfigService.getProperty(companyName + " SMS BASE URL", "http://api.wimwisure.com/") + relativeUrl + caseId;
		if (appConfigService.getBooleanProperty("NEW_SMS_FLOW", true)) {
			return as(appConfigService.getProperty("BITLY_ACCESS_KEY"),
					appConfigService.getProperty("BITLY_SECRET_KEY")).call(shorten(url)).getShortUrl();
		} else {
			return as(appConfigService.getProperty("BITLY_ACCESS_KEY"),
					appConfigService.getProperty("BITLY_SECRET_KEY"))
							.call(shorten(appConfigService.getProperty("BASE_URL") + relativeUrl + caseId))
							.getShortUrl();
		}
	}

	public String shortenQRCodeUrl(String filename) {
		return as(appConfigService.getProperty("BITLY_ACCESS_KEY"), appConfigService.getProperty("BITLY_SECRET_KEY"))
				.call(shorten(appConfigService.getProperty("QR_CODE_URL") + filename)).getShortUrl();
	}
}
