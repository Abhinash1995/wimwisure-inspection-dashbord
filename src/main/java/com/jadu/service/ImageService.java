package com.jadu.service;

import com.jadu.helpers.Pair;
import com.jadu.helpers.UtilHelper;
import javax.imageio.ImageIO;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;


@Service
public class ImageService {
    @Autowired
    Environment env;
    
    @Autowired
    S3Service s3Service;
    
    public Pair<String, String> saveProfileImageGetFileName(
            MultipartFile photo
    ) throws Exception{
        
        if(ImageIO.read(photo.getInputStream()) == null)
            throw new Exception("Invalid image! Please try again.");
        
        String profilePicPhotoName = 
                UtilHelper.getCurrentTimestampAsString()
                + UtilHelper.getRandomString(5)
                + "." + FilenameUtils.getExtension(photo.getOriginalFilename());
        
        String profilePicThumbName = "thumb_" + profilePicPhotoName;
        
        s3Service.uploadMultipartImage(photo, "users/profile_photos/" + profilePicPhotoName, "users/profile_photos/" + profilePicThumbName);
        
        return new Pair<>(profilePicPhotoName, profilePicThumbName);
    }
    
    public String saveAadharImageGetFileName(
            MultipartFile image
    ) throws Exception{
        
        if(ImageIO.read(image.getInputStream()) == null)
            throw new Exception("Invalid image! Please try again.");
        
        String aadharPicPhotoName = 
                UtilHelper.getCurrentTimestampAsString()
                + UtilHelper.getRandomString(5)
                + "." + FilenameUtils.getExtension(image.getOriginalFilename());
        
        s3Service.uploadMultipartImage(image, "users/aadhar_photos/" + aadharPicPhotoName, "users/aadhar_photos/thumb_" + aadharPicPhotoName);
        
        return aadharPicPhotoName;
    }
}
