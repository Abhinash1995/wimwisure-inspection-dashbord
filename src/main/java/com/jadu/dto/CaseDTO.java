package com.jadu.dto;

import java.io.Serializable;

public class CaseDTO implements Serializable{
    private Long id;
    private String vehicleNumber;
    private String customerName;
    private String customerPhoneNumber;
    private Double inspectionLatitude;
    private Double inspectionLongitude;
    private String purposeOfInspection;
    private String insuranceCompany;
    
    
    public CaseDTO(
            Long id,
            String vehicleNumber,
            String customerName,
            String customerPhoneNumber,
            Double inspectionLatitude,
            Double inspectionLongitude,
            String purposeOfInspection
    ){
        this.id = id;
        this.vehicleNumber = vehicleNumber;
        this.customerName = customerName;
        this.customerPhoneNumber = customerPhoneNumber;
        this.inspectionLatitude = inspectionLatitude;
        this.inspectionLongitude = inspectionLongitude;
        this.purposeOfInspection = purposeOfInspection;
    }

    public Long getId() {
        return id;
    }

    public String getVehicleNumber() {
        return vehicleNumber;
    }

    public String getCustomerName() {
        return customerName;
    }

    public String getCustomerPhoneNumber() {
        return customerPhoneNumber;
    }

    public Double getInspectionLatitude() {
        return inspectionLatitude;
    }

    public Double getInspectionLongitude() {
        return inspectionLongitude;
    }

    public String getPurposeOfInspection() {
        return purposeOfInspection;
    }

    
    
   
}
