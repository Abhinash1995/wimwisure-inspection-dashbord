package com.jadu.dto;

import java.io.Serializable;

public class CaseQCQuestionOptionDTO implements Serializable{
    private final int id;
    private final String value;
    private final String group;
    private final boolean isDefault;
    private final int optionOrder;
    
    public CaseQCQuestionOptionDTO(int id, String value, String group, boolean isDefault, int optionOrder){
        this.id = id;
        this.value= value;
        this.group = group;
        this.isDefault = isDefault;
        this.optionOrder = optionOrder;
    }

    public int getId() {
        return id;
    }

    public String getValue() {
        return value;
    }

    public String getGroup() {
        return group;
    }

    public boolean isIsDefault() {
        return isDefault;
    }

    public int getOptionOrder() {
        return optionOrder;
    }
}
