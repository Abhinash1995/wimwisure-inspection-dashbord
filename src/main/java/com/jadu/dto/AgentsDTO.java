package com.jadu.dto;

import java.io.Serializable;

public class AgentsDTO implements Serializable{
    
    public String email;
    public String phoneNumber;
    public String firstName;
    public String lastName;

    public AgentsDTO(String email, String phoneNumber, String firstName, String lastName) {
        this.email = email;
        this.phoneNumber = phoneNumber;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }
}
