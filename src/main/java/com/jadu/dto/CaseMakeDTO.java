/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jadu.dto;

import java.io.Serializable;

/**
 *
 * @author gautam
 */
public class CaseMakeDTO implements Serializable{
    Long id;
    String model;

    public CaseMakeDTO(Long id, String model) {
        this.id = id;
        this.model = model;
    }

    public Long getId() {
        return id;
    }

    public String getModel() {
        return model;
    }
    
    
}
