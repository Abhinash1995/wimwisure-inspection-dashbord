package com.jadu.dto.factory;

import com.jadu.dto.CaseSubmitDTO;
import com.jadu.dto.PhotoDTO;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class CaseSubmitDTOFactory {
    
    /*private static MultipartFile getPhotoByName(List<MultipartFile> vehiclePhotos, String id){
        for(MultipartFile p : vehiclePhotos){
            if(p.getOriginalFilename().equals(id))
                return p;
        }
        
        return null;
    }*/
    
    /*public static List<CaseSubmitDTO> getCasesDTO(JSONArray arr, List<MultipartFile> vehiclePhotos) throws Exception{
        List<CaseSubmitDTO>  result = new ArrayList<>();
        for(int i=0; i<arr.length(); i++){
            try {
                JSONObject item = arr.getJSONObject(i);
                String id = item.getString("id");
                JSONArray photoArr = item.getJSONArray("photos");
                
                CaseSubmitDTO t = new CaseSubmitDTO(id);
                
                for(int j=0; j<photoArr.length(); j++){
                    JSONObject photoItem = photoArr.getJSONObject(j);
                    String answer = photoItem.getString("answer");
                    Double latitude = photoItem.getDouble("latitude");
                    Double longitude = photoItem.getDouble("longitude");
                    String fileName = photoItem.getString("fileName");
                    MultipartFile photo = getPhotoByName(vehiclePhotos, fileName);
                    
                    //System.out.println(fileName);
                    //System.out.println("Hash for image with name " + fileName  + " is: " + DigestUtils.md5Hex(photo.getBytes()));
                    
                    t.addPhotoDTO(new PhotoDTO(fileName, latitude, longitude, answer, photo));
                }
                
                result.add(t);
                
            } catch (JSONException ex) {
                Logger.getLogger(CaseSubmitDTOFactory.class.getName()).log(Level.SEVERE, null, ex);
                throw new Exception("Unable to parse values. Please contact administrator!");
            }
            
        }
        
        return result;
    }*/
    
    public static List<CaseSubmitDTO> getCasesDTO(JSONArray arr) throws Exception{
        List<CaseSubmitDTO>  result = new ArrayList<>();
        for(int i=0; i<arr.length(); i++){
            try {
                JSONObject item = arr.getJSONObject(i);
                String id = item.getString("id");
                JSONArray photoArr = item.getJSONArray("photos");
                
                CaseSubmitDTO t = new CaseSubmitDTO(id);
                
                for(int j=0; j<photoArr.length(); j++){
                    JSONObject photoItem = photoArr.getJSONObject(j);
                    String answer = photoItem.getString("answer");
                    Double latitude = photoItem.getDouble("latitude");
                    Double longitude = photoItem.getDouble("longitude");
                    String fileName = photoItem.getString("fileName");
                    
                    t.addPhotoDTO(new PhotoDTO(fileName, latitude, longitude, answer));
                }
                
                result.add(t);
                
            } catch (JSONException ex) {
                Logger.getLogger(CaseSubmitDTOFactory.class.getName()).log(Level.SEVERE, null, ex);
                throw new Exception("Unable to parse values. Please contact administrator!");
            }
            
        }
        
        return result;
    }
    
    public static CaseSubmitDTO getCaseDTOById(List<CaseSubmitDTO> items, String id){
        for(CaseSubmitDTO item : items){
            if(item.getId().equals(id))
                return item;
        }
        
        return null;
    }
}
