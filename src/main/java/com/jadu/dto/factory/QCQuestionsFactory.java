package com.jadu.dto.factory;

import com.jadu.model.CaseQCQuestion;
import java.util.List;

public class QCQuestionsFactory {
    private final List<CaseQCQuestion> items;
    
    public QCQuestionsFactory(List items){
        this.items = items;
    }
    
    public CaseQCQuestion get(int id){
        for(CaseQCQuestion  item: items)
            if(item.getId() == id)
                return item;
        
        return null;
    }
}
