package com.jadu.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class CaseSubmitDTO implements Serializable{
    private String id;
    private final List<PhotoDTO> photos = new ArrayList<>();
    
    public CaseSubmitDTO(String id){
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<PhotoDTO> getPhotos() {
        return photos;
    }

    public void addPhotoDTO(PhotoDTO photoDTO){
        this.photos.add(photoDTO);
    }
    
}
