/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jadu.dto;

import java.io.Serializable;

/**
 *
 * @author gauta
 */
public class CaseQCAnswerDTO implements Serializable{
    private int id;
    private int questionId;
    private int optionId;
    
    public CaseQCAnswerDTO(
            int id, int questionId, int optionId
    ){
        this.id = id;
        this.questionId = questionId;
        this.optionId = optionId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getQuestionId() {
        return questionId;
    }

    public void setQuestionId(int questionId) {
        this.questionId = questionId;
    }

    public int getOptionId() {
        return optionId;
    }

    public void setOptionId(int optionId) {
        this.optionId = optionId;
    }
    
    
}
