package com.jadu.dto;

import java.io.Serializable;
import java.util.Date;

public class CaseAttemptAllocationDTO implements Serializable{
    private final long id;
    private final String user;
    private final Date attemptTime;
    private final Date attemptExpiryTime;;
    
    public CaseAttemptAllocationDTO(
            long id, String user, Date attemptTime, Date attemptExpiryTime
    ){
        this.id = id;
        this.user = user;
        this.attemptTime = attemptTime;
        this.attemptExpiryTime = attemptExpiryTime;
    }

    public long getId() {
        return id;
    }

    public String getUser() {
        return user;
    }

    public Date getAttemptTime() {
        return attemptTime;
    }

    public Date getAttemptExpiryTime() {
        return attemptExpiryTime;
    }
    
    
}
