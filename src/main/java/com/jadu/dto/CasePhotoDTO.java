package com.jadu.dto;

import java.io.Serializable;
import java.util.Date;

public class CasePhotoDTO implements Serializable{
    private final long id;
    private final double latitude;
    private final double longitude;
    private final String answer;
    private final String qcAnswer;
    private final String fileName;
    private final String photoType;
    private final Date snapTime;
    
    public CasePhotoDTO(long id, double latitude, double longitude, String answer, String qcAnswer, String fileName, String photoType, Date snapTime){
        this.id = id;
        this.latitude = latitude;
        this.longitude = longitude;
        this.answer = answer;
        this.fileName = fileName;
        this.photoType = photoType;
        this.qcAnswer = qcAnswer;
        this.snapTime = snapTime;
    }

    public long getId() {
        return id;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public String getAnswer() {
        return answer;
    }

    public String getFileName() {
        return fileName;
    }

    public String getPhotoType() {
        return photoType;
    } 

    public String getQcAnswer() {
        return qcAnswer;
    }

    public Date getSnapTime() {
        return snapTime;
    }
    
    
    
}
