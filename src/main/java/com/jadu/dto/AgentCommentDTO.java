package com.jadu.dto;

import java.io.Serializable;
import java.util.List;

public class AgentCommentDTO implements Serializable {
	private List<Long> casesCount;
	private List<CaseCommentDTO> caseCommentDTO;

	public List<Long> getCasesCount() {
		return casesCount;
	}

	public void setCasesCount(List<Long> casesCount) {
		this.casesCount = casesCount;
	}

	@Override
	public String toString() {
		return "AgentCommentDTO [casesCount=" + casesCount + ", caseCommentDTO=" + caseCommentDTO + "]";
	}

	public List<CaseCommentDTO> getCaseCommentDTO() {
		return caseCommentDTO;
	}

	public void setCaseCommentDTO(List<CaseCommentDTO> caseCommentDTO) {
		this.caseCommentDTO = caseCommentDTO;
	}

}
