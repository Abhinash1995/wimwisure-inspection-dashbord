package com.jadu.dto;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import org.apache.commons.io.FilenameUtils;

public class PhotoDTO implements Serializable{
    private final String filename;
    private final Double latitude;
    private final Double longitude;
    private final String answer;
    private final Date snapTime;
    
    public PhotoDTO(
            String filename,
            Double latitude,
            Double longitude,
            String answer
    ) throws Exception{
        this.filename = filename;
        this.latitude = latitude;
        this.longitude = longitude;
        this.answer = answer;
        
        long timestamp;
        
        try{
            timestamp = Long.valueOf(FilenameUtils.removeExtension(filename));
        }catch(Exception ex){
            throw new Exception("Invalid photo file name: " + filename);
        }
        
        Timestamp stamp = new Timestamp(timestamp);
        this.snapTime = new Date(stamp.getTime());
    }
    
    public Date getSnapTime() {
        return snapTime;
    }
    
    public String getFilename() {
        return filename;
    }

    public Double getLatitude() {
        return latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public String getAnswer() {
        return answer;
    }
}
