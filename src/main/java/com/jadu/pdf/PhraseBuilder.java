package com.jadu.pdf;

import com.itextpdf.text.Font;
import com.itextpdf.text.Phrase;

public class PhraseBuilder {
    private final Phrase phrase;
    
    public PhraseBuilder(String text){
        phrase = new Phrase(text);
    }
    
    public PhraseBuilder setFont(Font font){
        phrase.setFont(font);
        return this;
    }
    
    public Phrase build(){
        return this.phrase;
    }
}
