package com.jadu.error.handler;

import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javassist.NotFoundException;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONException;
import org.json.JSONObject;

public class ErrorHandler {
    
    public static void handleError(Exception ex, HttpServletResponse response) throws IOException{
        try {
            ex.printStackTrace();
            response.setContentType("application/json");
            
            
            JSONObject result =  new JSONObject();
            result.put("error_message", ex.getMessage());
            
            if(ex instanceof NullPointerException){
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                result.put("error_code", HttpServletResponse.SC_BAD_REQUEST);
            }if(ex instanceof NotFoundException){
                response.setStatus(HttpServletResponse.SC_NOT_FOUND);
                result.put("error_code", HttpServletResponse.SC_NOT_FOUND);
            }else{
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                result.put("error_code", HttpServletResponse.SC_BAD_REQUEST);
            }
            
            response.getWriter().write(result.toString());
            
        } catch (JSONException ex1) {
            Logger.getLogger(ErrorHandler.class.getName()).log(Level.SEVERE, null, ex1);
        }
    }
}
