
package com.jadu.callback;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.jadu.dao.CaseDAO;
import com.jadu.dao.OnlineCasesDAO;
import com.jadu.model.InspectionCase;
import com.jadu.model.OnlineCases;
import com.jadu.service.AppConfigService;
import com.jadu.service.CallbackService;
import com.jadu.service.CaseFollowupService;

@RestController
@PropertySource("/WEB-INF/config.properties")
@RequestMapping("/callback")
public class CallbackController {

	private static final Logger LOGGER = LoggerFactory.getLogger(CallbackController.class);

	@Autowired
	private CaseFollowupService caseFollowupService;

	@Autowired
	private CaseDAO caseDAO;

	@Autowired
	private OnlineCasesDAO onlineCasesDAO;

	@Autowired
	private CallbackService callbackService;

	@Autowired
	private AppConfigService appConfigService;

	/**
	 * 
	 * @param callSid
	 * @param status
	 * @param recordingUrl
	 * @param dateUpdated
	 */
	@RequestMapping(value = "/status", method = RequestMethod.POST)
	@ResponseBody
	public void autoGeneratedCallStatus(@RequestParam(value = "CallSid", required = false) String callSid,
			@RequestParam(value = "Status", required = false) String status,
			@RequestParam(value = "RecordingUrl", required = false) String recordingUrl,
			@RequestParam(value = "DateUpdated", required = false) String dateUpdated) {
		LOGGER.info(
				"/callback/status request recieved with parameter callSid={},status={},recordingUrl={},dateUpdated={}",
				callSid, status, recordingUrl, dateUpdated);
		caseFollowupService.updateCallStatus(callSid, status, recordingUrl, dateUpdated);

	}

	@RequestMapping(value = "/online-cases/status", method = RequestMethod.POST)
	@ResponseBody
	public void pushOnlineCasesStatus(@RequestParam(value = "secret_key", required = true) String secret_key)
			throws JSONException {
		if (secret_key
				.equals(appConfigService.getProperty("ONLINE_CASES_CALL_BACK_STATUS_PASSWORD", "magic@W!mwisure"))) {
			LOGGER.info(" callback/online-cases/statu request recieved ");
			List<InspectionCase> onlineCases = caseDAO.getOnlineCasesByCurrentStage();
			for (InspectionCase onlineInspectioncase : onlineCases) {
				OnlineCases onlineCase = onlineCasesDAO.findByCaseId(onlineInspectioncase.getId());
				if (onlineCase != null && !onlineCase.isStatusUpdated()) {
					try {
						callbackService.pushOnlineCaseStatus(onlineInspectioncase, onlineCase);
					} catch (Exception e) {
						e.printStackTrace();
						LOGGER.error(
								"Exception raised while updating online case status for case={} and quoteId={}, error={}",
								onlineInspectioncase.getId(), onlineCase.getQuoteNumber(), e.getMessage());
					}
				} else if (appConfigService.getBooleanProperty("IS_TO_RECALL_ONLINE_CASE_STATUS_API", true)
						&& onlineCase != null && onlineCase.isStatusUpdated()
						&& !onlineCase.getStatusMessage().equalsIgnoreCase("SUCCESS")) {
					try {
						callbackService.pushOnlineCaseStatus(onlineInspectioncase, onlineCase);
					} catch (Exception e) {
						e.printStackTrace();
						LOGGER.error(
								"Exception raised while updating online case status for case={} and quoteId={}, error={}",
								onlineInspectioncase.getId(), onlineCase.getQuoteNumber(), e.getMessage());
					}

				}
			}

		}

	}

	/**
	 * 
	 * @param secret_key
	 * @throws JSONException
	 * 
	 *                       Update case status every two hours for those cases
	 *                       which are closed or completed
	 */
	@RequestMapping(value = "/update/online-cases/status", method = RequestMethod.POST)
	@ResponseBody
	public void updateOnlineCasesStatus(@RequestParam(value = "secret_key", required = true) String secret_key)
			throws JSONException {
		if (secret_key
				.equals(appConfigService.getProperty("ONLINE_CASES_CALL_BACK_STATUS_PASSWORD", "magic@W!mwisure"))) {
			LOGGER.info(" /callback/online-cases/status request recieved at {}", new Date());
			List<OnlineCases> onlineCases = onlineCasesDAO.findByCreatedTime(
					getThresholdTime(new Date(), appConfigService.getIntProperty("ONLINE_CASES_THRESHOLD_TIME", -120)));
			for (OnlineCases onlineInspectioncase : onlineCases) {
				try {
					if (!onlineInspectioncase.isStatusUpdated()) {
						InspectionCase inspectionCase = (InspectionCase) caseDAO
								.getCaseById(onlineInspectioncase.getInspectionCaseId());
						if (inspectionCase != null && inspectionCase.getCurrentStage() == 5
								&& inspectionCase.getRemark() != null) {
							callbackService.pushOnlineCaseStatus(inspectionCase, onlineInspectioncase);
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
					LOGGER.error(
							"Exception raised while updating online case status for case={} and quoteId={}, error={}",
							onlineInspectioncase.getId(), onlineInspectioncase.getQuoteNumber(), e.getMessage());
				}
			}
			LOGGER.info(" /callback/online-cases/status request finished at {}", new Date());
		}

	}

	private Date getThresholdTime(Date from, int hours) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(from);
		calendar.add(Calendar.HOUR, hours);
		return calendar.getTime();
	}

}
