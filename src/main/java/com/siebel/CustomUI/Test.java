package com.siebel.CustomUI;

import java.rmi.RemoteException;

import javax.xml.rpc.ServiceException;

public class Test {
	public static void main(String[] args){
		
		ITGI_spcMotor_spcPreInspection_spcAgency_spcResponseLocator a = new ITGI_spcMotor_spcPreInspection_spcAgency_spcResponseLocator();
		try {
			ITGIMotorPreInspectionAgencyResponse s = a.getITGIMotorPreInspectionAgencyResponse();
			
			
			ITGIMotorPreInspectionAgencyRecommendationResponse_Input input = new ITGIMotorPreInspectionAgencyRecommendationResponse_Input();
			input.setChassisNumber("00989765");
			input.setEngineNumber("12345");
			input.setInspectionDateTime("02/07/2020 20:00:00");
			input.setInspectionLocation("GURUGRAM");
			input.setLatitudeLongitude("1.20056799,1.20056799");
			input.setPreInspectionIntegrationId("1-14219722140");
			input.setPreInspectionNumber("1-6J5O2JO");
			input.setRecommendationResponse("Inspection Agency Assigned");
			input.setRemarks("TestPreIns");
			
			
			ITGIMotorPreInspectionAgencyRecommendationResponse_Output output = s.ITGIMotorPreInspectionAgencyRecommendationResponse(input);
			System.out.println(output.getStatus());
		
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
	}
}
