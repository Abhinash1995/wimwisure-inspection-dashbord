/**
 * ITGI_spcMotor_spcPreInspection_spcAgency_spcResponseLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.siebel.CustomUI;

public class ITGI_spcMotor_spcPreInspection_spcAgency_spcResponseLocator extends org.apache.axis.client.Service implements com.siebel.CustomUI.ITGI_spcMotor_spcPreInspection_spcAgency_spcResponse {

    public ITGI_spcMotor_spcPreInspection_spcAgency_spcResponseLocator() {
    }


    public ITGI_spcMotor_spcPreInspection_spcAgency_spcResponseLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public ITGI_spcMotor_spcPreInspection_spcAgency_spcResponseLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for ITGIMotorPreInspectionAgencyResponse
    private java.lang.String ITGIMotorPreInspectionAgencyResponse_address = "http://125.22.81.132/eai_ws_enu/start.swe?SWEExtSource=WSWebService&SWEExtCmd=Execute";

    public java.lang.String getITGIMotorPreInspectionAgencyResponseAddress() {
        return ITGIMotorPreInspectionAgencyResponse_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String ITGIMotorPreInspectionAgencyResponseWSDDServiceName = "ITGIMotorPreInspectionAgencyResponse";

    public java.lang.String getITGIMotorPreInspectionAgencyResponseWSDDServiceName() {
        return ITGIMotorPreInspectionAgencyResponseWSDDServiceName;
    }

    public void setITGIMotorPreInspectionAgencyResponseWSDDServiceName(java.lang.String name) {
        ITGIMotorPreInspectionAgencyResponseWSDDServiceName = name;
    }

    public com.siebel.CustomUI.ITGIMotorPreInspectionAgencyResponse getITGIMotorPreInspectionAgencyResponse() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(ITGIMotorPreInspectionAgencyResponse_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getITGIMotorPreInspectionAgencyResponse(endpoint);
    }

    public com.siebel.CustomUI.ITGIMotorPreInspectionAgencyResponse getITGIMotorPreInspectionAgencyResponse(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.siebel.CustomUI.ITGIMotorPreInspectionAgencyResponseStub _stub = new com.siebel.CustomUI.ITGIMotorPreInspectionAgencyResponseStub(portAddress, this);
            _stub.setPortName(getITGIMotorPreInspectionAgencyResponseWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setITGIMotorPreInspectionAgencyResponseEndpointAddress(java.lang.String address) {
        ITGIMotorPreInspectionAgencyResponse_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (com.siebel.CustomUI.ITGIMotorPreInspectionAgencyResponse.class.isAssignableFrom(serviceEndpointInterface)) {
                com.siebel.CustomUI.ITGIMotorPreInspectionAgencyResponseStub _stub = new com.siebel.CustomUI.ITGIMotorPreInspectionAgencyResponseStub(new java.net.URL(ITGIMotorPreInspectionAgencyResponse_address), this);
                _stub.setPortName(getITGIMotorPreInspectionAgencyResponseWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("ITGIMotorPreInspectionAgencyResponse".equals(inputPortName)) {
            return getITGIMotorPreInspectionAgencyResponse();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://siebel.com/CustomUI", "ITGI_spcMotor_spcPreInspection_spcAgency_spcResponse");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://siebel.com/CustomUI", "ITGIMotorPreInspectionAgencyResponse"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("ITGIMotorPreInspectionAgencyResponse".equals(portName)) {
            setITGIMotorPreInspectionAgencyResponseEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
