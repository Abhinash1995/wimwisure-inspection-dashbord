/**
 * ITGIMotorPreInspectionAgencyResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.siebel.CustomUI;

public interface ITGIMotorPreInspectionAgencyResponse extends java.rmi.Remote {
    public com.siebel.CustomUI.ITGIMotorPreInspectionAgencyRecommendationResponse_Output ITGIMotorPreInspectionAgencyRecommendationResponse(com.siebel.CustomUI.ITGIMotorPreInspectionAgencyRecommendationResponse_Input ITGIMotorPreInspectionAgencyRecommendationResponse_Input) throws java.rmi.RemoteException;
}
