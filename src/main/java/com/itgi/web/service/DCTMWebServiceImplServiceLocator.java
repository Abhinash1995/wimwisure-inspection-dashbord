/**
 * DCTMWebServiceImplServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.itgi.web.service;

public class DCTMWebServiceImplServiceLocator extends org.apache.axis.client.Service implements com.itgi.web.service.DCTMWebServiceImplService {

    public DCTMWebServiceImplServiceLocator() {
    }


    public DCTMWebServiceImplServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public DCTMWebServiceImplServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for DCTMWebServiceImpl
    //private java.lang.String DCTMWebServiceImpl_address = "http://localhost:8080/DCTMWebUpload/services/DCTMWebServiceImpl";
    private java.lang.String DCTMWebServiceImpl_address = "http://staging.iffcotokio.co.in:8080/WSDCTMUpload/services/DCTMWebServiceImpl";

    public java.lang.String getDCTMWebServiceImplAddress() {
        return DCTMWebServiceImpl_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String DCTMWebServiceImplWSDDServiceName = "DCTMWebServiceImpl";

    public java.lang.String getDCTMWebServiceImplWSDDServiceName() {
        return DCTMWebServiceImplWSDDServiceName;
    }

    public void setDCTMWebServiceImplWSDDServiceName(java.lang.String name) {
        DCTMWebServiceImplWSDDServiceName = name;
    }

    public com.itgi.web.service.DCTMWebServiceImpl getDCTMWebServiceImpl() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(DCTMWebServiceImpl_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getDCTMWebServiceImpl(endpoint);
    }

    public com.itgi.web.service.DCTMWebServiceImpl getDCTMWebServiceImpl(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.itgi.web.service.DCTMWebServiceImplSoapBindingStub _stub = new com.itgi.web.service.DCTMWebServiceImplSoapBindingStub(portAddress, this);
            _stub.setPortName(getDCTMWebServiceImplWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setDCTMWebServiceImplEndpointAddress(java.lang.String address) {
        DCTMWebServiceImpl_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (com.itgi.web.service.DCTMWebServiceImpl.class.isAssignableFrom(serviceEndpointInterface)) {
                com.itgi.web.service.DCTMWebServiceImplSoapBindingStub _stub = new com.itgi.web.service.DCTMWebServiceImplSoapBindingStub(new java.net.URL(DCTMWebServiceImpl_address), this);
                _stub.setPortName(getDCTMWebServiceImplWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("DCTMWebServiceImpl".equals(inputPortName)) {
            return getDCTMWebServiceImpl();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://service.web.itgi.com", "DCTMWebServiceImplService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://service.web.itgi.com", "DCTMWebServiceImpl"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("DCTMWebServiceImpl".equals(portName)) {
            setDCTMWebServiceImplEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
