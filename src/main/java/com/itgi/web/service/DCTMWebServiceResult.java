/**
 * DCTMWebServiceResult.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.itgi.web.service;

public class DCTMWebServiceResult  implements java.io.Serializable {
    private java.lang.String detail;

    private java.lang.String status;

    private com.itgi.web.service.WebContent[] webContents;

    public DCTMWebServiceResult() {
    }

    public DCTMWebServiceResult(
           java.lang.String detail,
           java.lang.String status,
           com.itgi.web.service.WebContent[] webContents) {
           this.detail = detail;
           this.status = status;
           this.webContents = webContents;
    }


    /**
     * Gets the detail value for this DCTMWebServiceResult.
     * 
     * @return detail
     */
    public java.lang.String getDetail() {
        return detail;
    }


    /**
     * Sets the detail value for this DCTMWebServiceResult.
     * 
     * @param detail
     */
    public void setDetail(java.lang.String detail) {
        this.detail = detail;
    }


    /**
     * Gets the status value for this DCTMWebServiceResult.
     * 
     * @return status
     */
    public java.lang.String getStatus() {
        return status;
    }


    /**
     * Sets the status value for this DCTMWebServiceResult.
     * 
     * @param status
     */
    public void setStatus(java.lang.String status) {
        this.status = status;
    }


    /**
     * Gets the webContents value for this DCTMWebServiceResult.
     * 
     * @return webContents
     */
    public com.itgi.web.service.WebContent[] getWebContents() {
        return webContents;
    }


    /**
     * Sets the webContents value for this DCTMWebServiceResult.
     * 
     * @param webContents
     */
    public void setWebContents(com.itgi.web.service.WebContent[] webContents) {
        this.webContents = webContents;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof DCTMWebServiceResult)) return false;
        DCTMWebServiceResult other = (DCTMWebServiceResult) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.detail==null && other.getDetail()==null) || 
             (this.detail!=null &&
              this.detail.equals(other.getDetail()))) &&
            ((this.status==null && other.getStatus()==null) || 
             (this.status!=null &&
              this.status.equals(other.getStatus()))) &&
            ((this.webContents==null && other.getWebContents()==null) || 
             (this.webContents!=null &&
              java.util.Arrays.equals(this.webContents, other.getWebContents())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getDetail() != null) {
            _hashCode += getDetail().hashCode();
        }
        if (getStatus() != null) {
            _hashCode += getStatus().hashCode();
        }
        if (getWebContents() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getWebContents());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getWebContents(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(DCTMWebServiceResult.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://service.web.itgi.com", "DCTMWebServiceResult"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("detail");
        elemField.setXmlName(new javax.xml.namespace.QName("http://service.web.itgi.com", "detail"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("status");
        elemField.setXmlName(new javax.xml.namespace.QName("http://service.web.itgi.com", "status"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("webContents");
        elemField.setXmlName(new javax.xml.namespace.QName("http://service.web.itgi.com", "webContents"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://service.web.itgi.com", "WebContent"));
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://service.web.itgi.com", "item"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
