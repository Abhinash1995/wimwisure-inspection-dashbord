/**
 * DCTMWebServiceImplService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.itgi.web.service;

public interface DCTMWebServiceImplService extends javax.xml.rpc.Service {
    public java.lang.String getDCTMWebServiceImplAddress();

    public com.itgi.web.service.DCTMWebServiceImpl getDCTMWebServiceImpl() throws javax.xml.rpc.ServiceException;

    public com.itgi.web.service.DCTMWebServiceImpl getDCTMWebServiceImpl(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
