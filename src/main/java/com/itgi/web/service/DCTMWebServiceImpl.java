/**
 * DCTMWebServiceImpl.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.itgi.web.service;

public interface DCTMWebServiceImpl extends java.rmi.Remote {
    public com.itgi.web.service.DCTMWebServiceResult upload(com.itgi.web.service.WebContent[] webContents) throws java.rmi.RemoteException;
    public com.itgi.web.service.DCTMWebServiceResult download(com.itgi.web.service.KeyValueMap[] metadata) throws java.rmi.RemoteException;
}
